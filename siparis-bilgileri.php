<?php
ob_start();
session_start();
require_once "api.php";
$api = new api();
$total_data = $api->Total($_SESSION['form_data']);

include 'baglanti.php';
$total_data2 = $total_data;
if($_SESSION['form_data']['selection'] == 1){
	$total_data2['yer'] = $_SESSION['form_data']['station_name'];
}else{
	$total_data2['yer'] = $_SESSION['form_data']['from_station_name'] . " / ".$_SESSION['form_data']['to_station_name'];
}

$base64data = base64_encode(json_encode($total_data2));
/*echo "<pre>";
print_r($_SESSION['form_data']);
echo "</pre>";*/


?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erboy</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="assets/fonts/Montserrat/stylesheet.css">
	<style>
		* ===== Scrollbar CSS ===== */
  /* Firefox */
  .sz {
    scrollbar-width: auto;
    scrollbar-color: #212121 #ffffff;
  }

  /* Chrome, Edge, and Safari */
  .sz::-webkit-scrollbar {
    width: 10px;
  }

  .sz::-webkit-scrollbar-track {
    background: #ffffff;
  }

  .sz::-webkit-scrollbar-thumb {
    background-color: #212121;
    border-radius: 10px;
    border: 3px solid #ffffff;
  }
	</style>

</head>
<body>


	<?php 
		require('header.php');
		require('headerIc.php');
	?>

    <section class="page">
        <div class="container">
            <div class="row">
    
                <div class="col-lg-8">
                    <div class="er-card">

                        <div class="er-step">
                            <div class="row">
                                <div class="col">
                                    <div class="content">
                                        <span class="number active">1</span>
                                        <span class="step"><?php echo adim1; ?></span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="content">
                                        <span class="number active">2</span>
                                        <span class="step"><?php echo adim2; ?></span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="content">
                                        <span class="number on">3</span>
                                        <span class="step"><?php echo adim3; ?></span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="content">
                                        <span class="number">4</span>
                                        <span class="step"><?php echo adim4; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="body">
                            <a href="#" class="er-back"><img src="assets/img/back.png" alt="Back"> Geri</a>

                            <form action="" class="er-step-form" id="customer-form" method="POST">
 
                                <div class="er-sf-box">
                                    <h5 class="er-setting-label"><?php echo s1; ?></h5>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input1"><?php echo s2; ?></label>
                                                <select class="custom-select" id="input1" name="gender" required="">
                                                    <option selected><?php echo s11; ?></option>
                                                    <option><?php echo s12; ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input2"><?php echo s3; ?></label>
                                                <select class="custom-select csion" id="input2" name="customer_type" required="">
                                                    <option value="individual" selected><?php echo s4; ?></option>
                                                    <option value="company"><?php echo s5; ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input3"><?php echo s6; ?></label>
                                                <input type="text" class="form-control" name="firstname" id="input3" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input4"><?php echo s7; ?></label>
                                                <input type="text" class="form-control" name="lastname" id="input4" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="er-sf-box" style="border:0px;">
                                    <h5 class="er-setting-label"><?php echo s8; ?></h5>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input5"><?php echo s9; ?></label>
                                                <input type="text" class="form-control" name="email" id="input5" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="inpbox"><?php echo s10; ?></label>
                                                <input type="text" class="form-control" name="phone" id="inpbox" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
								
								 <div class="er-sf-box crs" style="display:none;">
                                    <h5 class="er-setting-label"><?php echo s13; ?></h5>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input5"><?php echo s14; ?></label>
                                                <input type="text" class="form-control" name="unvan" id="" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input6"><?php echo s15; ?></label>
                                                <input type="text" class="form-control" name="vergi_dairesi" id="">
                                            </div>
                                        </div>
                                    </div>
									<div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input5"><?php echo s16; ?></label>
                                                <input type="text" class="form-control" name="vergi_numarasi" id="" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input6"><?php echo s17; ?></label>
                                                <input type="text" class="form-control" name="telefon" id="">
                                            </div>
                                        </div>
                                    </div>
									<div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input5"><?php echo s18; ?></label>
                                                <input type="text" class="form-control" name="sehir" id="" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input6"><?php echo s19; ?></label>
                                                <input type="text" class="form-control" name="ulke" id="">
                                            </div>
                                        </div>
                                    </div>
									<div class="form-row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="input5"><?php echo s20; ?></label>
                                                <textarea class="form-control" name="adres"></textarea>
                                            </div>
                                        </div>
    
                                    </div>
                                </div>
								
								
								<input type="hidden" name="reeldata" value="<?php echo $base64data; ?>"/>
								
								

								<?php if($_SESSION['form_data']['type'] == 'creditcart'){?>
                                <div class="er-sf-box border-0" style="border:0px;">
                                    <h5 class="er-setting-label"><?php echo s21; ?></h5>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input7"><?php echo s22; ?></label>
                                                <input type="text" class="form-control" name="pan" maxlength="16" id="input7" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input8"><?php echo s23; ?></label>
                                                <input type="text" class="form-control" name="card_name" id="input8" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for=""><?php echo s24; ?></label>
                                                <div class="form-row">
                                                    <div class="col-4">
                                                        <select class="custom-select" name="exp_month" id="input2" required="">
                                                            <option selected disabled><?php echo s25; ?></option>
                                                            <option value="01">01</option>
                                                            <option value="02">02</option>
                                                            <option value="03">03</option>
                                                            <option value="04">04</option>
                                                            <option value="05">05</option>
                                                            <option value="06">06</option>
                                                            <option value="07">07</option>
                                                            <option value="08">08</option>
                                                            <option value="09">09</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-4">
                                                        <select class="custom-select" id="input2" name="exp_year" required="">
                                                            <option selected disabled><?php echo s26; ?></option>
                                                            <option value="2021">2021</option>
                                                            <option value="2022">2022</option>
                                                            <option value="2023">2023</option>
                                                            <option value="2024">2024</option>
                                                            <option value="2025">2025</option>
                                                            <option value="2026">2026</option>
                                                            <option value="2027">2027</option>
                                                            <option value="2028">2028</option>
                                                            <option value="2029">2029</option>
                                                            <option value="2030">2030</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-4">
                                                        <input type="text" class="form-control" name="cvv" placeholder="CVC" maxlength="3" required="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								 <span class="er-sf-text text-danger"><strong><?php echo s27; ?>:</strong> Ödeme yapılan kredi kartı, aracı kiralayacak kişiye ait olmalı ve kart üzerinde kendi adı bulunmalıdır. Ödeme yapılan kredi kartının araç tesliminde ofis görevlilerine gösterilmesi talep edilecektir.</span>
								<?php } ?>

								
                               
                                
                                <span class="er-sf-text text-primary"><strong><?php echo s27; ?>:</strong> Müşterilerimizden geldiklerinde uçuş bilgisi istenmektedir. Uçak müşterisi olmadan araç teslim edilmemektedir.</span>
								<hr/>
								<span class="er-sf-text text-danger"  style="font-size:1em; color:#fff !important; border:1px solid red; padding-left:10px; background:#b70000;">Lütfen tüm sözleşmeyi okuyup; sonunda bulunan okudum; anladım butonuna tıklayınız .</span>

								<div style="border:1px solid red; padding:10px; height:300px; overflow-x:hidden; overflow-y:scroll;" class="sz">
									<?=ayargetir('sozlesme',$dil)?>
								</div>

                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" disabled class="custom-control-input" id="customCheck1" required="">
                                    <label class="custom-control-label" for="customCheck1">Kiralama Sözleşmesini Okudum ve Kabul Ediyorum</label>
                                </div>
								<div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="sms_mail_order" class="custom-control-input" id="customCheck2" required="">
                                    <label class="custom-control-label" for="customCheck2">Tarafıma SMS ve Mail Gönderilmesini Onaylıyorum</label>
                                </div>
                                <!--a href="#" class="er-sf-important" data-toggle="modal" data-target="#contractModal">Sözleşmeyi Oku</a-->

                            </form>
							
							<?php if($_SESSION['form_data']['type'] == 'creditcart'){?>
							
							
							<form method="POST" id="submit3dForm" action="#" style="display:none;">
	<input type="hidden" name="PaReq" value="">
	<input type="hidden" name="TermUrl" value="">
	<input type="hidden" name="MD" value="">
	<input type="submit" value="3D Ödemeye Geç"/>
</form>
							
							<?php } ?>
                        </div>

                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="er-card er-step-invoice" id="order_summary">
                        
                    </div>
                    <a class="btn btn-success btn-block er-btn-success" id="confirm-order"><?php echo info8; ?></a>
                </div>
                
            </div>
        </div>
    </section>
    <footer class="er-footer">
        <div class="container">
            <img src="assets/img/logo-footer.png" alt="Logo Footer" class="er-footer-logo">
            <div class="row">
                <div class="col-md-4">
                    <p class="er-footer-text">Erboycar Araç Kiralama ve Turizm sektörüne 1994 yılında adım atmıştır. Kuruluşundan bu yana sunmuş olduğu kaliteli hizmeti ve güvenilirliği ile Oto Kiralama ve Turizm sektörünün önde gelen markalarından biri olmayı başarmıştır.</p>
                    <img src="assets/img/banks.png" alt="Cards" class="dnm">
                </div>
                <div class="col-md-2">
                    <ul class="list-unstyled">
                        <li><a href="#">Hakkımızda</a></li>
                        <li><a href="#">Araç Filomuz</a></li>
                        <li><a href="#">Ofislerimiz</a></li>
                    </ul>
                    <img src="assets/img/cards.png" alt="Cards" class="dnm">
                </div>
                <div class="col-md-2">
                    <ul class="list-unstyled">
                        <li><a href="#">Kiralama Koşulları</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">İletişim</a></li>
                    </ul>
                    <ul class="list-unstyled social">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="er-footer-mail">
                        <p>En Güncel Haberlerden Ve Duyurulardan Haberdar Olmak İçin E-Mail Adresinizi Bırakınız</p>
                        <form action="">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="E-Posta Adresinizi Yazınız">
                            </div>
                            <button class="btn btn-success btn-block" type="submit">Gönder <img src="assets/img/send.png" alt="Send"></button>
                        </form>
                    </div>
                </div>
            </div>
            <p class="er-footer-copyright">Copyright ©2017 - Erboycar Online Araç Kiralama Servisi</p>
        </div>
    </footer>
	
	<!-- Modal -->
<div class="modal fade" id="contractModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sözleşme</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?=ayargetir('sozlesme',$dil)?>
      </div>
    </div>
  </div>
</div>

    <script src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>
    <script src="vendor/owlcarousel/owl.carousel.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
	
	<?php if($total_data['error']){?>
	
		alert("Oturum Süresi sona erdi. Lütfen Tekrar seçim yapınız");
		window.location.href = "/";
	<?php } ?>
		$('#order_summary').load('order_summary.php');
		$('#confirm-order').on('click',function(){
			$("#customer-form .alert").remove();
			<?php if($_SESSION['form_data']['type'] == 'creditcart'){?>
				if(!$('#input7').val() || !$('#input8').val() || !$('select[name="exp_month"] option:selected').val() || !$('select[name="exp_year"] option:selected').val() || !$('input[name="cvv"]').val()){
					$("#customer-form").append('<div class="alert alert-danger">Kart Bilgilerinizi Doldurunuz</div>');
					return false;
				}else{
					
				}
			<?php } ?>
			$('#customer-form').submit();
			
		});
		

		$("#customer-form").submit(function(e) {
			
			

			//prevent Default functionality
			e.preventDefault();
			
	
			
			if(validateForm()){
				
				if ($('#customCheck1').is(':checked')) {
					<?php if($_SESSION['form_data']['type'] == 'creditcart'){?>
					alert("Kart bilgileriniz kontrol ediliyor... Bekleyiniz...");
					<?php } ?>
					$("#confirm-order").text("BEKLEYİNİZ...");
					
			$.ajax({
				url: 'functions.php?type=saveForm',
				type: 'post',
				data: $("#customer-form").serialize(),
				dataType: 'json',
				success: function(response) {
					if(response.status == 'success'){
						window.location.href = 'siparis-onay.php';
						
					}
					<?php if($_SESSION['form_data']['type'] == 'creditcart'){?>
					else if(response.status == 'cart-error'){
						alert("Kartı bilgileriniz hatalı!");
						$("#confirm-order").text("DEVAM ET");
						return false;
					}else if(response.status == 'cart-success'){
						$("#confirm-order").text("DEVAM ET");
						alert("Kart bilgileriniz doğrulandı!");
						$("#submit3dForm").attr('action', response.URL[0]);
						$("#submit3dForm input[name=PaReq]").attr('value', response.PaReq[0]);
						$("#submit3dForm input[name=TermUrl]").attr('value', response.TermUrl[0]);
						$("#submit3dForm input[name=MD]").attr('value', response.MD[0]);
						$("#submit3dForm").submit();
						
					}<?php } ?>else{
						$("#customer-form").append('<div class="alert alert-danger">'+response.message+'</div>');
					}
					
				}
			});
			}else{
				alert("Sözleşmeyi kabul ediniz");
				return false;
			}
			}else{
				alert(" Tüm alanları doldurunuz");
				return false;
			}
			
		});
		
		
		
		function validateForm(){
			if(!$('#customer-form input[required=""]').val()){
				return false;
			}else{
				return true;
			}
			
		}
		
		$(".csion").change(function(){
			var val = $(this).val();
			if(val == 'company'){
				$(".crs").show();
			}else{
				$(".crs").hide();
			}
		});
		
		$(".sz").scroll(function(){
			if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
				$("#customCheck1").removeAttr('disabled');
			}
		})
		
	</script>
	
	        <script src="js/jquery.ccpicker.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery.ccpicker.css">
			<script>
			$( document ).ready(function() {
				$("#inpbox").CcPicker({"countryCode":"tr"});
			});
		</script>
	

</body>
</html>