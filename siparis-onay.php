<?php ob_start();session_start();include 'baglanti.php';
	$pdf = false;
	$print = false;
	$download = false;
	if(isset($_GET['pdf'])){
		$pdf = true;
	}
	
	if(isset($_GET['print'])){
		$print = true;
	}
	
	if(isset($_GET['download'])){
		$download = true;
	}
?><!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erboy</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="assets/fonts/Montserrat/stylesheet.css">
</head>
<body>
	<?php 	if(!$pdf){	require('header.php');		require('headerIc.php'); }	?>

    <section class="page">
        <div class="container">
            <div class="row">
    
                <div class="col-lg-12">
                    <div class="er-card">

                        <div class="er-step">
                            <div class="row">
                                <div class="col">
                                    <div class="content">
                                        <span class="number active">1</span>
                                        <span class="step"><?php echo adim1; ?></span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="content">
                                        <span class="number active">2</span>
                                        <span class="step"><?php echo adim2; ?></span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="content">
                                        <span class="number active">3</span>
                                        <span class="step"><?php echo adim3; ?></span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="content">
                                        <span class="number on">4</span>
                                        <span class="step"><?php echo adim4; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="body">


                            <div class="er-setting-box">
								<div class="row">
									<div class="col-md-2">
										<img src="images/checked.png" class="img-fluid"/>
									</div>
									<div class="col-md-10 my-auto">
										<h2>Rezervasyonunuz tamamlandı.</h2>
										<p>Rezervasyonunuz başarılı bir şekilde alındı! Detaylar mail ile tarafınıza iletildi.</p>
									</div>
								</div>
								<hr>
								<div class="row">
									<div class="col-md-12">
										<h4 style="color:#0aa06e; font-weight:bold;">Rezervasyon Bilgileri</h4>
										<b><?php if(isset($_SESSION['lastReservationDetails'])){									echo $_SESSION['lastReservationDetails'];								}?></b>
									</div>
								</div>
                                
                            </div>

                            
                        </div>

                    </div>
                </div>

                
                
            </div><br>
			<?php 
			if(!$pdf){
			echo "
	<a href='siparis-onay.php?pdf=1&print=1' class='btn btn-primary'><i class='fa fa-print'></i> Yazdır</a>
	<a href='res.pdf' target='_blank' class='btn btn-success'><i class='fa fa-download'></i> İndir</a>
			";}
			?>
        </div>
    </section>

<?php if(!$pdf){ 

include('footer.php'); }else{
	if($print){
	echo "<script>
		window.print();
	</script>";
	}
} ?>

    <script src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>
    <script src="vendor/owlcarousel/owl.carousel.min.js"></script>
    <script src="assets/js/main.js"></script>
    
</body>
</html>