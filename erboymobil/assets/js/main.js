﻿$('.em-home-slider').owlCarousel({
    loop: true,
    margin: 0,
    nav: false,
    dots: true,
    items: 1,
});

$('.em-slider-imgs').owlCarousel({
    loop: true,
    margin: 20,
    nav: false,
    dots: true,
    items: 1,
});

$(document).ready(function(){

$('.input-daterange').datepicker({
format: 'dd-mm-yyyy',
autoclose: false,
calendarWeeks : false,
clearBtn: false,
disableTouchKeyboard: true
});

});

$( function(){
	dateToday = new Date();
	dateToday2 = new Date();
    $("#datepicker").datepicker();
    $("#tarihM1").datepicker({
		minDate: dateToday,
        monthNames: [ "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "ğŸustos", "Eylül", "Ekim", "Kasım", "Aralık" ],
        dayNamesMin: [ "Pa", "Pt", "Sl", "Çş", "Pe", "Cu", "Ct" ],
        dateFormat: 'dd-mm-yy'
    });
	dateToday2.setDate(dateToday2.getDate() + 1);
    $("#tarihM2").datepicker({
		minDate: dateToday2,
        monthNames: [ "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül",  "Ekim", "Kasım", "Aralık" ],
        dayNamesMin: [ "Pa", "Pt", "Sl", "Çş", "Pe", "Cu", "Ct" ],
        dateFormat: 'dd-mm-yy'
    });
});




$('#from-station').click(debounce(liveSearch, 0));
$('#from-station').keyup(debounce(liveSearch, 500));

function debounce(func, wait, immediate) {
		var timeout;
		return function () {
			var context = this, args = arguments;
			var later = function () {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	};

	function liveSearch(){
		var inputVal = $(this).val();
		var resultDropdown = $(this).siblings(".liveresult-from");
						  
        $.get('/erboymobil/functions.php', {type:"getStations",name: inputVal}).done(function(data){
            $('.liveresult-from').show();
            $('.liveresult-from').html(data);
        });

         
	}

    $(document.body).on('click','.liveresult-from a', function() {
        $('#from-station').val($(this).text());
        $('#from-station-code').val($(this).data('code'));
        $('.liveresult-from').hide();
    })
	
	
	$('#customSwitch1').trigger('change');
	$('#customSwitch1').on('change',function(){
		if($(this).is(':checked')){
			$('#to-station').parent().hide().prev().hide();
		}else{
			$('#to-station').parent().show().prev().show();
		}
	})
	
$('#to-station').click(debounce2(liveSearch2, 0));
$('#to-station').keyup(debounce2(liveSearch2, 500));

function debounce2(func, wait, immediate) {
		var timeout;
		return function () {
			var context = this, args = arguments;
			var later = function () {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	};

	function liveSearch2(){
		var inputVal = $(this).val();
		var resultDropdown = $(this).siblings(".liveresult-to");
						  
        $.get('/erboymobil/functions.php', {type:"getStations",name: inputVal}).done(function(data){
            $('.liveresult-to').show();
            $('.liveresult-to').html(data);
        });

        
	}

    $(document.body).on('click','.liveresult-to a', function() {
        $('#to-station').val($(this).text());
        $('#to-station-code').val($(this).data('code'));
        $('.liveresult-to').hide();
    })
	
	
	mobiscroll.datepicker('#picker', {
    controls: ['time'],
    select: 'range',
    display: 'inline',
    touchUi: true
});