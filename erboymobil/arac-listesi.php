<?php require('db/baglan.php');

$araclar = $db->query("SELECT * FROM araclar where dil = 'turkce' order by arac_id  desc ")->fetchAll();
$page = 'araclar'; 
?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erboy</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="assets/fonts/Montserrat/stylesheet.css">
</head>
<body>

   <div class="em-app">
        <div class="container-fluid">

            <div class="em-header">
                <a href="<?=$previous?>" class="back"><img src="assets/img/back.png" alt="Back"></a>
                <img src="assets/img/logo-search.png" alt="Logo">
                <div class="em-header-title">
                    <h5>Araç Filomuz</h5>
                    <a href="#"><img src="assets/img/icon-filter.png" alt="Filter"></a>
                </div>
            </div>

            <div class="em-page-content em-list1">



                <div class="tab-content">

                    <div class="tab-pane active fade show" id="yeni" role="tabpanel">
                        <div class="em-list1-cars">
<?php 
                    foreach ($araclar as $key => $value) {
                        ?>
                            <a href="#" class="em-link">
                                <div class="em-box-horizontal">
                                    <div class="top">
                                        <img src="../i/site/<?php echo $value['arac_gorsel']?>" alt="Car" class="img-car">
                                        <div class="content">
                                            <span class="name"><?php echo $value['araci_adi']?></span>
                                            <span class="group"><?php echo $value['arac_marka']?></span>
                                            <ul class="list-unstyled">
                                                <li><img src="assets/img/mini-oil.png" alt="Oil"> <?php echo $value['yakit']?></li>
                                                <li><img src="assets/img/mini-air.png" alt="Air"> <?php echo $value['kilima']?></li>
                                                <li><img src="assets/img/mini-wh.png" alt="WH"> <?php echo $value['vites']?></li>
                                                <li><img src="assets/img/mini-person3.png" alt="Kapı"> <?php echo $value['kapi']?></li>
                                                <li><img src="assets/img/mini-location.png" alt=""> <?php echo $value['km']?></li>
                                                <li><img src="assets/img/mini-wallet.png" alt=""> <?php echo $value['depozit']?></li>
                                                <li><img src="assets/img/mini-person.png" alt=""> <?php echo $value['kapasite']?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="bottom">
                                        <div class="left"></div>
                                        <div class="right">
                                            <span class="price">
                                                <span class="now">Rezervasyon Yap (<?php echo $value['fiyat']?>)</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </a>

					<?php } ?>

                        </div>
                    </div>


                    

                </div>

            </div>

			<?php include('_inc/menu.php'); ?>

        </div>
   </div>

    <script src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>
    <script src="vendor/owlcarousel/owl.carousel.min.js"></script>
    <script src="assets/js/main.js"></script>
    
</body>
</html>