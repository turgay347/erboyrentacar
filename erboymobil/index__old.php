<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erboy</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="assets/fonts/Montserrat/stylesheet.css">
</head>
<body>

   <div class="em-app em-home">
        <div class="container-fluid">

            <div class="em-home-search">
                <img src="assets/img/logo-search.png" alt="Logo">
                <form action="">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><span class="icon-nav-search"></span></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Marka veya Modeli Ara" aria-describedby="basic-addon1">
                    </div>
                </form>
            </div>
        
            <div class="em-home-slider owl-carousel owl-theme">

                <div class="item">
                    <div class="em-hs-item" style="background-image:url(assets/img/slider-back.jpg)">
                        <div class="alt">
                            <h2>Sana En<br>Uygun Aracı Bul</h2>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="em-hs-item" style="background-image:url(assets/img/slider-back.jpg)">
                        <div class="alt">
                            <h2>Sana En<br>Uygun Aracı Bul</h2>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="em-hs-item" style="background-image:url(assets/img/slider-back.jpg)">
                        <div class="alt">
                            <h2>Sana En<br>Uygun Aracı Bul</h2>
                        </div>
                    </div>
                </div>

            </div>

            <div class="em-home-section">
                
                <div class="head">
                    <h3>Videolar</h3>
                    <a href="#" class="all">Tümü gör</a>
                </div>

                <div class="list">

                    <a href="#" class="em-link">
                        <div class="em-home-card">
                            <img src="assets/img/home-video.png" alt="Video">
                            <div class="content">
                                <span class="title">Alfa Romeo 540S sürüşü</span>
                                <span class="author">Elif Yılmaz. 14 Nisan</span>
                            </div>
                        </div>
                    </a>

                    <a href="#" class="em-link">
                        <div class="em-home-card">
                            <img src="assets/img/home-video.png" alt="Video">
                            <div class="content">
                                <span class="title">Alfa Romeo 540S sürüşü</span>
                                <span class="author">Elif Yılmaz. 14 Nisan</span>
                            </div>
                        </div>
                    </a>

                    <a href="#" class="em-link">
                        <div class="em-home-card">
                            <img src="assets/img/home-video.png" alt="Video">
                            <div class="content">
                                <span class="title">Alfa Romeo 540S sürüşü</span>
                                <span class="author">Elif Yılmaz. 14 Nisan</span>
                            </div>
                        </div>
                    </a>

                </div>

            </div>

            <div class="em-home-section mb-5">
                
                <div class="head">
                    <h3>Trend Haberler</h3>
                    <a href="#" class="all">Tümü gör</a>
                </div>

                <div class="list">

                    <a href="#" class="em-link">
                        <div class="em-home-card">
                            <img src="assets/img/home-video.png" alt="Video">
                            <div class="content">
                                <span class="title">Alfa Romeo 540S sürüşü</span>
                                <span class="author">Elif Yılmaz. 14 Nisan</span>
                            </div>
                        </div>
                    </a>

                    <a href="#" class="em-link">
                        <div class="em-home-card">
                            <img src="assets/img/home-video.png" alt="Video">
                            <div class="content">
                                <span class="title">Alfa Romeo 540S sürüşü</span>
                                <span class="author">Elif Yılmaz. 14 Nisan</span>
                            </div>
                        </div>
                    </a>

                    <a href="#" class="em-link">
                        <div class="em-home-card">
                            <img src="assets/img/home-video.png" alt="Video">
                            <div class="content">
                                <span class="title">Alfa Romeo 540S sürüşü</span>
                                <span class="author">Elif Yılmaz. 14 Nisan</span>
                            </div>
                        </div>
                    </a>

                </div>

            </div>

            <div class="em-nav">
                <div class="row">
                    <div class="col-3">
                        <div class="em-nav-link active">
                            <span class="icon icon-nav-home"></span>
                            <span class="name">Anasayfa</span>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="em-nav-link">
                            <span class="icon icon-nav-search"></span>
                            <a href="/erboymobil/arama.php"><span class="name">Arama</span></a>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="em-nav-link">
                            <span class="icon icon-nav-notification"></span>
                            <span class="name">Bildirimler</span>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="em-nav-link">
                            <span class="icon icon-nav-user"></span>
                            <span class="name">Profil</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
   </div>

    <script src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>
    <script src="vendor/owlcarousel/owl.carousel.min.js"></script>
    <script src="assets/js/main.js"></script>
    
</body>
</html>