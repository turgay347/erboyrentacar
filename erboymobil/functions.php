<?php
ob_start();
session_start();
require_once "api.php";
$api = new api();

    $type = (isset($_GET['type']) && $_GET['type']) ? $_GET['type'] : '';

    switch ($type) {
        case 'getStations':
            $name = (isset($_GET['name']) && $_GET['name']) ? $_GET['name'] : '';
            echo $api->getStations($name);
            break;
        case 'submit':
			if(searchFormValidate($_POST)){
				$_SESSION['form_data'] = array(
					'from_station_name' => $_POST['from_station_name'],
					'to_station_name' => $_POST['to_station_name'],
					'same_station' => $_POST['same_station'],
					'from_station_code' => $_POST['from_station_code'],
					'to_station_code' => $_POST['to_station_code'],
					'startdate' => $_POST['startdate'],
					'startclock' => $_POST['startclock'],
					'enddate' => $_POST['enddate'],
					'endclock' => $_POST['endclock']
				);
				
				echo json_encode(array('success' => 'true','redirect' => '/erboymobil/arac-secimi.php'));
				die();
			}
            break;
        case 'selectcar':
            $_SESSION['form_data']['car_token'] = $_POST['token'];	
			
			if($_POST['type']){
				$_SESSION['form_data']['type'] = $_POST['type'];	
				echo json_encode(array('success' => 'true','redirect' => '/erboymobil/arac-ekstra.php'));
			}else{
				echo json_encode(array('success' => 'true','redirect' => '/erboymobil/arac-detay.php'));
			}
			
            break;
			
		case 'selectextras':
			$_SESSION['form_data']['extras'] = $_POST['extras'];	
			echo json_encode(array('success' => 'true'));
            break;
			
		case 'saveForm':
			$form_result = $api->saveForm($_POST);	
			echo json_encode($form_result);
            break;
        }
		
		
function searchFormValidate($post){
	
	$error = array();
	if(!$post['station_name'] && !$post['station_code']){
		$error[] = "Teslim yeri seçilmelidir";
	}
	
	if(!$post['startdate'] && !$post['startclock']){
		$error[] = "Alış Tarih ve saati seçilmelidir";
	}
	
	if(!$post['enddate'] && !$post['endclock']){
		$error[] = "Teslim Tarih ve saati seçilmelidir";
	}
	
	if($error){
		return array(
			'status' => 'error',
			'messages' => $error
		);
	}else{
		return array('status' => 'success');
	}
	
	
}
?>

