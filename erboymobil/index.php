<?php $page = 'anasayfa'; 

date_default_timezone_set('Europe/Istanbul');
$time = new DateTime("00:00");
    $close = new DateTime('24:00');
    while ($time < $close) {
        $day[] = $time->format('H:i');
        $time->modify('+15 minutes');
    }
$time = date('H',strtotime(date("H:i")) + 3600);
?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erboy</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="assets/fonts/Montserrat/stylesheet.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>

   <div class="em-app">
        <div class="container-fluid">

            <div class="em-header">
                <img src="assets/img/logo-search.png" alt="Logo">
            </div>

            <div class="em-page-content em-rent">

                <div class="em-rent-card em-rc-nav">
                    <ul class="nav nav-pills nav-justified" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="gunluk-tab" data-toggle="tab" href="#gunluk" role="tab" aria-controls="gunluk" aria-selected="true">Günlük Kiralama</a>
                        </li>
                     
                    </ul>
                </div>

                <div class="tab-content">

                    <div class="tab-pane active fade show" id="gunluk" role="tabpanel">
						<form id="gunluk-form" autocomplete="off">
							<div class="em-rent-card active" style="position:relative;">
								<label for="input1"><img src="assets/img/location-active.png" alt="Location"> Alış / Bırakış Noktası</label>
								<input type="text" class="form-control" name="from_station_name" id="from-station" placeholder="Şehir, Havaalanı, Araç Kiralama...">
								<ul class="liveresult-from"></ul>
								<input type="hidden" id="from-station-code" name="from_station_code">
							</div>

							<div class="em-rent-divider" style="display:none"><img src="assets/img/divider.png" alt="Divider"></div>

							<div class="em-rent-card" style="position:relative;display:none">
								<label for="input2"><img src="assets/img/location.png" alt="Location"> Bırakış Noktası</label>
								<input type="text" class="form-control" name="to_station_name" id="to-station" placeholder="Şehir, Havaalanı, Araç Kiralama...">
								<ul class="liveresult-to"></ul>
								<input type="hidden" id="to-station-code" name="to_station_code">
							</div>

							<div class="custom-control custom-switch">
								<input type="checkbox" class="custom-control-input" name="same_station" id="customSwitch1" checked="checked">
								<label class="custom-control-label" for="customSwitch1" ><span>Aynı yere teslim edeceğim</span></label>
							</div>

							<div class="row">
								<div class="col-6 pr-2">
									<div class="em-rent-card">
										<label for="tarihM1"><img src="assets/img/icon-calendar.png" alt="Location"> Alış Tarihi</label>
										<input type="text" id="tarihM1" value="<?=date("d-m-Y")?>" name="startdate" class="form-control" placeholder="gg/aa/yyyy" autocomplete="off">
									</div>
								</div>
								<div class="col-6 pl-2">
									<div class="em-rent-card">
										<label for="saat1"><img src="assets/img/icon-time.png" alt="Location"> Alış Saati</label>
										<select class="form-control" id="saat1" name="startclock">
											   <?php foreach($day as $d){ ?>
                                                <option <?=$d == $time.":30" ? 'selected':''?> value="<?php echo $d; ?>"><?php echo $d; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>

							<div class="em-rent-divider"><img src="assets/img/divider.png" alt="Divider"></div>

							<div class="row">
								<div class="col-6 pr-2">
									<div class="em-rent-card">
										<label for="tarihM2"><img src="assets/img/icon-calendar.png" alt="Location"> Teslim Tarihi</label>
										<input type="text" id="tarihM2" value="<?=date("d-m-Y", time() + 86400)?>" name="enddate"  class="form-control" placeholder="gg/aa/yyyy"  autocomplete="off">
									</div>
								</div>
								<div class="col-6 pl-2">
									<div class="em-rent-card">
										<label for="saat2"><img src="assets/img/icon-time.png" alt="Location"> Teslim Saati</label>
										<select class="form-control" id="saat2" name="endclock">
											   <?php foreach($day as $d){ ?>
                                                <option <?=$d == $time.":30" ? 'selected':''?> value="<?php echo $d; ?>"><?php echo $d; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
						</form>
                        <a href="#" onClick="$('#gunluk-form').submit();" class="btn btn-primary btn-block em-rent-btn mt-3 mb-4">Ara</a>
                    </div>


                </div>

            </div>

            <!-- ÖNCEKİ ALAN
            <div class="em-rent-result">
                <div class="right"><i class="fa fa-chevron-right"></i></div>
                <div class="row">
                    <div class="col-6">
                        <div class="content">
                            <span class="title"><img src="assets/img/calendar.png" alt="Calendar"> Alış Tarihi</span>
                            <span class="text">19 Nis 2021<br>10:00</span>
                        </div>
                    </div>
                    <div class="col-6 border-left">
                        <div class="content">
                            <span class="title"><img src="assets/img/calendar.png" alt="Calendar"> Bırakış Tarihi</span>
                            <span class="text text-muted">19 Nis 2021<br>10:00</span>
                        </div>
                    </div>
                </div>
            </div> -->

			<?php include('_inc/menu.php'); ?>

        </div>
   </div>

    <script src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>
    <script src="vendor/owlcarousel/owl.carousel.min.js"></script>
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   
    <script>
            $("#gunluk-form").on("submit", function (e) {
                e.preventDefault();
				
				var start_date = $('#tarihM1').val();
				var end_date = $('#tarihM2').val();
				
				
				if(!$('#from-station-code').val()){
					alert("Teslim yeri seçilmelidir");
					return false;
					
				}
				
				
				if(!end_date || !start_date ){
					
					alert("Teslim tarihi ve alış tarihi seçilmelidir");
					return false;
				}
				
				
				var dates1 = start_date.split("-");
				var st_date = dates1[1]+"/"+dates1[0]+"/"+dates1[2];
				var start_date_time = new Date(st_date).getTime();
				
				var dates2 = end_date.split("-");
				var en_date = dates2[1]+"/"+dates2[0]+"/"+dates2[2];
				var end_date_time = new Date(en_date).getTime();
				
				

				if(end_date_time <= start_date_time ){
					alert("Teslim tarihi alış tarihinden önce olamaz");
					return false;
				}
				
				
                $.ajax({
                    url: "/erboymobil/functions.php?type=submit",
                    type: "post",
                    data: $("#gunluk-form").serialize(),
                    dataType: "json",
                    success: function (response) {
                        if (response.success == "true") {
                            window.location.href = response.redirect;
                        }
                    },
                });
            });
        </script>
		
		
    <script src="assets/js/main.js"></script>
	

    
</body>
</html>