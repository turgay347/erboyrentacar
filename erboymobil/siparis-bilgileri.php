<?php require('db/baglan.php');
ob_start();
session_start();
require_once "api.php";
$api = new api();
$total_data = $api->Total($_SESSION['form_data']);$sozlesme = $db->query("SELECT * FROM ayarlar where ad = 'sozlesme' && dil = 'turkce' LIMIT 1")->fetch();$sozlesme = $sozlesme['ayar'];

$total_data2 = $total_data;
$total_data2['yer'] = $_SESSION['form_data']['from_station_name'];
$base64data = base64_encode(json_encode($total_data2));

?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erboy</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="assets/fonts/Montserrat/stylesheet.css">
		<style>
		* ===== Scrollbar CSS ===== */
  /* Firefox */
  .sz {
    scrollbar-width: auto;
    scrollbar-color: #212121 #ffffff;
  }

  /* Chrome, Edge, and Safari */
  .sz::-webkit-scrollbar {
    width: 10px;
  }

  .sz::-webkit-scrollbar-track {
    background: #ffffff;
  }

  .sz::-webkit-scrollbar-thumb {
    background-color: #212121;
    border-radius: 10px;
    border: 3px solid #ffffff;
  }
	</style>
</head>
<body>

   <div class="em-app">
        <div class="container-fluid">

            <div class="em-header">
                <img src="assets/img/logo-search.png" alt="Logo">
            </div>

            <div class="em-page-content em-rent">

                <div class="em-rent-card em-steps">
                    <div class="row">
                        <div class="col-3">
                            <div class="content">
                                <span class="number active">1</span>
                                <span class="step">Araç Seçimi</span>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="content">
                                <span class="number active">2</span>
                                <span class="step">Araç Ekstraları</span>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="content">
                                <span class="number on">3</span>
                                <span class="step">Müşteri Bilgileri</span>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="content">
                                <span class="number">4</span>
                                <span class="step">Rzrvsyn Onay</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="em-rent-card p-4">

                    <form action="" class="er-step-form" id="customer-form">

                        <div class="er-sf-box">
                            <h5 class="er-setting-label">Kişisel Bilgiler</h5>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="input1">Sayın</label>
                                        <select class="custom-select" id="input1" name="gender">
                                            <option selected>Bay</option>
                                            <option>Bayan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="input2">Müşteri Tipi</label>
                                        <select class="custom-select csion" id="input2" name="customer_type" required="">
                                            <option value="individual" selected>Şahıs</option>
                                            <option value="company">Kurumsal</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="input3">Adınız</label>
                                        <input type="text" class="form-control" name="firstname" required="" id="input3">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="input4">Soyadınız</label>
                                        <input type="text" class="form-control" name="lastname" required=""  id="input4">
                                    </div>
                                </div>
                            </div>
                        </div>
						
						<input type="hidden" name="reeldata" value="<?php echo $base64data; ?>"/>

                        <div class="er-sf-box">
                            <h5 class="er-setting-label">İletişim Bilgileri</h5>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="input5">E-Posta Adresiniz</label>
                                        <input type="text" class="form-control" name="email" required="" id="input5">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="input6">Telefon Numaranız</label>
                                        <input type="text" class="form-control" name="phone" required="" id="input6">
                                    </div>
                                </div>
                            </div>
                        </div>
						
						 <div class="er-sf-box crs" style="display:none;">
                                    <h5 class="er-setting-label">Kurumsal Fatura Bilgisi</h5>
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input5">Ünvan</label>
                                                <input type="text" class="form-control" name="unvan" id="" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input6">Vergi Dairesi</label>
                                                <input type="text" class="form-control" name="vergi_dairesi" id="">
                                            </div>
                                        </div>
                                    </div>
									<div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input5">Vergi Numarası</label>
                                                <input type="text" class="form-control" name="vergi_numarasi" id="" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input6">Telefon</label>
                                                <input type="text" class="form-control" name="telefon" id="">
                                            </div>
                                        </div>
                                    </div>
									<div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input5">Şehir</label>
                                                <input type="text" class="form-control" name="sehir" id="" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="input6">Ülke</label>
                                                <input type="text" class="form-control" name="ulke" id="">
                                            </div>
                                        </div>
                                    </div>
									<div class="form-row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="input5">Adres</label>
                                                <textarea class="form-control" name="adres"></textarea>
                                            </div>
                                        </div>
    
                                    </div>
                                </div>
						
						<?php if($_SESSION['form_data']['type'] == 'creditcart'){?>
                        <div class="er-sf-box border-0">
                            <h5 class="er-setting-label">Ödeme Bilgileri</h5>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="input7">Kart Numarası</label>
                                        <input type="text" class="form-control" name="pan" id="input7">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="input8">Kart Üzerindeki İsim</label>
                                        <input type="text" class="form-control" name="card_name" id="input8">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Son Kullanma Tarihi</label>
                                        <div class="form-row">
                                            <div class="col-4">
                                                <select class="custom-select" name="exp_month" required>
                                                    <option selected disabled>Ay</option>
                                                    <option value="01">01</option>
                                                </select>
                                            </div>
                                            <div class="col-4">
                                                <select class="custom-select" name="exp_year" >
                                                    <option selected disabled>Yıl</option>
                                                    <option value="2021">2021</option>
                                                </select>
                                            </div>
                                            <div class="col-4">
                                                <input type="text" class="form-control" name="cvv"  placeholder="CVC">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						

                        <span class="er-sf-text text-danger"><strong>Not:</strong> Ödeme yapılan kredi kartı, aracı kiralayacak kişiye ait olmalı ve kart üzerinde kendi adı bulunmalıdır. Ödeme yapılan kredi kartının araç tesliminde ofis görevlilerine gösterilmesi talep edilecektir.</span>
                        <?php } ?>
                        <span class="er-sf-text text-primary"><strong>Not:</strong> Müşterilerimizden geldiklerinde uçuş bilgisi istenmektedir. Uçak müşterisi olmadan araç teslim edilmemektedir.</span>

<hr/>
								<span class="er-sf-text text-danger">Lütfen tüm sözleşmeyi okuyup; sonunda bulunan okudum; anladım butonuna tıklayınız .</span>

								<div style="border:1px solid red; padding:10px; height:300px; overflow-x:hidden; overflow-y:scroll;" class="sz">
									<?=$sozlesme?>
								</div>

                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" disabled class="custom-control-input" id="customCheck1" required="">
                                    <label class="custom-control-label" for="customCheck1">Kiralama Sözleşmesini Okudum ve Kabul Ediyorum</label>
                                </div>

                        <!--div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label" for="customCheck1">Kiralama Sözleşmesini Okudum ve Kabul Ediyorum</label>
                        </div>
                        <a href="#" class="er-sf-important" data-toggle="modal" data-target="#contractModal">Sözleşmeyi Oku</a-->

                    </form>

                </div>

                <a id="confirm-order" class="btn btn-primary btn-block btn-devam">Devam Et</a>

            </div>

            <?php include('_inc/menu.php'); ?>

        </div>
   </div>
	<!-- Modal -->
<div class="modal fade" id="contractModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sözleşme</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?=$sozlesme?>
      </div>
    </div>
  </div>
</div>


    <script src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>
    <script src="vendor/owlcarousel/owl.carousel.min.js"></script>
    <script src="assets/js/main.js"></script>
    
</body>
</html>
    <script>
	
	<?php if($total_data['error']){?>
	
		alert("Oturum Süresi sona erdi. Lütfen Tekrar seçim yapınız");
		window.location.href = "/";
	<?php } ?>
	
		$('#confirm-order').on('click',function(){
			$('.alert.alert-danger').remove();
			<?php if($_SESSION['form_data']['type'] == 'creditcart'){?>
				if(!$('#input7').val() || !$('#input8').val() || !$('select[name="exp_month"] option:selected').val() || !$('select[name="exp_year"] option:selected').val() || !$('input[name="cvv"]').val()){
					$("#confirm-order").before('<div class="alert alert-danger">Kart Bilgilerinizi Doldurunuz</div>');
					return false;
				}
			<?php } ?>
			
			$('#customer-form').submit();
			
		})
		
		$("#customer-form").submit(function(e) {

			//prevent Default functionality
			e.preventDefault();
			
			if(validateForm()){
				if ($('#customCheck1').is(':checked')) {
			$.ajax({
				url: '/erboymobil/functions.php?type=saveForm',
				type: 'post',
				data: $("#customer-form").serialize(),
				dataType: 'json',
				success: function(response) {
					if(response.status == 'success'){
						window.location.href = '/erboymobil/siparis-onay.php';
						
					}else{
						$("#confirm-order").before('<div class="alert alert-danger">'+response.message+'</div>');
					}
					
				}
			});
			
			}else{alert("Sözleşmeyi kabul ediniz");
				return false;}
			
			}else{
				alert(" Tüm alanları doldurunuz");
				return false;
			}
		});
		
		function validateForm(){
			if(!$('#customer-form input[required=""]').val()){
				return false;
			}else{
				return true;
			}
			
		}
		
				$(".csion").change(function(){
			var val = $(this).val();
			if(val == 'company'){
				$(".crs").show();
			}else{
				$(".crs").hide();
			}
		})
		
				$(".sz").scroll(function(){
			if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
				$("#customCheck1").removeAttr('disabled');
			}
		})
		
	</script>
</body>
</html>