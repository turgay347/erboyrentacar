<?php
ob_start();
session_start();
require_once "api.php";
$api = new api();
$search = $api->Search($_SESSION['form_data']);
date_default_timezone_set('Europe/Istanbul');
$time = new DateTime("00:00");
    $close = new DateTime('24:00');
    while ($time < $close) {
        $day[] = $time->format('H:i');
        $time->modify('+15 minutes');
    }
$time = date('H',strtotime(date("H:i")) + 3600);




?>
<?php if(!$_SESSION['form_data']){?>
	<script>
		alert("Oturum Süresi sona erdi. Lütfen Tekrar seçim yapınız");
		window.location.href = "/erboymobil";
	</script>
<?php } ?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erboy</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="assets/fonts/Montserrat/stylesheet.css">
		<style>
	.vehicle-filter li.selected {
    background-color: #037b00;
    color: #fff !important;
}
	ul.vehicle-filter {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    list-style: none;
	padding-left:0px;
	margin-top:10px;
}
	ul.vehicle-filter li {
    margin-right: 15px;
    border: 1px solid #037b00;
    list-style: none;
    color: #000;
    font-size: 16px;
    font-weight: 600;
    padding: 10px 15px;
    display: block;
    text-transform: uppercase;
    cursor: pointer;
}

.list-group{
	width: 100%;
    overflow-x: scroll;
}

.dcs{
	background:#333 !important;
	color:#fff;
}
	</style>
</head>
<body>

   <div class="em-app">
        <div class="container-fluid">

            <div class="em-header">
                <a href="#" class="back"><img src="assets/img/back.png" alt="Back"></a>
                <img src="assets/img/logo-search.png" alt="Logo">
                <div class="em-header-title">
                    <h5>Bir araba seç</h5>
                    <a href="#"><img src="assets/img/icon-filter.png" alt="Filter"></a>
                </div>
            </div>

            <div class="em-page-content em-list1">

                <div class="em-list1-date">
				<form id="search-form" action="">
					<div class="row">
					<?php if($_SESSION['form_data']['same_station'] == 'on'){ ?>
						<div class="col-12">
							<div class="content">
								<span class="title"><img src="assets/img/location.png" alt="Station"> Alış/Varış İstasyon</span>
								<span class="date">
									<input type="text" class="form-control" name="from_station_name" value="<?=$_SESSION['form_data']['from_station_name']?>" id="from-station" placeholder="Şehir, Havaalanı, Araç Kiralama...">
								<ul class="liveresult-from"></ul>
								<input type="hidden" id="from-station-code" value="<?=$_SESSION['form_data']['from_station_code']?>" name="from_station_code">
								</span>
							</div>
						</div>
					<?php }else{ ?>
					<div class="col-6">
							<div class="content">
								<span class="title"><img src="assets/img/location.png" alt="Station"> Alış İstasyon</span>
								<span class="date">
									<input type="text" class="form-control" name="from_station_name" value="<?=$_SESSION['form_data']['from_station_name']?>" id="from-station" placeholder="Şehir, Havaalanı, Araç Kiralama...">
								<ul class="liveresult-from"></ul>
								<input type="hidden" id="from-station-code" value="<?=$_SESSION['form_data']['from_station_code']?>" name="from_station_code">
								</span>
							</div>
						</div>
						
						<div class="col-6">
							<div class="content">
								<span class="title"><img src="assets/img/location.png" alt="Station"> Varış İstasyon</span>
								<span class="date">
									<input type="text" class="form-control" name="to_station_name" value="<?=$_SESSION['form_data']['to_station_name']?>" id="to-station" placeholder="Şehir, Havaalanı, Araç Kiralama...">
								<ul class="liveresult-to"></ul>
								<input type="hidden" id="to-station-code" value="<?=$_SESSION['form_data']['to_station_code']?>" name="to_station_code">
								</span>
							</div>
						</div>
					<?php } ?>
					</div>
                    <div class="row">
                        <div class="col-6 border-right">
                            <div class="content">
                                <span class="title"><img src="assets/img/calendar-gray.png" alt="Calendar"> Alış Tarihi</span>
                                <span class="date">
								<input type="text" id="tarihM1" value="<?=$_SESSION['form_data']['startdate']?>" name="startdate" class="form-control dcs" placeholder="gg/aa/yyyy" autocomplete="off">
								<select class="form-control" id="saat1" name="startclock">
											   <?php foreach($day as $d){ ?>
                                                <option <?=$d == $_SESSION['form_data']['startclock'] ? 'selected':''?> value="<?php echo $d; ?>"><?php echo $d; ?></option>
											<?php } ?>
										</select></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="content">
                                <span class="title"><img src="assets/img/calendar-gray.png" alt="Calendar"> Bırakış Tarihi</span>
                                <span class="date">
								<input type="text" id="tarihM2" value="<?=$_SESSION['form_data']['enddate']?>" name="enddate" class="form-control dcs" placeholder="gg/aa/yyyy" autocomplete="off">
								<select class="form-control" id="saat1" name="endclock">
											   <?php foreach($day as $d){ ?>
                                                <option <?=$d == $_SESSION['form_data']['endclock'] ? 'selected':''?> value="<?php echo $d; ?>"><?php echo $d; ?></option>
											<?php } ?>
										</select>
								</span>
                            </div>
                        </div>
                    </div>
					<div class="row">
						<div class="col-12">
							<div class="content">
								<span class="date">
									<button type="submit" class="btn btn-success btn-block"><i class="fa fa-search"></i> Filtrele</button>
								</span>
							</div>
						</div>
					</div>
					</form>
                </div>

                <!--div class="em-list-filter">
                    <ul class="nav nav-pills" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="yeni-tab" data-toggle="tab" href="#yeni" role="tab" aria-controls="yeni" aria-selected="true">Yeni</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link disabled" id="populer-tab" data-toggle="tab" href="#populer" role="tab" aria-controls="populer" aria-selected="false">Popüler</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link disabled" id="dusuk-tab" data-toggle="tab" href="#dusuk" role="tab" aria-controls="dusuk" aria-selected="false">Düşük Fiyat</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link disabled" id="yuksek-tab" data-toggle="tab" href="#yuksek" role="tab" aria-controls="yuksek" aria-selected="false">Yüksek Fiyat</a>
                        </li>
                    </ul>
                </div-->
				
				
<div class="row-fluid">
				<div class="col-lg-12">
					<div class="list-group">
        <ul class="vehicle-filter">
            <li class="all selected" id="allSelect">Hepsi</li>
            <li class="fuel" data="DIZEL" onclick="FilterFuelType()">Dizel</li>
            <li class="fuel" data="BENZIN">Benzin</li>
            <li class="transmission" data="OTOMATIK">Otomatik</li>
            <li class="transmission" data="MANUEL">Manuel</li>
        </ul>
    </div>
				</div>
			</div>
                <div class="tab-content">

                    <div class="tab-pane active fade show" id="yeni" role="tabpanel">
                        <div class="em-list1-cars">

							<?php if($search['cars']){?>
								<?php foreach($search['cars'] as $cars){?>
								<a href="#" class="em-link er-car selectcart" data-fuel-type="<?php echo $cars['fuel_type'];?>" data-transmission-type="<?php echo $cars['transmission_type'];?>" data-token="<?php echo $cars['_token'];?>">
									<div class="em-box-horizontal">
										<div class="top">
											<img src="<?php echo $cars['image_url'];?>" alt="<?php echo $cars['model'];?>" class="img-car">
											<div class="content">
												<span class="name"><?php echo $cars['model'];?></span>
												<span class="group">Araç Grubu <?php echo $cars['group'];?></span>
												<ul class="list-unstyled">
													<li><img src="assets/img/mini-oil.png" alt="Oil"> <?php echo $cars['fuel_type'];?></li>
													<li><img src="assets/img/mini-air.png" alt="Air"> Klimalı</li>
													<li><img src="assets/img/mini-wh.png" alt="WH"> <?php echo $cars['transmission_type'];?></li>
													<li><img src="assets/img/mini-location.png" alt="Location"> <?php echo $cars['daily_km'];?> km.</li>
												</ul>
											</div>
										</div>
										<div class="bottom">
											<div class="left"><span class="day"><?php echo $search['rentalDays'];?> Gün</span></div>
											<div class="right">
												<span class="price">
													<span class="past"><?php echo number_format($cars['price']*$search['rentalDays'],2);?>  TL</span>
													<span class="now"><?php echo number_format($cars['discounted_daily_price']*$search['rentalDays'],2);?> TL</span>
												</span>
											</div>
										</div>
									</div>
								</a>
								<?php } ?>
							<?php }else{ ?>
								<div class="alert alert-primary"> Seçtiğiniz kriterlere ait araç bulunamadı </div>
							<?php } ?>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="populer" role="tabpanel">
                        <div class="em-list1-cars">

                            <a href="#" class="em-link">
                                <div class="em-box-horizontal">
                                    <div class="top">
                                        <img src="assets/img/car1.png" alt="Car" class="img-car">
                                        <div class="content">
                                            <span class="name">RENAULT SYMBOL</span>
                                            <span class="group">Araç Grubu CDMR</span>
                                            <ul class="list-unstyled">
                                                <li><img src="assets/img/mini-oil.png" alt="Oil"> Benzin</li>
                                                <li><img src="assets/img/mini-air.png" alt="Air"> Klimalı</li>
                                                <li><img src="assets/img/mini-wh.png" alt="WH"> Manuel</li>
                                                <li><img src="assets/img/mini-location.png" alt="Location"> 3000 km.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="bottom">
                                        <div class="left"><span class="day">4 Gün</span></div>
                                        <div class="right">
                                            <span class="price">
                                                <span class="past">1,554,29 TL</span>
                                                <span class="now">854,99 TL</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </a>

                        </div>
                    </div>

                    <div class="tab-pane fade" id="dusuk" role="tabpanel">
                        <div class="em-list1-cars">

                            <a href="#" class="em-link">
                                <div class="em-box-horizontal">
                                    <div class="top">
                                        <img src="assets/img/car1.png" alt="Car" class="img-car">
                                        <div class="content">
                                            <span class="name">RENAULT SYMBOL</span>
                                            <span class="group">Araç Grubu CDMR</span>
                                            <ul class="list-unstyled">
                                                <li><img src="assets/img/mini-oil.png" alt="Oil"> Benzin</li>
                                                <li><img src="assets/img/mini-air.png" alt="Air"> Klimalı</li>
                                                <li><img src="assets/img/mini-wh.png" alt="WH"> Manuel</li>
                                                <li><img src="assets/img/mini-location.png" alt="Location"> 3000 km.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="bottom">
                                        <div class="left"><span class="day">4 Gün</span></div>
                                        <div class="right">
                                            <span class="price">
                                                <span class="past">1,554,29 TL</span>
                                                <span class="now">854,99 TL</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </a>

                        </div>
                    </div>

                    <div class="tab-pane fade" id="yuksek" role="tabpanel">
                        <div class="em-list1-cars">

                            <a href="#" class="em-link">
                                <div class="em-box-horizontal">
                                    <div class="top">
                                        <img src="assets/img/car1.png" alt="Car" class="img-car">
                                        <div class="content">
                                            <span class="name">RENAULT SYMBOL</span>
                                            <span class="group">Araç Grubu CDMR</span>
                                            <ul class="list-unstyled">
                                                <li><img src="assets/img/mini-oil.png" alt="Oil"> Benzin</li>
                                                <li><img src="assets/img/mini-air.png" alt="Air"> Klimalı</li>
                                                <li><img src="assets/img/mini-wh.png" alt="WH"> Manuel</li>
                                                <li><img src="assets/img/mini-location.png" alt="Location"> 3000 km.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="bottom">
                                        <div class="left"><span class="day">4 Gün</span></div>
                                        <div class="right">
                                            <span class="price">
                                                <span class="past">1,554,29 TL</span>
                                                <span class="now">854,99 TL</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </a>

                        </div>
                    </div>

                </div>

            </div>

            <?php include('_inc/menu.php'); ?>

        </div> 
   </div>

    <script src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>
    <script src="vendor/owlcarousel/owl.carousel.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
	
	$("#search-form").on("submit", function (e) {
				
                e.preventDefault();
				var start_date = $('#tarihM1').val();
				var end_date = $('#tarihM2').val();
				
				
				if(!$('#from-station-code').val()){
					alert("Teslim yeri seçilmelidir");
					return false;
					
				}

				
				
				if(!end_date || !start_date ){
					
					alert("Teslim tarihi ve alış tarihi seçilmelidir");
					return false;
				}
				
				var dates1 = start_date.split("-");
				var st_date = dates1[1]+"/"+dates1[0]+"/"+dates1[2];
				var start_date_time = new Date(st_date).getTime();
				
				var dates2 = end_date.split("-");
				var en_date = dates2[1]+"/"+dates2[0]+"/"+dates2[2];
				var end_date_time = new Date(en_date).getTime();
				
				

				if(end_date_time <= start_date_time ){
					alert("Teslim tarihi alış tarihinden önce olamaz");
					return false;
				}
				/*
								var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
var dateTime = date+' '+time;
var allStartTime = start_date +" "+ start_date_time;

console.log(dateTime);
console.log(allStartTime);

return false;*/

                $.ajax({
                    url: "functions.php?type=submit",
                    type: "post",
                    data: $("#search-form").serialize(),
                    dataType: "json",
                    success: function (response) {
                        if (response.success == "true") {
                            window.location.href = response.redirect;
                        }
                    },
                });
            });
	
	var selectedFuel = '';
	var selectedTransmission = '';
	
	$(".fuel").click(function(){
		$(".all").removeClass('selected');
		$(".fuel").removeClass('selected');
		$(this).addClass('selected');
		var val = $(this).attr('data');
		selectedFuel = val;
		if(selectedTransmission != ''){
			$('.er-car[data-fuel-type="'+val+'"][data-transmission-type="'+selectedTransmission+'"]').show();
			$('.er-car[data-fuel-type!="'+val+'"]').hide();
		}else{
			$('.er-car[data-fuel-type="'+val+'"]').show();
			$('.er-car[data-fuel-type!="'+val+'"]').hide();
		}
		
	});
	
	$(".transmission").click(function(){
		$(".all").removeClass('selected');
		$(".transmission").removeClass('selected');
		$(this).addClass('selected');
		var val = $(this).attr('data');
		selectedTransmission = val;
		if(selectedFuel != ''){
			$('.er-car[data-transmission-type="'+val+'"][data-fuel-type="'+selectedFuel+'"]').show();
			$('.er-car[data-transmission-type!="'+val+'"]').hide();
		}else{
			$('.er-car[data-transmission-type="'+val+'"]').show();
			$('.er-car[data-transmission-type!="'+val+'"]').hide();
		}
	});
	
	$(".all").click(function(){
		$(".fuel").removeClass('selected');
		$(".transmission").removeClass('selected');
		$(".all").addClass('selected');
		$('.er-car').show();
	})
	
		$('.selectcart').on('click',function(e){
			var token = $(this).data('token');
			
			e.preventDefault();
			
			$.ajax({
				url: '/erboymobil/functions.php?type=selectcar',
				type: 'post',
				data: {token:token},
				dataType: 'json',
				success: function(response) {
					if(response.success == 'true'){
						window.location.href = response.redirect;
					}
					
				}
			});
			
			
			console.log(token);
			
		});
	</script>
</body>
</html>