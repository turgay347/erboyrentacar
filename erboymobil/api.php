<?php
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	require '../vendor/autoload.php';

	
	
    class api
    {
        public $url;
        /*public function getStations($name = ''){

            $this->url = "carsysapi.erboycar.com.tr:3224/stations";
            $stations = $this->getData();
                        
            $return_stations = array();
            foreach($stations as $station){
                if($name){
                    if(strpos($station['name'],mb_strtoupper($name)) !== false){
                        $return_stations[] = array(
                            '_id' => $station['_id'],
                            'name' => $station['name'],
                            'code' => $station['code'],
                        );
                    }
                    
                }else{
                    $return_stations[] = array(
                        '_id' => $station['_id'],
                        'name' => $station['name'],
                        'code' => $station['code'],
                    );
                }
            }

            $html = '';
            if($return_stations){
                foreach($return_stations as $return_station){
                    $html .= '<li><a data-code="'.$return_station['code'].'">'.$return_station['name'].'</a></li>';
                }
            }

            return $html;
        }*/
		
		 public function getStations($name = ''){

            /*$this->url = "carsysapi.erboycar.com.tr:3224/stations";
            $stations = $this->getData();
                        
            $return_stations = array();
            foreach($stations as $station){
                if($name){
                    if(strpos($station['name'],mb_strtoupper($name)) !== false){
                        $return_stations[] = array(
                            '_id' => $station['_id'],
                            'name' => $station['name'],
                            'code' => $station['code'],
                        );
                    }
                    
                }else{
                    $return_stations[] = array(
                        '_id' => $station['_id'],
                        'name' => $station['name'],
                        'code' => $station['code'],
                    );
                }
            }

            $html = '';
            if($return_stations){
                foreach($return_stations as $return_station){
                    $html .= '<li><a data-code="'.$return_station['code'].'">'.$return_station['name'].'</a></li>';
                }
            }

            return $html;*/
			
			$stations = file_get_contents("http://carsysapi.erboycar.com.tr:3224/stations");
	$stations = json_decode($stations,TRUE);
	 $return_stations = array();
	 
		$sonuclar = array();
		$alttakiler = array();
		
	 
            foreach($stations as $station){
                if($name){
                    if(strpos($station['name'],mb_strtoupper($name)) !== false){
                        $sonuclar[] = array(
                            '_id' => $station['_id'],
                            'name' => $station['name'],
                            'code' => $station['code'],
                        );
						
						
                    }else{
						$alttakiler[] = array(
                            '_id' => $station['_id'],
                            'name' => $station['name'],
                            'code' => $station['code'],
                        );
					}
                    
                }else{
                    $return_stations[] = array(
                        '_id' => $station['_id'],
                        'name' => $station['name'],
                        'code' => $station['code'],
                    );
					
                }
            }

			
			
			
			if($name != ""){$return_stations = array_merge($sonuclar,$alttakiler); }

            $html = '';
            if($return_stations){
                foreach($return_stations as $return_station){
                    $html .= '<li><a data-code="'.$return_station['code'].'">'.$return_station['name'].'</a></li>';
                }
            }

            return $html;
        }
		
		public function Search($data = array()){
			
			$dropstation = '';
			if($data['to_station_code'] && $data['to_station_code'] != $data['from_station_code']){
				$dropstation = '&drop_station='.$data['to_station_code'];
			}
			
			$data['startdate'] = date('Y-m-d',strtotime($data['startdate']));
			$data['enddate'] = date('Y-m-d',strtotime($data['enddate']));
			$this->url = 'carsysapi.erboycar.com.tr:3224/search?station='.$data['from_station_code'].$dropstation.'&pickupDateTime='.$data['startdate'].'%20'.$data['startclock'].'&dropDateTime='.$data['enddate'].'%20'.$data['endclock'];
			
			$results = $this->getData();

			foreach($results['availableVehicles'] as $vehicles){			
				$brands[$vehicles['brand']] = $vehicles['brand'];
				
				$cars[] = array(
					'_id' => $vehicles['_id'],
					'group' => $vehicles['group'],
					'model' => $vehicles['model'],
					'image_url' => $vehicles['image_url'],
					'brand' => $vehicles['brand'],
					'count' => $vehicles['count'],
					'price' => $vehicles['price'],
					'discounted_daily_price' => $vehicles['discounted_daily_price'],
					'number_of_passengers' => $vehicles['number_of_passengers'],
					'number_of_doors' => $vehicles['number_of_doors'],
					'driver_age' => $vehicles['driver_age'],
					'min_licence_age' => $vehicles['min_licence_age'],
					'deposit' => $vehicles['deposit'],
					'daily_km' => $vehicles['daily_km'],
					'max_km' => $vehicles['max_km'],
					'transmission_type' => $vehicles['transmission_type'],
					'fuel_type' => $vehicles['fuel_type'],
					'_token' => $vehicles['_token'],
				);
			}
			
			return array(
				'cars' => $cars,
				'brands' => $brands,
				'rentalDays' => $results['rentalDays']
			);
			
		}
		
		public function getExtras($data = array()){
			$this->url = 'carsysapi.erboycar.com.tr:3224/get-extra';
			
			return $this->getData();
			
		} 
		
		public function Total($data = array()){
			
			$this->url = "carsysapi.erboycar.com.tr:3224/calculate-price";
			
			$request['_token'] = $data['car_token'];			if(isset($data['extras'])){
			foreach($data['extras'] as $id => $status){
				if($status == '1'){
					$request['extra'][] = ['_id' => $id,'qty' => 1];
				}
			}			}
			
			$response = $this->getData(json_encode($request));
			$_SESSION['form_data']['car_detail'] = $response;
			return $response;
			
		}
		
		public function saveDb($data = array(),$resNo = "") {

			$reelData = json_decode(base64_decode($data['reeldata']),TRUE);

			$aracDetay = $reelData['carData']['vehicle'];
			$digerDetay = $reelData['carData'];
			$aracDetayText = $aracDetay['model']." / ".$aracDetay['transmission_type']." / ".$aracDetay['fuel_type'];
			$baslangicTarih = $digerDetay['isoPickupDateTime'];
			$bitisTarih = $digerDetay['isoDropDateTime'];
			$alisistasyon = $digerDetay['pickupStation'];
			$varisistasyon = $alisistasyon;
			$topucret = $reelData['extraTotalPrice'] + $reelData['carRentalPrice'];
			
			
		    try {
                 $db = new PDO("mysql:host=localhost;dbname=erboyrentacar_ilkay;charset=utf8", "erboyrentacar_ilkaycan", "0#JpFw2ua!*a");
            } catch ( PDOException $e ){
                 print $e->getMessage();
                 echo "\nPDO::errorInfo():\n";
            }
		    $ad = $data['firstname'];
		    $soyad = $data['lastname'];
		    $query = $db->prepare("INSERT INTO siparisler SET
              musteri_adi = :ad,
              musteri_soyadi = :soyad,
              musteri_eposta = :mail,
              musteri_telefon = :telefon,
              araba_bilgi = :arb,
              istasyon_adi = :istad,
              istasyon_kodu = :istkod,
			  baslangic = :baslangic,
			  bitis = :bitis,
			  ucret = :ucret,
			  musteri_cinsiyet = :musteri_cinsiyet,
			  musteri_tip = :musteri_tip,
			  rezID = :rezID,
			  unvan = :unvan,
			  vergi_dairesi = :vergi_dairesi,
			  vergi_numarasi = :vergi_numarasi,
			  telefon = :telefon,
			  sehir = :sehir,
			  ulke = :ulke,
			  adres = :adres
			  "); 
			  $urr = array(
                    "ad" => $ad,
                    "soyad" => $soyad,
                    "mail" => $data['email'],
                    "telefon" => $data['phone'],
                    "arb" => $aracDetayText,
                    "istad" => $reelData['yer'],
                    "istkod" => $alisistasyon,
                    "baslangic" => $baslangicTarih,
                    "bitis" => $bitisTarih,
                    "ucret" => $topucret,
                    "musteri_cinsiyet" => $data['gender'],
                    "musteri_tip" => $data['customer_type'],
                    "rezID" => $resNo,
					"unvan" => $data['unvan'],
					"vergi_dairesi" => $data['vergi_dairesi'],
					"vergi_numarasi" => $data['vergi_numarasi'],
					"telefon" => $data['telefon'],
					"sehir" => $data['sehir'],
					"ulke" => $data['ulke'],
					"adres" => $data['adres']
              );
              $insert = $query->execute($urr);
			  
			  if(!$insert){
				  return "Oops!";
			  }else{
				  
				  $date1=date_create($baslangicTarih);
				  $date1 = date_format($date1,"d.m.Y H:i");
				  $date2=date_create($bitisTarih);
				  $date2 = date_format($date2,"d.m.Y H:i");
				  return "
					Rezervasyonunuz başarılı bir şekilde alındı! Detaylar mail ile tarafınıza iletildi.<br>
					<b>REZERVASYON BİLGİLERİ</b>
					<p>Rezervasyon Numarası : $resNo</p>
					<p>İstasyon : ".$reelData['yer']."</p>
					<p>İstasyon Kodu : ".$alisistasyon."</p>
					<p>Başlangıç Tarihi : ".$date1."</p>
					<p>Bitiş Tarihi : ".$date2."</p>
					<p>Toplam Ücret : ".$topucret." TL</p>
					<p>Araç : ".$aracDetayText."</p>
					<p>Müşteri Bilgileri : $ad $soyad / ".$data['gender']."</p>
				  ";
			  }
			  
		  
		    
		}
		
		public function saveForm($data = array()){
			
			$this->url = "carsysapi.erboycar.com.tr:3224/save";
			
			$request['_token'] = $_SESSION['form_data']['car_token'];			if(isset($data['extras'])){
			foreach($data['extras'] as $id => $status){
				if($status == '1'){
					$request['extra'][] = ['_id' => $id,'qty' => 1];
				}
			}						}
			$request['customer'] = [
				'first_name' => $data['firstname'],
				'last_name' => $data['lastname'],
				'phone_mobile' => $data['phone'],
				'email' => $data['email'],
			];
			$request['customer_type'] = $data['customer_type'];
			$request['paid'] = 0;
			
			if($data['pan']){
				$request['credit_card'] = [
					'pan' => $data['pan'],
					'cvv' => $data['cvv'],
					'expiry' => substr($data['exp_year'],2,4).$data['exp_month']
				];
			}
			
			if($request['customer_type'] == 'company'){
				$request['company'] = [
					'name' => $data['unvan'],
					'tax_no' => $data['vergi_numarasi'],
					'tax_office' => $data['vergi_dairesi'],
					'phone' => $data['telefon'],
					'address' => $data['adres'] . " ". $data['sehir']. " / ".$data['ulke']
				];
			}
			
			
			$result = $this->getData(json_encode($request));
			// var_dump($result);die();			
			if($result['reservation_status'] == "Success"){
				$_SESSION['lastReservationDetails'] = $this->saveDb($data,$result['summary']['res_no']);
				$mailUsername = "reservation@erboyrentacar.com";
				$mailSifre = "erboyrentacar123";
				$mailHost = "mail.erboyrentacar.com";
				$mailGidenAdres = $data['email'];
				$mailbody = $_SESSION['lastReservationDetails'];

				$mail = new PHPMailer(true);

					$mail->isSMTP();
					$mail->Host       = $mailHost;
					$mail->SMTPAuth   = true;
					$mail->SMTPSecure = "tsl";
					$mail->Username   = $mailUsername;
					$mail->CharSet  = 'utf-8';
					$mail->Password   = $mailSifre;
					$mail->Port       = 587;    


					$mail->setFrom($mailUsername, 'Rezervasyon');
					$mail->addAddress($mailGidenAdres, 'Rezervasyon');     
					$mail->addAddress($mailUsername, 'Rezervasyon');     


					$mail->isHTML(true);                          
					$mail->Subject = 'Erboyrentacar - Rezervasyon';
					$mail->Body    = $mailbody;

					$mail->send();
				// unset($_SESSION['form_data']);
				return array(
					'status' => 'success'
				);
			}else{
				return array(
					'status' => 'alert',
					'message' => 'Bilgilerinizi kontrol ederek tekrar deneyin'
				);
			}
			
		}
		
		
        public function getData($json = ''){


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

            if($json){
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
				curl_setopt($ch, CURLOPT_HTTPHEADER,
					array(
						'Content-Type:application/json',
						'Content-Length: ' . strlen($json)
					)
				);

            }
            
            $output = curl_exec($ch);
            return json_decode($output,true);
        }

    }

?>