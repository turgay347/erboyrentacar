<?php
ob_start();
session_start();
require_once "api.php";
$api = new api();
$total_data = $api->Total($_SESSION['form_data']);
$search = $api->Search($_SESSION['form_data']);

?>
<?php if(!$_SESSION['form_data']){?>
	<script>
		alert("Oturum Süresi sona erdi. Lütfen Tekrar seçim yapınız");
		window.location.href = "/erboymobil";
	</script>
<?php } ?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erboy</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="assets/fonts/Montserrat/stylesheet.css">
</head>
<body>

   <div class="em-app">
        <div class="container-fluid">
            <div class="em-page-content em-detail2">

                <div class="em-detail2-card">

                    <div class="header">
                        <a href="#" class="back"><img src="assets/img/back-dark.png" alt="Back"></a>
                        <div class="badge-free"><img src="assets/img/tick.png" alt="Tick"> Ücretsiz İptal</div>
                    </div>

                    <div class="slider">
                        <div class="owl-carousel owl-theme em-slider-imgs">

                            <div class="item">
                                <img src="<?php echo $total_data['carData']['vehicle']['image_url'];?>" alt="Car">
                            </div>

                        </div>
                    </div>

                    <div class="content">
                        <span class="name"><?php echo $total_data['carData']['vehicle']['model'];?></span>
                        <span class="group">Araç Grubu <?php echo $total_data['carData']['vehicle']['group'];?></span>
                        <span class="daily">Günlük fiyat <?php echo $total_data['carData']['vehicle']['price'];?> TL'dir <span class="quote">Toplam <?php echo $total_data['carData']['rentalDays'];?> Gün</span></span>
                    </div>
    
                    <div class="features">
                        <ul class="list-unstyled">
                            <li>
                                <div class="content">
                                    <div class="alt">
                                        <img src="assets/img/mini-oil.png" alt="Oil"> <?php echo $total_data['carData']['vehicle']['fuel_type'];?>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <div class="alt">
                                        <img src="assets/img/mini-air.png" alt="Oil"> Klimalı
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <div class="alt">
                                        <img src="assets/img/mini-person.png" alt="Oil"> <?php echo $total_data['carData']['vehicle']['number_of_passengers'];?> Kişi
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <div class="alt">
                                        <img src="assets/img/mini-wh.png" alt="Oil"> <?php echo $total_data['carData']['vehicle']['transmission_type'];?> 
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <div class="alt">
                                        <img src="assets/img/mini-location.png" alt="Oil"> <?php echo $total_data['carData']['vehicle']['daily_km'];?> km.
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <div class="alt">
                                        <img src="assets/img/mini-car.png" alt="Oil"> <?php echo $total_data['carData']['vehicle']['number_of_doors'];?> Kapı
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <div class="alt">
                                        <img src="assets/img/mini-person2.png" alt="Oil"> +<?php echo $total_data['carData']['vehicle']['driver_age'];?>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <div class="alt">
                                        <img src="assets/img/mini-person3.png" alt="Oil"> +<?php echo $total_data['carData']['vehicle']['min_licence_age'];?>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <div class="alt">
                                        <img src="assets/img/mini-wallet.png" alt="Oil"> <?php echo $total_data['carData']['vehicle']['deposit'];?> ₺
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
					
					<div class="bottom">
                        <div class="left">
                            <div class="price">
                                <span class="now"><?php echo $total_data['carData']['vehicle']['price']*$total_data['carData']['rentalDays'];?> TL</span>
                            </div>
                        </div>
                        <div class="right">
                            <a class="btn btn-gradient selectcart" data-type="office" data-token="<?php echo $_SESSION['form_data']['car_token'];?>">Ofiste Kirala</a>
                        </div>
                    </div>

                    <div class="bottom mt-2">
                        <div class="left">
                            <div class="price">
                                <span class="past"><?php echo $total_data['carData']['vehicle']['price']*$total_data['carData']['rentalDays'];?> TL</span>
                                <span class="now"><?php echo $total_data['carData']['vehicle']['discounted_daily_price']*$total_data['carData']['rentalDays'];?> TL</span>
                            </div>
                        </div>
                        <div class="right">
                            <a class="btn btn-gradient selectcart" data-type="creditcart" data-token="<?php echo $_SESSION['form_data']['car_token'];?>">Kredi Kartı ile Kirala</a>
                        </div>
                    </div>
					
					

                </div>

                <div class="em-home-section em-detail2-cars">
                
                    <div class="head">
                        <h3>En iyi fırsatlar</h3>
                        <a href="#" class="all">Tümü gör</a>
                    </div>
    
                    <div class="list">
    
						<?php foreach($search['cars'] as $car){?>
						<?php if($car['_token'] != $_SESSION['form_data']['car_token']){?>
                        <a href="#" class="em-link selectcart" data-token="<?php echo $car['_token'];?>">
                            <div class="em-box-vertical">
                                <img src="<?php echo $car['image_url'];?>" alt="Car">
                                <div class="content">
                                    <span class="name"><?php echo $car['model'];?></span>
                                    <span class="group">Araç Grubu <?php echo $car['group'];?></span>
                                    <div class="price">
                                        <span class="past"><?php echo $car['price']*$search['rentalDays'];?> TL</span>
                                        <span class="now"><?php echo $car['discounted_daily_price']*$search['rentalDays'];?> TL</span>
                                    </div>
                                </div>
                            </div>
                        </a>
						<?php } ?>
						<?php } ?>
    
                    </div>
    
                </div>

            </div>
        </div>
   </div>

    <script src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>
    <script src="vendor/owlcarousel/owl.carousel.min.js"></script>
    <script src="assets/js/main.js"></script>
	
	<script>
		$('.selectcart').on('click',function(e){
			var token = $(this).data('token');
			var type = $(this).data('type');
			
			e.preventDefault();
			
			$.ajax({
				url: '/erboymobil/functions.php?type=selectcar',
				type: 'post',
				data: {token:token,type:type},
				dataType: 'json',
				success: function(response) {
					if(response.success == 'true'){
						window.location.href = response.redirect;
					}
					
				}
			});
		});
	</script>
    
</body>
</html>