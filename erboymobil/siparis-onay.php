<?php
ob_start();
session_start();
$session=$_SESSION['form_data'];

// var_dump($session);die();
unset($_SESSION['form_data']);

?>

<?php if(!$session){?>
	<script>
		window.location.href = "/erboymobil";
	</script>
<?php } ?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erboy</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="assets/fonts/Montserrat/stylesheet.css">
</head>
<body>

   <div class="em-app">
        <div class="container-fluid">

            <div class="em-header">
                <img src="assets/img/logo-search.png" alt="Logo">
            </div>

            <div class="em-page-content em-rent">
 
                <div class="em-rent-card em-steps">
                    <div class="row">
                        <div class="col-3">
                            <div class="content">
                                <span class="number active">1</span>
                                <span class="step">Araç Seçimi</span>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="content">
                                <span class="number active">2</span>
                                <span class="step">Araç Ekstraları</span>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="content">
                                <span class="number active">3</span>
                                <span class="step">Müşteri Bilgileri</span>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="content">
                                <span class="number on">4</span>
                                <span class="step">Rzrvsyn Onay</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="em-rent-card er-step-invoice p-4">
                    <h2><?php echo $session['car_detail']['carData']['vehicle']['model'];?></h2>
                    <span class="cdmr">ARAÇ GRUBU <?php echo $session['car_detail']['carData']['vehicle']['group'];?></span>
                    <img src="<?php echo $session['car_detail']['carData']['vehicle']['image_url'];?>" alt="Car" class="img-car">
                    <div class="er-step-info">
                        <span class="text"><strong>Gidiş</strong> Bilgileri</span>
                        <span class="info"><?php echo $session['from_station_name'];?> <div class="divider">|</div> <?php echo $session['startdate'];?> - <?php echo $session['startclock'];?></span>
                    </div>
                    <div class="er-step-info">
                        <span class="text"><strong>Dönüş</strong> Bilgileri</span>
                        <span class="info"><?php echo ($session['to_station_name'])? $session['to_station_name']: $session['from_station_name'];?> <div class="divider">|</div> <?php echo $session['enddate'];?> - <?php echo $session['endclock'];?></span>
                    </div>
                    <ul class="list-unstyled">
                        <li><img src="assets/img/features/oil.png" alt="Icon"> <?php echo $session['car_detail']['carData']['vehicle']['fuel_type'];?></li>
                        <li><img src="assets/img/features/gear.png" alt="Icon"> <?php echo $session['car_detail']['carData']['vehicle']['transmission_type'];?></li>
                        <li><img src="assets/img/features/air.png" alt="Icon"> Klimalı</li>
                        <li><img src="assets/img/features/Profile.png" alt="Icon"> <?php echo $session['car_detail']['carData']['vehicle']['number_of_passengers'];?> Kişi</li>
                        <li><img src="assets/img/features/car.png" alt="Icon"> <?php echo $session['car_detail']['carData']['vehicle']['number_of_doors'];?> Kapı</li>
                        <li><img src="assets/img/features/racing.png" alt="Icon"> <?php echo $session['car_detail']['carData']['vehicle']['daily_km'];?> Km</li>
                        <li><img src="assets/img/features/age.png" alt="Icon"> +<?php echo $session['car_detail']['carData']['vehicle']['driver_age'];?></li>
                        <li><img src="assets/img/features/badge.png" alt="Icon"> +<?php echo $session['car_detail']['carData']['vehicle']['min_licence_age'];?></li>
                        <li><img src="assets/img/features/wallet.png" alt="Icon"> <?php echo $session['car_detail']['carData']['vehicle']['deposit'];?>₺</li>
                    </ul>
                    <table>
                        <tr>
                            <td>Kiralama Süresi</td>
                            <td><?php echo $session['car_detail']['carData']['rentalDays'];?> Gün</td>
                        </tr>
                        <tr>
                            <td>Günlük Fiyat</td>
                            <td><?php echo $session['car_detail']['carData']['vehicle']['discounted_daily_price'];?>₺</td>
                        </tr>
                        <tr>
                            <td>Toplam Kira Bedeli</td>
                            <td><?php echo $session['car_detail']['carRentalPrice'];?>₺</td>
                        </tr>
                        <tr>
                            <td>Ekstra Hizmetler</td>
                            <td><?php echo $session['car_detail']['extraTotalPrice'];?>₺</td>
                        </tr>
                        <tr class="total">
                            <td>Genel Toplam</td>
                            <td><?php echo $session['car_detail']['extraTotalPrice']+$session['car_detail']['carRentalPrice'];?>₺</td>
                        </tr>
                    </table>
                </div>

                    <div class="alert alert-success"> Rezervasyonunuz alınmıştır.</div>

            </div>

        </div>

                       <?php include('_inc/menu.php'); ?>

        </div>
   </div>

    <script src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>
    <script src="vendor/owlcarousel/owl.carousel.min.js"></script>
    <script src="assets/js/main.js"></script>
    
</body>
</html>