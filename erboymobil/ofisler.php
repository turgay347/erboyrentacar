<?php require('db/baglan.php');

$ofisler = $db->query("SELECT * FROM ofisler where dil = 'turkce' order by id  desc ")->fetchAll();
$page = 'ofisler'; 
?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erboy</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="assets/fonts/Montserrat/stylesheet.css">
</head>
<body>

   <div class="em-app">
        <div class="container-fluid">

            <div class="em-header">
                <a href="<?=$previous?>" class="back"><img src="assets/img/back.png" alt="Back"></a>
                <img src="assets/img/logo-search.png" alt="Logo">
                <div class="em-header-title">
                    <h5>Ofislerimiz</h5>
                    <a href="#"><img src="assets/img/icon-filter.png" alt="Filter"></a>
                </div>
            </div>

            <div class="em-page-content em-list1">



                <div class="tab-content">

                    <div class="tab-pane active fade show" id="yeni" role="tabpanel">
                        <div class="em-list1-cars">
<?php 
                    foreach ($ofisler as $key => $value) {
                        ?>
                            <div class="bg-light p-2 mb-4">
                                <div class="row-fluid">
                                    <div class="col-md-10">
                                        <h2><?php echo $value['ad']?> / <?php echo $value['sehir']?></h2>
                                        <p><?php echo $value['adres']?></p>
                                        <p><a href="tel:<?php echo $value['tel']?>"><?php echo $value['tel']?></a></p>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="<?php echo $value['map']?>" class="btn btn-success btn-block"><?php echo haritadaAc ?></a>
                                    </div>
                                </div>
                            </div>
                        <?php
                    }
                ?>

                        </div>
                    </div>


                    

                </div>

            </div>

			<?php include('_inc/menu.php'); ?>

        </div>
   </div>

    <script src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>
    <script src="vendor/owlcarousel/owl.carousel.min.js"></script>
    <script src="assets/js/main.js"></script>
    
</body>
</html>