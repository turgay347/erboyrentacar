<?php
ob_start();
session_start();
require_once "api.php";
$api = new api();
$total_data = $api->Total($_SESSION['form_data']);
?>
<h2><?php echo $total_data['vehicle']['model'];?></h2>
                        <span>ARAÇ GRUBU <?php echo $total_data['carData']['vehicle']['group'];?></span>
                        <img src="<?php echo $total_data['carData']['vehicle']['image_url'];?>" alt="Car" class="img-car">
                        <div class="er-step-info">
                            <span class="text"><strong>Gidiş</strong> Bilgileri</span>
                            <span class="info"><?php echo $_SESSION['form_data']['station_name'];?> <div class="divider">|</div> <?php echo $_SESSION['form_data']['startdate'];?> - <?php echo $_SESSION['form_data']['startclock'];?></span>
                        </div>
                        <div class="er-step-info">
                            <span class="text"><strong>Gidiş</strong> Bilgileri</span>
                            <span class="info"><?php echo $_SESSION['form_data']['station_name'];?> <div class="divider">|</div> <?php echo $_SESSION['form_data']['enddate'];?> - <?php echo $_SESSION['form_data']['endclock'];?></span>
                        </div>
                        
                        <table>
                            <tr>
                                <td>Kiralama Süresi</td>
                                <td><?php echo $total_data['carData']['rentalDays'];?> Gün</td>
                            </tr>
                            <tr>
                                <td>Günlük Fiyat</td>
                                <td><?php echo $total_data['carData']['vehicle']['price'];?>₺</td>
                            </tr>
                            <tr>
                                <td>Toplam Kira Bedeli</td>
                                <td><?php echo $total_data['carRentalPrice'];?>₺</td>
                            </tr>
                            <tr>
                                <td>Ekstra Hizmetler</td>
                                <td><?php echo $total_data['extraTotalPrice'];?>₺</td>
                            </tr>
                            <tr class="total">
                                <td>Genel Toplam</td>
                                <td><?php echo ($total_data['carRentalPrice']+$total_data['extraTotalPrice']);?>₺</td>
                            </tr>
                        </table>