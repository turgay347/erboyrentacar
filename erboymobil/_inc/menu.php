            <div class="em-nav">

                <div class="row">

                    <div class="col-3">

                        <div class="em-nav-link <?php if($page == 'anasayfa'){ echo 'active'; } ?>">

                            <!--span class="icon icon-nav-home"></span-->
							<a href="index.php"><span class="fas fa-home d-block" style="font-size:19px;"></span>

							 <span class="name">Anasayfa</span></a>
                        </div>

                    </div>

                    <div class="col-3">

                        <div class="em-nav-link <?php if($page == 'araclar'){ echo 'active'; } ?>">

                            <a href="arac-listesi.php"><span class="fas fa-car d-block" style="font-size:19px;"></span>

                            <span class="name">Araçlarımız</span></a>

                        </div>

                    </div>

                    <div class="col-3">

                        <div class="em-nav-link">

                            <span class="fab fa-whatsapp d-block" style="font-size:19px;"></span>

                            <a href="https://api.whatsapp.com/send?phone=+90 850 255 0 255Whatsapp&amp;data=&amp;app_absent="> <span class="name">Destek</span></a>

                        </div>

                    </div>

                    <div class="col-3">

                        <div class="em-nav-link <?php if($page == 'ofisler'){ echo 'active'; } ?>">

                            <a href="ofisler.php"><span class="fas fa-building d-block" style="font-size:19px;"></span>

                             <span class="name">Ofislerimiz</span></a>

                        </div>

                    </div>

                </div>

            </div>
