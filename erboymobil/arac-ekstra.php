<?php
ob_start();
session_start();
require_once "api.php";
$api = new api();
$extras = $api->getExtras($_SESSION['form_data']);

?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erboy</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="assets/fonts/Montserrat/stylesheet.css">
</head>
<body>

   <div class="em-app">
        <div class="container-fluid">

            <div class="em-header">
                <img src="assets/img/logo-search.png" alt="Logo">
            </div>

            <div class="em-page-content em-rent">

                <div class="em-rent-card em-steps">
                    <div class="row">
                        <div class="col-3">
                            <div class="content">
                                <span class="number active">1</span>
                                <span class="step">Araç Seçimi</span>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="content">
                                <span class="number on">2</span>
                                <span class="step">Araç Ekstraları</span>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="content">
                                <span class="number">3</span>
                                <span class="step">Müşteri Bilgileri</span>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="content">
                                <span class="number">4</span>
                                <span class="step">Rzrvsyn Onay</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="em-rent-card p-4">

                    <div class="er-setting-box hidden" style="display:none;">
                        <h5 class="er-setting-label">Seçilebilecek Sigortalar</h5>

                        <div class="er-setting">
                            <div class="left">
                                <div class="name"><strong>Full Hasar Güvencesi</strong> / Günlük</div>
                                <p>1500 TL ye kadar oluşan hasarlarda kiracı beyanı ile onarım sağlanma avantajı sunan güvencesidir. Lastik (1 adet), Cam ve Far (LCF) dahildir.</p>
                            </div>
                            <div class="right">
                                <div class="price">30₺</div>
                                <a href="#" class="btn btn-outline-success">Ekle</a>
                            </div>
                        </div>

                        <div class="er-setting">
                            <div class="left">
                                <div class="name"><strong>Muafiyetsiz Sigorta</strong> / Günlük</div>
                                <p>Maddi hasarlı kazalarda 1000TL muafiyet bedelini ödememeniz için satın almanız gerekir.</p>
                            </div>
                            <div class="right">
                                <div class="price">10₺</div>
                                <a href="#" class="btn btn-success">Eklenmiş</a>
                            </div>
                        </div>

                        <div class="er-setting">
                            <div class="left">
                                <div class="name"><strong>Lastik-Cam-Far Sigortası</strong> / Günlük</div>
                                <p>Kaza durumu olmaksızın lastik-cam-far oluşabilecek hasarların karşılanma güvencesidir..</p>
                            </div>
                            <div class="right">
                                <div class="price">7₺</div>
                                <a href="#" class="btn btn-success">Eklenmiş</a>
                            </div>
                        </div>

                    </div>

                    <div class="er-setting-box">
                        <h5 class="er-setting-label">Seçilebilecek Ekstralar</h5>
						<?php  foreach($extras as $extra){?>
                        <div class="er-setting mini">
                            <div class="left">
                                <div class="name"><strong><?php echo $extra['name_tr'];?></strong></div>
                                <div class="price"><?php echo $extra['price_tl'];?>₺</div>
                            </div>
                            <div class="right">
                                <?php if($_SESSION['form_data']['extras'][$extra['_id']]){?>
											<a href="#" data-id="<?php echo $extra['_id'];?>" data-status="true" class="btn btn-secondary extrabtn"> Çıkar</a>
											<input type="hidden" name="extras[<?php echo $extra['_id'];?>]" value="1">
										<?php }else{?>
											<a href="#" data-id="<?php echo $extra['_id'];?>" data-status="false" class="btn btn-success extrabtn"> Ekle</a>
											<input type="hidden" name="extras[<?php echo $extra['_id'];?>]" value="0">
										
										<?php } ?>
                            </div>
                        </div>
						<?php } ?>

                        
                    </div>

                </div>
				
				                <div class="em-rent-card p-4">
<div class="er-card er-step-invoice" id="order_summary">
					
                    </div>
								</div>
				
				 

                <a href="/erboymobil/siparis-bilgileri.php" class="btn btn-primary btn-block btn-devam">Devam Et</a>

            </div>

            <?php include('_inc/menu.php'); ?>

        </div>
   </div>

    <script src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>
    <script src="vendor/owlcarousel/owl.carousel.min.js"></script>
    <script src="assets/js/main.js"></script>
    
</body>
</html>
	<script>
		$('#order_summary').load('order_summary.php');
	
			
		$('.extrabtn').on('click',function(e){
			e.preventDefault();
			
			var status = $(this).data('status');
			var id = $(this).data('id');
			console.log(status);
			if(status == false){
				$(this).removeClass('btn-success').addClass('btn-secondary');
				$('input[name="extras['+id+']"]').val('1');
				$(this).data('status',true);
				$(this).text("Çıkar");
			}else if(status == true){console.log("test");
				$(this).removeClass('btn-secondary').addClass('btn-success');
				$('input[name="extras['+id+']"]').val('0');
				$(this).data('status',false);
				$(this).text("Ekle");
			}
			
			$.ajax({
				url: '/erboymobil/functions.php?type=selectextras',
				type: 'post',
				data: $('input[name*="extras"]').serialize(),
				dataType: 'json',
				success: function(response) {
					if(response.success == 'true'){
						$('#order_summary').load('/erboymobil/order_summary.php');
					}
					
				}
			});
			
		});
		
		
	</script>
	
	
</body>
</html>