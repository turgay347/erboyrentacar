<?php echo !defined("guvenlik") ? die("Hata") : null;

$sayfaDetay= blogSorgu($v);
$pageTitle = $sayfaDetay->baslik;
require('header.php');
require('headerIc.php');

$bloglar = $db->query("SELECT * FROM bloglar where dil = '$dil' order by id desc limit 0 , 10")->fetchAll();
?>


<section class="er-blog-page">
        <img src="<?php echo ayargetir('domain','turkce')?>i/site/<?php echo $sayfaDetay->resim?>" alt="<?php echo $sayfaDetay->baslik?>" class="er-bp-img">
        <div class="container">
            
            

            <div class="content">

                <h1><?php echo $sayfaDetay->baslik?></h1>
                <?php echo $sayfaDetay->icerik?>


            </div>

            <div class="share">
                <span><?php echo share ?></span>
                <a target="_blank" href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo $url?>" class="linkedin"><i class="fab fa-linkedin-in"></i></a>
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url?>" class="facebook"><i class="fab fa-facebook-f"></i></a>
                <a target="_blank" href="https://twitter.com/intent/tweet?url=<?=urlencode($url)?>" class="twitter"><i class="fab fa-twitter"></i></a>
            </div>

        </div>
    </section>

   


<?php include('footer.php');