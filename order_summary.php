<?php
ob_start();
session_start();
require_once "api.php";
$api = new api();
$total_data = $api->Total($_SESSION['form_data']);

?>
<h2><?php echo $total_data['vehicle']['model'];?></h2>
                        <span>ARAÇ GRUBU <?php echo $total_data['carData']['vehicle']['group'];?></span>
                        <img src="<?php echo $total_data['carData']['vehicle']['image_url'];?>" alt="Car" class="img-car">
                        <div class="er-step-info">
                            <span class="text"><?php echo info1; ?></span>
                            <span class="info"><?php echo $_SESSION['form_data']['station_name'];?> <div class="divider">|</div> <?php echo $_SESSION['form_data']['startdate'];?> - <?php echo $_SESSION['form_data']['startclock'];?></span>
                        </div>
                        <div class="er-step-info">
                            <span class="text"><?php echo info2; ?></span>
                            <span class="info"><?php echo $_SESSION['form_data']['station_name'];?> <div class="divider">|</div> <?php echo $_SESSION['form_data']['enddate'];?> - <?php echo $_SESSION['form_data']['endclock'];?></span>
                        </div>
                        <ul class="list-unstyled">
                            <li><img src="assets/img/features/oil.png" alt="Icon"> <?php echo $total_data['carData']['vehicle']['fuel_type'];?></li>
                            <li><img src="assets/img/features/gear.png" alt="Icon"> <?php echo $total_data['carData']['vehicle']['transmission_type'];?></li>
                            <li><img src="assets/img/features/air.png" alt="Icon"> Klimalı</li>
                            <li><img src="assets/img/features/profile.png" alt="Icon"> <?php echo $total_data['carData']['vehicle']['number_of_passengers'];?> Kişi</li>
                            <li><img src="assets/img/features/car.png" alt="Icon"> <?php echo $total_data['carData']['vehicle']['number_of_doors'];?> Kapı</li>
                            <li><img src="assets/img/features/racing.png" alt="Icon"> <?php echo $total_data['carData']['vehicle']['daily_km'];?> Km</li>
                            <li><img src="assets/img/features/age.png" alt="Icon"> +<?php echo $total_data['carData']['vehicle']['driver_age'];?></li>
                            <li><img src="assets/img/features/badge.png" alt="Icon"> +<?php echo $total_data['carData']['vehicle']['min_licence_age'];?></li>
                            <li><img src="assets/img/features/wallet.png" alt="Icon"> <?php echo $total_data['carData']['vehicle']['price'];?> <?php echo $sembol; ?></li>
                        </ul>
                        <table>
                            <tr>
                                <td><?php echo info3; ?></td>
                                <td><?php echo $total_data['carData']['rentalDays'];?> Gün</td>
                            </tr>
                            <tr>
                                <td><?php echo info4; ?></td>
                                <td><?php echo number_format($total_data['carData']['vehicle']['price']/$oran,2);?> <?php echo $sembol; ?></td>
                            </tr>
                            <tr>
                                <td><?php echo info5; ?></td>
                                <td><?php echo number_format($total_data['carRentalPrice']/$oran,2);?> <?php echo $sembol; ?></td>
                            </tr>
                            <tr>
                                <td><?php echo info6; ?></td>
                                <td><?php echo number_format($total_data['extraTotalPrice']/$oran,2);?> <?php echo $sembol; ?></td>
                            </tr>
                            <tr class="total">
                                <td><?php echo info7; ?></td>
                                <td><?php echo number_format(($total_data['carRentalPrice']+$total_data['extraTotalPrice'])/$oran,2);?> <?php echo $sembol; ?></td>
                            </tr>
                        </table>