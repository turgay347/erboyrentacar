<?
date_default_timezone_set('Europe/Istanbul');
setlocale(LC_ALL, 'tr_TR.UTF-8', 'tr_TR', 'tr', 'turkish');
$bugun = time();

$diller = array('turkce,ingilizce,almanca,arapca');
require "libarry/classupload/class.upload.php";
$iller = array('Adana', 'Adıyaman', 'Afyon', 'Ağrı', 'Amasya', 'Ankara', 'Antalya', 'Artvin',
'Aydın', 'Balıkesir', 'Bilecik', 'Bingöl', 'Bitlis', 'Bolu', 'Burdur', 'Bursa', 'Çanakkale',
'Çankırı', 'Çorum', 'Denizli', 'Diyarbakır', 'Edirne', 'Elazığ', 'Erzincan', 'Erzurum', 'Eskişehir',
'Gaziantep', 'Giresun', 'Gümüşhane', 'Hakkari', 'Hatay', 'Isparta', 'Mersin', 'İstanbul', 'İzmir', 
'Kars', 'Kastamonu', 'Kayseri', 'Kırklareli', 'Kırşehir', 'Kocaeli', 'Konya', 'Kütahya', 'Malatya', 
'Manisa', 'Kahramanmaraş', 'Mardin', 'Muğla', 'Muş', 'Nevşehir', 'Niğde', 'Ordu', 'Rize', 'Sakarya',
'Samsun', 'Siirt', 'Sinop', 'Sivas', 'Tekirdağ', 'Tokat', 'Trabzon', 'Tunceli', 'Şanlıurfa', 'Uşak',
'Van', 'Yozgat', 'Zonguldak', 'Aksaray', 'Bayburt', 'Karaman', 'Kırıkkale', 'Batman', 'Şırnak',
'Bartın', 'Ardahan', 'Iğdır', 'Yalova', 'Karabük', 'Kilis', 'Osmaniye', 'Düzce');

@$p     = $_GET["p"];
@$v=$_GET["v"];
$pdir   = 'tema/';

$ayarlar = dilAyarlar('turkce');
$ayarlarEn = dilAyarlar('ingilizce');
$ayarlarArap = dilAyarlar('arapca');
$ayarlarAlmanca = dilAyarlar('almanca');

 // print_r($ayarlarAlmanca);
function dilAyarlar($dil){
  global $db;
  $ayarlar = null;
  $ayarlar = $db->query("select * from ayarlar where dil = '$dil' ")->fetchAll(); 
  return  $ayarlar;
}
function ayargetir($deger = "all", $dil = null) {

  global $ayarlar ;
  global $ayarlarEn;
  global $ayarlarArap ;
  global $ayarlarAlmanca ;

  if ($dil != null) {
    if ($dil == 'ingilizce') {
      $ayarlar = $ayarlarEn;
    }
    elseif ($dil == 'almanca') {
      $ayarlar = $ayarlarAlmanca;
    }
    elseif ($dil == 'turkce') {
      $ayarlar = $ayarlar;
    }
    elseif ($dil == 'arapca') {
      $ayarlar = $ayarlarArap;
    }
  }
  else {
    global $ayarlar;
    
  }

  if ($deger == 'all') {
    $ayar = $ayarlar;
  }
  else {
    if (!empty($ayarlar)) {
      foreach ($ayarlar as $key => $item) {

        if (array_search($deger,$item)) {
          $ayar= $item['ayar'];
          $ayar = html_entity_decode($ayar, ENT_QUOTES,"UTF-8");
        }
        if (empty($ayar)) {
          $ayar = null;
        }
      }
    }
    else {
      return '';
    }
  }
  return $ayar;
}

function formDilSec() {
  ?>
      <div class="form-group">
			    <label for="dil">Dil</label>
				<select name="dil" id="" class="form-control">
					<option value="turkce">Türkçe</option>
					<option value="ingilizce">İngilizce</option>
					<option value="almanca">Almanca</option>
					<option value="arapca">Arapça</option>
				</select>
			  </div>
  <?
}
function formDilSecEdit($dil) {
  ?>
      <div class="form-group">
			    <label for="dil">Dil</label>
				<select name="dil" id="" class="form-control">
					<option value="turkce" <?php echo $dil == 'turkce'  ? 'selected' : ''; ?> >Türkçe</option>
					<option value="ingilizce" <?php echo $dil == 'ingilizce'  ? 'selected' : ''; ?>>İngilizce</option>
					<option value="almanca" <?php echo $dil == 'almanca'  ? 'selected' : ''; ?>>Almanca</option>
					<option value="arapca" <?php echo $dil == 'arapca'  ? 'selected' : ''; ?>>Arapça</option>
				</select>
			  </div>
  <?
}
function ftirnak_kodla($degisken){
  print_r($degisken);
  $degisken= htmlentities($degisken, ENT_QUOTES, "UTF-8");
  return $degisken;
}
function ftirnak_dekodla($degisken){
    $degisken= html_entity_decode($degisken, ENT_QUOTES,"UTF-8");
    return $degisken;
}
function tirnaktemizle($deger) {
  $deger = htmlspecialchars(stripslashes($deger));
  return $deger;
}



function dilMenu($sayfa) {
  $dil = dilSec();
  ?>
      <ul class="nav nav-pills mb-3">
        <li class="nav-item ">
          <a class="nav-link  <?php echo $dil == 'turkce' ? 'active' : '' ; ?>" href="index.php?p=<?php echo $sayfa ?>&dil=turkce">Türkçe</a>
        </li>
        <!--li class="nav-item">
          <a class="nav-link <?php echo $dil == 'almanca' ? 'active' : '' ; ?>" href="index.php?p=<?php echo $sayfa ?>&dil=almanca">Almanca</a>
        </li-->
        <li class="nav-item">
          <a class="nav-link <?php echo $dil == 'ingilizce' ? 'active' : '' ; ?>" href="index.php?p=<?php echo $sayfa ?>&dil=ingilizce">İngilizce</a>
        </li>
        <!--li class="nav-item">
          <a class="nav-link <?php echo $dil == 'arapca' ? 'active' : '' ; ?>" href="index.php?p=<?php echo $sayfa ?>&dil=arapca">Arapça</a>
        </li-->
      </ul>
  <?
}
 function dilSec(Type $var = null)
{
  if (empty($_GET['dil'])) {
    return 'turkce';
  }
  else {
    return $_GET['dil'];
  }
}
function showmenu($s) {
  global $p;
  if ($s == 'menu-1' and ($p == 'markalar' or $p == 'marka-ekle' or $p == 'marka-duzenle' or $p == 'markamenu' or $p == 'bayiler') ) {
    echo "show";
  }
  elseif ($s == 'menu-2' and ($p == 'markalarw' or $p == 'genel-ayarlar' or $p == 'menu') ) {
    echo "show";
  }
  elseif ($s == 'menu-3' and ($p == 'haber-ekle' or $p == 'haberler' or $p == 'blogedit' ) ) {
    echo "show";
  }
  elseif ($s == 'menu-4' and ($p == 'sayfa-ekle' or $p == 'sayfalar' or $p == 'sayfa-duzenle' ) ) {
    echo "show";
  }



}

function seokontrol($dbisim,$baslik) {
  global $db;

  $baslik_seo_orjinal = turkceyap($baslik);
  $baslik_seo_orjinal = "$baslik_seo_orjinal";
  $baslik_seo_orjinal = turkceyap($baslik_seo_orjinal);

  $tumseolarigetir = $db->query("SELECT * from $dbisim where seo like '$baslik_seo_orjinal%'", PDO::FETCH_OBJ);

  $tumseolar = array();
  if ($tumseolarigetir->rowCount()) {
    foreach ($tumseolarigetir as $row) {
      $tumseolar[] = $row->seo;
    }
  }

  if (in_array($baslik_seo_orjinal, $tumseolar)){
    $i = 1;
    $baslikseo = $baslik_seo_orjinal. '-'. $i;
    while (in_array($baslikseo, $tumseolar)){
      $i++;
      $baslikseo = $baslik_seo_orjinal. '-'. $i;
    }
  }else {
    $baslikseo = $baslik_seo_orjinal;
  }

  return $baslikseo;


}

function blogkategorisec($id = 0, $string = 0) {
  global $db;
  $query = $db->query("select * from anamenu where ustid = $id order by id asc", PDO::FETCH_OBJ);

  if ($query->rowCount()) {
    foreach ($query as $row) {
      ?>
      <?
      if ($row->ustid == 0) {
        echo '<option value="'.$row->id.'">'.$row->ad.'</option>';
      }
      else {
        echo '<option value="'.$row->id.'">'.str_repeat('--', $string).$row->ad.'</option>';

      }
      ?>
      <?
      blogkategorisec($row->id, $string + 1);
    }
  }
  else {
    return false;
  }
}

function blogkategoriedit($id = 0, $string = 0, $ustid) {

 global $db;
 $query = $db->query("select * from anamenu where ustid = $id order by id asc", PDO::FETCH_OBJ);


   if ($query->rowCount()) {
      foreach ($query as $row) {
        echo '<option ';
        echo $row->id == $ustid ? 'selected ':null;
        echo ' value="'.$row->id.'">'.str_repeat('--', $string).$row->ad.'</option>';
        blogkategoriedit($row->id, $string + 1, $ustid);
      }
    }
    else {
      return false;
    }
}
function blogkategorigetir($id = 0, $string = 0) {
  global $db;
  $query = $db->query("select * from anamenu where ustid = $id order by sira asc", PDO::FETCH_OBJ);

  if ($query->rowCount()) {
    foreach ($query as $row) {
      ?>
      <tr class="<? echo $row->ustid == 0 ? 'bg-light':'' ?>">
        <td>
          <?
          if ($row->ustid == 0) {
            ?><strong><? echo str_repeat('--', $string) ?> <?=$row->ad?></strong><?
          }
          else {
            ?><? echo str_repeat('--', $string) ?> <?=$row->ad?><?
          }
          ?>
        </td>
        <td class="text-right"><button class="btn btn-danger btn-sm btn-sil" data-idname="id" data-db="anamenu" data-id="<?=$row->id?>" >Sil</button> <button type="button" class="btn btn-warning btn-sm " data-toggle="modal"  data-target="#Duzenle<?=$row->id?>" >Düzenle</button></td>
      </tr>
      <?
      blogkategorigetir($row->id, $string + 1);
    }
  }
  else {
    return false;
  }
}



function clear($data) {
  $data = strip_tags($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  $data = str_replace("%","",$data);
  $data = str_replace("'"," ",$data);
  $data = str_replace("chr(34)"," ",$data);
  $data = str_replace("chr(39)"," ",$data);
  $data =str_replace("="," ",$data);
  $data =str_replace("&"," ",$data);
  $data =str_replace("¿"," ",$data);
  return $data;
}

function turkceyap($str, $options = array()) {

  $str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());

  $defaults = array(

    'delimiter' => '-',

    'limit' => null,

    'lowercase' => true,

    'replacements' => array(),

    'transliterate' => true

  );

  $options = array_merge($defaults, $options);

  $char_map = array(

  // Latin

    'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',

    'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',

    'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',

    'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',

    'ß' => 'ss',

    'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',

    'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',

    'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',

    'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',

    'ÿ' => 'y',

  // Latin symbols

    '©' => '(c)',

  // Greek

    'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',

    'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',

    'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',

    'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',

    'Ϋ' => 'Y',

    'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',

    'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',

    'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',

    'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',

    'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',

  // Turkish

    'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',

    'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',

  // Russian

    'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',

    'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',

    'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',

    'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',

    'Я' => 'Ya',

    'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',

    'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',

    'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',

    'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',

    'я' => 'ya',

  // Ukrainian

    'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',

    'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',

  // Czech

    'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',

    'Ž' => 'Z',

    'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',

    'ž' => 'z',

  // Polish

    'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',

    'Ż' => 'Z',

    'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',

    'ż' => 'z',

  // Latvian

    'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',

    'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',

    'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',

    'š' => 's', 'ū' => 'u', 'ž' => 'z'

  );

  $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

  if ($options['transliterate']) {

    $str = str_replace(array_keys($char_map), $char_map, $str);

  }

  $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

  $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

  $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

  $str = trim($str, $options['delimiter']);

  return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;

}