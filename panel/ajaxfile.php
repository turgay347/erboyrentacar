<?php
// Count total files
$countfiles = count($_FILES['files']['name']);



include_once('libarry/classupload/class.upload.php');
$resim_sayisi   = count($_FILES["files"]["name"]);
  /* For döngümüzü oluşturuyoruz $i değişkeni dizi indesimiz belirleyecek ve gelen sayı kadar gönecek her döndüğünde 1 sayı artacak */
  for($i=0; $i<$resim_sayisi; $i++) {
      /* FORMUMUZDAN GELEN RESİMLERİN BİLGİLERİNİ ALALIM */
      $name       = $_FILES["files"]["name"][$i];     /* RESMİMİZİN İSMİ*/
      $type       = $_FILES["files"]["type"][$i];     /* RESMİMİZİN DOSYA UZANTISI*/
      $tmp_name   = $_FILES["files"]["tmp_name"][$i]; /* RESMİMİZİN GEÇİCİ DOSYA YOLU */
      $error      = $_FILES["files"]["error"][$i];    /* RESMİM İLE İLGİLİ VARSA HATA KODU */
      $size       = $_FILES["files"]["size"][$i];     /* RESMİMİZİN BOYUTU */
      /* TÜM BU ALDIĞIMIZ BİLGİLERİ YÜKLEME YAPABİLMEK İÇİN ARRAY İÇERİSİNDE TOPLAYIP $resim İSMİNDE BİR DEĞİŞKENE ATIYORUZ. */
      $resim = array('name' => $name , 'type' => $type , 'tmp_name' => $tmp_name , 'error' => $error ,'tmp_name' => $tmp_name , 'size' => $size);
      /* class.upload.php içersindeki yükleme fonksiyonlarını kullanıyoruz. */
      /* upload sınıfmızı başlatıyoruz */
      $yukle = new upload(@$resim);           
      /* Eğer yükleme işlermi gerçekleşmi ise aşağıdaki if çalışıyor*/
      if($yukle->uploaded){
          $yukle->file_new_name_body = 'images';
          $yukle->file_auto_rename = true;
          $yukle->process('../images/i/');
          $name=$yukle->file_dst_name;
          $iname = str_replace(".", "-", $name);
         
          /* Opsiyonel işlemi olumlu olduysa if çalışıyor */
          if($yukle->processed){
            ?>
              <script>
                $('#resimleronizleme').append('<figure class="<?=$iname?>"><button type="button" class="btn btn-danger btn-sm gorsel-sil" data-resimid="<?=$iname?>">sil</button><img src="../images/i/<?=$name?>" /></figure>');
                $('#resimlergrup').append('<input class="<?=$iname?>" type="hidden" value="<?=$name?>" name="gorseller[]" />');
              </script>

            <?
              /* En son $yukle değişkenimizi temizlemeyi unutmuyoruz */
              $yukle->clean();
          } 
      } 
  } 

