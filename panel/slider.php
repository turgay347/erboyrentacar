<?php echo !defined("guvenlik") ? die("Hata") : null; 
$dil = dilSec();
$query = $db->query("SELECT * FROM slider where dil = '$dil' order by id desc")->fetchAll();
dilMenu('slider');
?>


<div class="shadow p-3">

	<div class="row">
		<div class="col-md-6"><h4>Slider</h4></div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#aracEkle">
        Ekle
     </button>
   </div>
 </div>

 <hr>
 <table class="table table-bordered table-hover " >
  <thead>
    <tr>
      <th scope="col">Başlık</th>
      <th scope="col" class="text-right">İşlem</th>
    </tr>
  </thead>
  <tbody>
    <?
    foreach ($query as $key => $value) {
      ?>
      <tr>
        <td><?=$value['ad']?></td>
        <td class="text-right">
          <button class="btn btn-danger btn-sm btn-sil" data-db="slider" data-idname="id" data-id="<?=$value['id']?>" >Sil</button> 
          <button type="button" class="btn btn-warning btn-sm " data-toggle="modal" data-target="#Duzenle<?=$value['id']?>" >Düzenle</button>
        </td>
      </tr>
      <?
    }
    ?>
  </tbody>
</table>
</div>
<div class="modal fade" id="aracEkle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Araç Ekle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card mb-3">
          <div class="card-header">
            <h6 class="mb-0">Araç Resmi</h6>
          </div>
          <div class="card-body">
            <form  name="icerikresim" id="icerikresim" method="POST" action="inc/icerikresim.php"  onsubmit="return false;">
              <input type="hidden" names="crop" value="no">
              <div class="form-group">
                <input type="file"  name="resim" >
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-dark btn-sm"  onclick="AjaxFormS('icerikresim','form_status')">Resim Yükle</button>
              </div>
            </form>
            <div >
              <img id="ironizleme" src="../images/blank.png" class="img-fluid" >
            </div>
          </div>
        </div>
        <form  name="aracekle" id="aracekle" method="POST" action="inc/islem.php"  onsubmit="return false;">
          <input type="hidden" id="resim" name="resim">
          <input type="hidden" name="islem" value="sliderekle">
          <input type="hidden" name="dil" value="<?php echo $dil?>">
          <div class="form-group">
            <label for="ad"> Adı</label>
            <input type="text" class="form-control" id="ad"  name="ad" >
          </div>
          <div class="form-group">
            <label for="link">Link</label>
            <input type="text" class="form-control" id="link"  name="link" >
          </div>
          <div class="form-group">
            <label for="text1">Text 1</label>
            <input type="text" class="form-control" id="text1"  name="text1" >
          </div>
          <div class="form-group">
            <label for="text2">Text 2</label>
            <input type="text" class="form-control" id="text2"  name="text2" >
          </div>
          <div class="form-group">
            <label for="fiyat">Fiyat</label>
            <input type="text" class="form-control" id="fiyat"  name="fiyat" >
          </div>
        
          <button type="submit" class="btn btn-dark" onclick="AjaxFormS('aracekle','form_status')">Araç Ekle</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
      </div>
    </div>
  </div>
</div>
<?
foreach ($query as $row) {
  ?>
  <div class="modal fade" id="Duzenle<?=$row['id']?>" tabindex="-1" role="dialog" aria-labelledby="DuzenleLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="DuzenleLabel">Araç Düzenle</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="card mb-3">
            <div class="card-header">
              <h6 class="mb-0">Araç Resmi</h6>
            </div>
            <div class="card-body">
              <form  name="icerikresim<?=$row['id']?>" id="icerikresim<?=$row['id']?>" method="POST" action="inc/icerikresim.php"  onsubmit="return false;">
                <input type="hidden" names="crop" value="no">
                <input type="hidden" name="id" value="<?=$row['id']?>">
                <div class="form-group">
                  <input type="file"  name="resim" >
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-dark btn-sm"  onclick="AjaxFormS('icerikresim<?=$row['id']?>','form_status')">Resim Yükle</button>
                </div>
              </form>
              <div >
                <img id="ironizleme<?=$row['id']?>" src="../i/site/<?=$row['resim']?>" class="img-fluid" >
              </div>
            </div>
          </div>
          <form  name="aracedit<?=$row['id']?>" id="aracedit<?=$row['id']?>" method="POST" action="inc/islem.php"  onsubmit="return false;">
            <input type="hidden" name="islem" value="slideredit">
            <input type="hidden" name="dil" value="<?php echo $dil?>">
            <input type="hidden" name="id" value="<?=$row['id']?>">
            <input type="hidden" id="resim<?=$row['id']?>" name="resim" value="<?=$row['resim']?>">
            <div class="form-group">
                <label for="ad"> Adı</label>
                <input type="text" class="form-control" id="ad"  name="ad" value="<?=tirnaktemizle($row['ad'])?> ">
            </div>
            <div class="form-group">
                <label for="link">Link</label>
                <input type="text" class="form-control" id="link"  name="link" value="<?=tirnaktemizle($row['link'])?>">
            </div>
            <div class="form-group">
            <label for="text1">Text 1</label>
            <input type="text" class="form-control" id="text1"  name="text1" value="<?=tirnaktemizle($row['text1'])?>">
          </div>
          <div class="form-group">
            <label for="text2">Text 2</label>
            <input type="text" class="form-control" id="text2"  name="text2" value="<?=tirnaktemizle($row['text2'])?>">
          </div>
          <div class="form-group">
            <label for="fiyat">Fiyat</label>
            <input type="text" class="form-control" id="fiyat"  name="fiyat" value="<?=tirnaktemizle($row['fiyat'])?>">
          </div>
        
            <button type="submit" class="btn btn-dark" onclick="AjaxFormS('aracedit<?=$row['id']?>','form_status')">Düzenle</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
        </div>
      </div>
    </div>
  </div>
  <?
}
?>