<?

ob_start();
@session_start();
include '../../db/baglan.php';
include '../fonksiyonlar.php';

if ($_SESSION['y_id'] == '') {
	exit();
}

$islem = $_POST['islem'];


switch ($islem) {
  case 'sliderekle':
      if ( $_POST["ad"] == '' ) {
        echo '<div class="alert alert-danger">Ad Girin</div>';
        exit();
      }
      $query = $db->prepare("INSERT INTO slider SET
      ad = :ad,
      resim = :resim,
      text1 = :text1,
      text2 = :text2,
      fiyat = :fiyat,
      link = :link,
      dil = :dil");
      $insert = $query->execute(array(
            "ad" => $_POST["ad"],
            "resim" => $_POST["resim"],
            "link" => $_POST["link"],
            "text1" => $_POST["text1"],
            "text2" => $_POST["text2"],
            "fiyat" => $_POST["fiyat"],
            "dil" => $_POST["dil"]
      ));
      if ( $insert ){
          $last_id = $db->lastInsertId();
          ?>
            <script>
              location.reload();
            </script>
          <?
      }
      else {
        print_r($db->errorInfo());
      }
  break;
  case 'slideredit':

    @$id = addslashes($_POST["id"]);

    if ($_POST["ad"] == '' ) {
      echo '<div class="alert alert-danger">Tüm alanları doldurun</div>';
      exit();
    }

    $query = $db->prepare("UPDATE slider SET
      ad = :ad,
      resim = :resim,
      link = :link,
      text1 = :text1,
      text2 = :text2,
      fiyat = :fiyat,
      dil = :dil
      WHERE id = '$id'");
      $update = $query->execute(array(
        "ad" => $_POST["ad"],
            "resim" => $_POST["resim"],
            "text1" => $_POST["text1"],
            "text2" => $_POST["text2"],
            "fiyat" => $_POST["fiyat"],
            "link" => $_POST["link"],
            "dil" => $_POST["dil"]
    ));

    if ( $update ){
        $last_id = $db->lastInsertId();

        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    else {
      print_r($db->errorInfo());
      echo "hata";
    }


  break;
  case 'ofisekle':
   
    if ($_POST["ad"] == '' ) {
      echo '<div class="alert alert-danger">Ad Girin</div>';
      exit();
    }

    $query = $db->prepare("INSERT INTO ofisler SET
    ad = :ad,
    sehir = :sehir,
    adres = :adres,
    map = :map,
    dil = :dil,
    tel = :tel");
    $insert = $query->execute(array(
          "ad" => $_POST["ad"],
          "sehir" => $_POST["sehir"],
          "adres" => $_POST["adres"],
          "map" => $_POST["map"],
          "dil" => $_POST["dil"],
          "tel" => $_POST["tel"]
    ));
    if ( $insert ){
        $last_id = $db->lastInsertId();
        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    else {
      print_r($db->errorInfo());
    }
  break;
  case 'ofisedit':
    @$id = addslashes($_POST["id"]);

    if ($_POST["ad"] == '' ) {
      echo '<div class="alert alert-danger">Tüm alanları doldurun</div>';
      exit();
    }

    $query = $db->prepare("UPDATE ofisler SET
      ad = :ad,
      sehir = :sehir,
      adres = :adres,
      map = :map,
      dil = :dil,
      tel = :tel
      WHERE id = '$id'");
      $update = $query->execute(array(
        "ad" => $_POST["ad"],
        "sehir" => $_POST["sehir"],
        "adres" => $_POST["adres"],
        "map" => $_POST["map"],
        "dil" => $_POST["dil"],
        "tel" => $_POST["tel"]
    ));

    if ( $update ){
        $last_id = $db->lastInsertId();

        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    else {
      print_r($db->errorInfo());
      echo "hata";
    }
  
  break;
  case 'markaduzenle':
    @$id = addslashes($_POST["id"]);
    $marka_logo = $_POST["resim"];
    $marka_ad = addslashes($_POST["marka_ad"]);
    $marka_kisa_tanim = addslashes($_POST["marka_kisa_tanim"]);
    $marka_hakkinda = addslashes($_POST["editordetay"]);
    $marka_kategori = addslashes($_POST["marka_kategori"]);
    $marka_urunler_text = ($_POST["marka_urunler_text"]);
    if ($marka_ad == '' ) {
      echo '<div class="alert alert-danger">Tüm alanları doldurun</div>';
      exit();
    }

    $query = $db->prepare("UPDATE markalar SET
      marka_logo = :marka_logo,
      marka_ad = :marka_ad,
      marka_kisa_tanim = :marka_kisa_tanim,
      marka_hakkinda = :marka_hakkinda,
      marka_kategori = :marka_kategori,
      marka_urunler_text = :marka_urunler_text 
      WHERE marka_id = '$id'");
      $update = $query->execute(array(
           "marka_logo" => $marka_logo,
          "marka_ad" => $marka_ad,
          "marka_kisa_tanim" => $marka_kisa_tanim,
          "marka_kategori" => $marka_kategori,
          "marka_hakkinda" => $marka_hakkinda,
          "marka_urunler_text" => $marka_urunler_text
    ));

    if ( $update ){
        $last_id = $db->lastInsertId();

        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    else {
      print_r($db->errorInfo());
      echo "hata";
    }
  
    break;

  case 'aekle': 
      
      
      
    
    
    $query = $db->prepare("INSERT INTO araclar SET
      araci_adi = :araci_adi,
      arac_marka = :arac_marka,
      yakit = :yakit,
      arac_gorsel = :arac_gorsel,
      arac_durum = :arac_durum,
      vites = :vites,
      kapasite = :kapasite,
      fiyat = :fiyat,
      dil = :dil,
      kapi = :kapi,
      km = :km,
      kilima = :kilima,
      depozit = :depozit
      ");
    $insert = $query->execute(array(
      'araci_adi' => $_POST['ad'],
      'arac_marka' => $_POST['arac_marka'],
      'yakit' => $_POST['yakit'],
      'arac_gorsel' => $_POST['resim'],
      'arac_durum' => $_POST['durum'],
      'fiyat' => $_POST['fiyat'],
      'vites' => $_POST['vites'],
      'kapasite' => $_POST['kapasite'],
      'kapi' => $_POST['kapi'],
      'km' => $_POST['km'],
      'dil' => $_POST['dil'],
      'kilima' => $_POST['kilima'],
      'depozit' => $_POST['depozit']
    ));
    
    if ( $insert ){
      $last_id = $db->lastInsertId();
      ?>
      <script>
         location.reload();
      </script>
      <?
    }
    else {
      print_r($db->errorInfo());
      
    }
  break;

  case 'aedit':

    $query = $db->prepare("UPDATE araclar SET
      araci_adi = :araci_adi,
      arac_marka = :arac_marka,
      yakit = :yakit,
      arac_gorsel = :arac_gorsel,
      arac_durum = :arac_durum,
      vites = :vites,
      kapasite = :kapasite,
      kapi = :kapi,
      dil = :dil,
      fiyat = :fiyat,
      km = :km,
      kilima = :kilima,
      depozit = :depozit
      WHERE arac_id = $_POST[id]
      ");

    $update = $query->execute(array(
      'araci_adi' => $_POST['ad'],
      'arac_marka' => $_POST['arac_marka'],
      'yakit' => $_POST['yakit'],
      'arac_gorsel' => $_POST['resim'],
      'arac_durum' => $_POST['durum'],
      'vites' => $_POST['vites'],
      'fiyat' => $_POST['fiyat'],
      'kapasite' => $_POST['kapasite'],
      'dil' => $_POST['dil'],
      'kapi' => $_POST['kapi'],
      'km' => $_POST['km'],
      'kilima' => $_POST['kilima'],
      'depozit' => $_POST['depozit']
  ));

    if ( $update ){
      $last_id = $db->lastInsertId();
      ?>
      <script>
        location.reload();
      </script>
      <?
    }
    else {
      echo "hata";
    }
  break;
  
 
  case 'markakatekle':
    
    if ($_POST['ad'] == '' ) {
      echo '<div class="alert alert-danger">Tüm alanları doldurun</div>';
      exit();
    }

    $query = $db->prepare("INSERT INTO marka_kategoriler SET
    ad = :ad,
    anasayfa = :anasayfa");
    $insert = $query->execute(array(
          "ad" => trim($_POST['ad']),
          "anasayfa" => $_POST['anasayfa'],
    ));
    if ( $insert ){
        $last_id = $db->lastInsertId();
        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    else {
      echo "hatddda";
      print_r($db->errorInfo());

    }
    break;
  case 'markakatduzenle':

    @$id = addslashes($_POST["id"]);
   
     if ($_POST['ad'] == '' ) {
      echo '<div class="alert alert-danger">Tüm alanları doldurun</div>';
      exit();
    }

    $query = $db->prepare("UPDATE marka_kategoriler SET
      ad = :ad,
      anasayfa = :anasayfa
      WHERE id = '$id'");
      $update = $query->execute(array(
          "ad" => trim($_POST['ad']),
          "anasayfa" => $_POST['anasayfa']
    ));

    if ( $update ){
        $last_id = $db->lastInsertId();

        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    else {
      print_r($db->errorInfo());
      echo "hata";
    }
  break;
  case 'bayiekle':
    $bayiler_ad = addslashes($_POST["bayiler_ad"]);
    $bayiler_telefon = addslashes($_POST["bayiler_telefon"]);
    $bayiler_adres = addslashes($_POST["bayiler_adres"]);
    $bayiler_konum = addslashes($_POST["bayiler_konum"]);
    $bayiler_marka_id = addslashes($_POST["bayiler_marka_id"]);
    $bayiler_sehir = addslashes($_POST["bayiler_sehir"]);
    if ($bayiler_ad == '' ) {
      echo '<div class="alert alert-danger">Tüm alanları doldurun</div>';
      exit();
    }

    $query = $db->prepare("INSERT INTO bayiler SET
    bayiler_ad = :bayiler_ad,
    bayiler_telefon = :bayiler_telefon,
    bayiler_adres = :bayiler_adres,
    bayiler_konum = :bayiler_konum,
    bayiler_sehir = :bayiler_sehir,
    bayiler_marka_id = :bayiler_marka_id");
    $insert = $query->execute(array(
          "bayiler_ad" => $bayiler_ad,
          "bayiler_telefon" => $bayiler_telefon,
          "bayiler_adres" => $bayiler_adres,
          "bayiler_konum" => $bayiler_konum,
          "bayiler_sehir" => $bayiler_sehir,
          "bayiler_marka_id" => $bayiler_marka_id
    ));
    if ( $insert ){
        $last_id = $db->lastInsertId();
        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    else {
      echo "hatddda";
      print_r($db->errorInfo());

    }
    break;
  case 'bayiduzenle':
    @$id = addslashes($_POST["id"]);
    $bayiler_ad = addslashes($_POST["bayiler_ad"]);
    $bayiler_telefon = addslashes($_POST["bayiler_telefon"]);
    $bayiler_adres = addslashes($_POST["bayiler_adres"]);
    $bayiler_konum = addslashes($_POST["bayiler_konum"]);
    $bayiler_marka_id = addslashes($_POST["bayiler_marka_id"]);
    $bayiler_sehir = addslashes($_POST["bayiler_sehir"]);
    if ($bayiler_ad == '' ) {
      echo '<div class="alert alert-danger">Tüm alanları doldurun</div>';
      exit();
    }


    $query = $db->prepare("UPDATE bayiler SET
      bayiler_ad = :bayiler_ad,
      bayiler_telefon = :bayiler_telefon,
      bayiler_adres = :bayiler_adres,
      bayiler_konum = :bayiler_konum,
      bayiler_sehir = :bayiler_sehir,
      bayiler_marka_id = :bayiler_marka_id
      WHERE bayiler_id = '$id'");
      $update = $query->execute(array(
          "bayiler_ad" => $bayiler_ad,
          "bayiler_telefon" => $bayiler_telefon,
          "bayiler_adres" => $bayiler_adres,
          "bayiler_konum" => $bayiler_konum,
          "bayiler_sehir" => $bayiler_sehir,
          "bayiler_marka_id" => $bayiler_marka_id
    ));

    if ( $update ){
        $last_id = $db->lastInsertId();

        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    else {
      print_r($db->errorInfo());
      echo "hata";
    }
    break;
  case 'urunekle':

    $menu_ad = addslashes($_POST["menu_ad"]);
    $menu_marka_id = addslashes($_POST["menu_marka_id"]);
    $menu_resim = addslashes($_POST["resim"]);
    $menu_icerik = addslashes($_POST["menu_icerik"]);
    $menu_fiyat = addslashes($_POST["menu_fiyat"]);
    if ($menu_ad == '' ) {
      echo '<div class="alert alert-danger">Ad ve Resim giriniz</div>';
      exit();
    }



    $query = $db->prepare("INSERT INTO menu SET
    menu_ad = :menu_ad,
    menu_marka_id = :menu_marka_id,
    menu_resim = :menu_resim,
    menu_icerik = :menu_icerik,
    menu_fiyat = :menu_fiyat");
    $insert = $query->execute(array(
          "menu_ad" => $menu_ad,
          "menu_marka_id" => $menu_marka_id,
          "menu_resim" => $menu_resim,
          "menu_icerik" => $menu_icerik,
          "menu_fiyat" => $menu_fiyat
    ));
    if ( $insert ){
        $last_id = $db->lastInsertId();
        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    else {
      echo "hatddda";
      print_r($db->errorInfo());

    }
    break;
  case 'urunedit':


    $menu_ad = addslashes($_POST["menu_ad"]);
    $menu_marka_id = addslashes($_POST["menu_marka_id"]);
    $menu_resim = addslashes($_POST["resim"]);
    $menu_icerik = addslashes($_POST["menu_icerik"]);
    $menu_fiyat = ($_POST["menu_fiyat"]);
    @$id = addslashes($_POST["id"]);
    if ($menu_ad == '' or $menu_resim == '' ) {
      echo '<div class="alert alert-danger">Ad ve Resim giriniz</div>';
      exit();
    }

    $query = $db->prepare("UPDATE menu SET
      menu_ad = :menu_ad,
      menu_marka_id = :menu_marka_id,
      menu_resim = :menu_resim,
      menu_icerik = :menu_icerik,
      menu_fiyat = :menu_fiyat
      WHERE menu_id = '$id'");
      $update = $query->execute(array(
          "menu_ad" => $menu_ad,
        "menu_marka_id" => $menu_marka_id,
        "menu_resim" => $menu_resim,
        "menu_icerik" => $menu_icerik,
        "menu_fiyat" => $menu_fiyat
    ));

 

    if ( $update ){
        $last_id = $db->lastInsertId();
        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    else {
      echo "hatddda";
      print_r($db->errorInfo());

    }
    break;
  case 'blogekle':

    @$baslik = addslashes($_POST["baslik"]);
    @$aciklama = addslashes($_POST["aciklama"]);
    @$title = addslashes($_POST["title"]);
    @$keyw = addslashes($_POST["keyw"]);
    @$kategori = addslashes($_POST["kategori"]);
    @$icerik = addslashes($_POST["icerik"]);
    @$resim = addslashes($_POST["resim"]);
    @$dil = addslashes($_POST["dil"]);

    if ($baslik == '') {
      echo '<div class="alert alert-danger">Tüm alanları doldurun</div>';
      exit();
    }

    $seo =  seokontrol('bloglar',$baslik);


    $query = $db->prepare("INSERT INTO bloglar SET
    baslik = :baslik,
    icerik = :icerik,
    title = :title,
    aciklama = :aciklama,
    keyw = :keyw,
    seo = :seo,
    dil = :dil,
    resim = :resim");
    $insert = $query->execute(array(
          "baslik" => $baslik,
          "icerik" => $icerik,
          "title" => $title,
          "aciklama" => $aciklama,
          "keyw" => $keyw,
          "dil" => $dil,
          "seo" => $seo,
          "resim" => $resim
    ));
    if ( $insert ){
        $last_id = $db->lastInsertId();
        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    else {
      echo "hata";
    }
  break;
  case 'sayfaekle':

    @$baslik = addslashes($_POST["baslik"]);
    @$aciklama = addslashes($_POST["aciklama"]);
    @$title = addslashes($_POST["title"]);
    @$keyw = addslashes($_POST["keyw"]);
    @$kategori = addslashes($_POST["kategori"]);
    @$icerik = addslashes($_POST["editordetay"]);
    @$resim = addslashes($_POST["resim"]);

    if ($baslik == '') {
      echo '<div class="alert alert-danger">Tüm alanları doldurun</div>';
      exit();
    }



    $query = $db->prepare("INSERT INTO hazirsayfa SET
    baslik = :baslik,
    icerik = :icerik");
    $insert = $query->execute(array(
          "baslik" => $baslik,
          "icerik" => $icerik
    ));
    if ( $insert ){
        $last_id = $db->lastInsertId();
        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    else {
      echo "hata";
    }
  break;
  case 'blogedit':
    @$id = addslashes($_POST["id"]);
    @$baslik = addslashes($_POST["baslik"]);
    @$icerik = addslashes($_POST["icerik"]);
    @$title = addslashes($_POST["title"]);
    @$keyw = addslashes($_POST["keyw"]);
    @$icerik = addslashes($_POST["icerik"]);
    @$resim = addslashes($_POST["resim"]);
    @$dil = addslashes($_POST["dil"]);

    if ($baslik == '') {
      echo '<div class="alert alert-danger">Tüm alanları doldurun</div>';
      exit();
    }
    $seosil = $db->prepare("UPDATE bloglar SET
      seo = :seo
      WHERE id = '$id'");
      $updateseo = $seosil->execute(array(
           "seo" => ''
    ));

    $seo =  seokontrol('bloglar',$baslik);



    $query = $db->prepare("UPDATE bloglar SET
      baslik = :baslik,
      icerik = :icerik,
      title = :title,
      keyw = :keyw,
      seo = :seo,
      dil = :dil,
      resim = :resim
      WHERE id = '$id'");
      $update = $query->execute(array(
           "baslik" => $baslik,
           "icerik" => $icerik,
           "dil" => $dil,
           "title" => $title,
          "keyw" => $keyw,
          "seo" => $seo,
           "resim" => $resim
    ));





    if ( $update ){
        $last_id = $db->lastInsertId();
        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    else {
      echo "hata";
    }
    break;
  case 'sayfaedit':
    @$id = ($_POST["id"]);
    @$baslik = ($_POST["baslik"]);
    @$icerik = ($_POST["editordetay"]);

    if ($baslik == '' or $icerik == '') {
      echo '<div class="alert alert-danger">Tüm alanları doldurun</div>';
      exit();
    }


      $query = $db->prepare("UPDATE hazirsayfa SET
        baslik = :baslik,
        icerik = :icerik
        WHERE id = '$id'");
        $update = $query->execute(array(
             "baslik" => $baslik,
             "icerik" => $icerik
      ));



      if ( $update ){
        $last_id = $db->lastInsertId();
        ?>
          <script>
            location.reload();
          </script>
        <?
    }
    break;
  default:
    # code...
    break;
}


