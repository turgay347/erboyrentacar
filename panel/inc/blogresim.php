<?
ob_start();
@session_start();
include '../../db/baglan.php';
include '../fonksiyonlar.php';
if ($_SESSION['y_id'] == '') {
	exit();
}
include_once('../libarry/classupload/class.upload.php');
$upload = new upload(@$_FILES['resim']);


if ($_POST['boyut'] == 0) {
	$resize = false;
	$crop = false;
}
else {
	$resize = true;
	$crop = true;
}
if ($upload->uploaded)
{
$upload->allowed = array ( 'image/*' );
$upload->file_auto_rename = true;
$upload->image_resize          = $resize;
$upload->image_ratio_crop      = $crop;
$upload->image_y               = 640;
$upload->image_x               = 640;
$upload->process('../../i/site');
$name=$upload->file_dst_name;

	if ($upload->processed)
	{
		$upload->clean();
		?>
	    	<script>
	    		$("#bgonizleme").attr("src","../i/site/<?=$name?>");
	    		$('#resim').val("<?=$name?>");
	    		$("#blogresim")[0].reset();
	    	</script>
	    <?
	}
	else
	{
		?>
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
			  <strong>Hata!</strong> <?=$upload->error?>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
		<?
	}
}

?>
