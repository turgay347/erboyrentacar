<?
ob_start();
@session_start();
include '../../db/baglan.php';
include '../fonksiyonlar.php';
if ($_SESSION['y_id'] == '') {
	exit();
}

include_once('../libarry/classupload/class.upload.php');
$upload = new upload(@$_FILES['resim']);

if ($upload->uploaded)
{
$upload->allowed = array ( 'image/*' );
$upload->file_auto_rename = true;
$upload->process('../../i/site');
$name=$upload->file_dst_name;

	if ($upload->processed)
	{
		$upload->clean();
		$query = $db->prepare("UPDATE ayarlar SET
		ayar = :logo
		WHERE ad = 'hlogo'");
		$update = $query->execute(array(
		     "logo" => $name
		));
		if ( $update ){
		    ?>
		    	<script>
		    		$("#hlogo").attr("src","../i/site/<?=$name?>");
		    	</script>
		    <?
		}
	}
	else
	{
		?>
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
			  <strong>Hata!</strong> <?=$upload->error?>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
		<?
	}
}

?>