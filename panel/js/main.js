$(document).ready(function () {
  $('#sidebarCollapse').on('click', function () {
    $('#sidebar').toggleClass('active');
  });

   $("#files").change(function (){


     var form_data = new FormData();

       // Read selected files
       var totalfiles = document.getElementById('files').files.length;
       for (var index = 0; index < totalfiles; index++) {
        form_data.append("files[]", document.getElementById('files').files[index]);
      }

       // AJAX request
       $.ajax({
         url: 'ajaxfile.php',
         type: 'post',
         data: form_data,
         contentType: false,
         processData: false,
         success: function (e) {
           $('#form_status').html(e);
           $("#files").val('');
         }
       });

  });

   
});


function editorFormSucces() {
  var myMce = tinymce.get("editor").getContent();
  $('#editordetay').val(myMce);
  AjaxFormS('editorForm','form_status');
}

function editorFormSucces2() {
  var myMce = tinymce.get("editor").getContent();
  $('#editordetay').val(myMce);
  AjaxFormS('textayarlar','form_status');
}

function bolgeekle() {
  var myMce = tinymce.get("editor").getContent();
  $('#icerik').val(myMce);
  AjaxFormS('bolgeekles','form_status');
}

function blogekle() {
  var myMce = tinymce.get("editor").getContent();
  $('#icerik').val(myMce);
  AjaxFormS('blogekles','form_status');
}
function kiralikevekle() {
  var myMce = tinymce.get("editor").getContent();
  $('#kiralik_yat_detay').val(myMce);
  $('#kiralik_ev_detay').val(myMce);
  AjaxFormS('kiralikEvEkle','form_status');
}
function otelekle() {
  var myMce = tinymce.get("editor").getContent();
  $('#icerik').val(myMce);
  AjaxFormS('otelekles','form_status');
}
function turekle() {
  var myMce = tinymce.get("editor").getContent();
  $('#turlar_detay').val(myMce);
  AjaxFormS('turekles','form_status');
}
$(document).ready(function () {
  $(".btn-sil").click(function(){
    if (confirm('Bu işlemin geri dönüşü yoktur. Silmek istediğinizden eminmisiniz?')) {
      var db = $(this).data("db");
      var id = $(this).data("id");
      var idname = $(this).data("idname");
      $.ajax({
        type:'POST',

        url:'inc/sil.php',

        data: { db: db, id : id, idname:idname},

        success:function(e){
          $('#form_status').html(e);
        }
      });
    } else {
      alert('Silinmedi');
    }

  });
});






function AjaxFormS(FORMID,SONUCID){
  $("#"+SONUCID).fadeOut(400);
  $("#"+FORMID).ajaxForm({
    target: '#'+SONUCID,
    complete:function(){
      $("#"+SONUCID).fadeIn(400);
    }
  }).submit();

}







$(document).ready(function () {
  $('#resimleronizleme').on('click', '.gorsel-sil', function() {
    var resimid = $(this).data("resimid");
    $('.'+resimid+'').remove();
  });
});






$(document).ready(function () {
  tinymce.init({
    selector: "#editor",
    language : 'tr_TR',
    relative_urls : false,
    remove_script_host : false,
    convert_urls : true,
    plugins: [

    "advlist autolink lists link image charmap print preview anchor ",
    "searchreplace visualblocks code fullscreen ",
    ],


          toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | mybutton | inserttable"

        });
});



$(document).ready(function() {
  $('#dataTable').DataTable( {
        dom: 'Bfrtip',
        buttons: [
             'excelFlash', 'excel'
        ],
        language: {
          "emptyTable": "Gösterilecek ver yok.",
          "processing": "Veriler yükleniyor",
          "sDecimal": ".",
          "sInfo": "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
          "sInfoFiltered": "(_MAX_ kayıt içerisinden bulunan)",
          "sInfoPostFix": "",
          "sInfoThousands": ".",
          "sLengthMenu": "Sayfada _MENU_ kayıt göster",
          "sLoadingRecords": "Yükleniyor...",
          "sSearch": "Ara:",
          "sZeroRecords": "Eşleşen kayıt bulunamadı",
          "oPaginate": {
              "sFirst": "İlk",
              "sLast": "Son",
              "sNext": "Sonraki",
              "sPrevious": "Önceki"
          },
          "oAria": {
              "sSortAscending": ": artan sütun sıralamasını aktifleştir",
              "sSortDescending": ": azalan sütun sıralamasını aktifleştir"
          },
          "select": {
              "rows": {
                  "_": "%d kayıt seçildi",
                  "0": "",
                  "1": "1 kayıt seçildi"
              }
          }
      }
    } );
});


function tinyEkle(mySrc) {
  var myMce = tinyMCE.activeEditor;
  myMce.focus();
  myMce.selection.setContent('<img src="'+mySrc+'" />');

}
