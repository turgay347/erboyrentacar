<?php echo !defined("guvenlik") ? die("Hata") : null; 
$query = $db->query("SELECT * FROM siparisler order by id asc", PDO::FETCH_OBJ);
?>
<div class=" shadow p-3">
	<h4>Rezervasyonlar</h4>
	<hr>
	<?
		if (!$query->rowCount()) {
			echo "Kayıt Yok";
		}
	?>
	<?
		if ($query->rowCount ()) {
			?>
				<table class="table" id="dataTable">
				  <thead>
				    <tr>
				      <th scope="col">Rezervasyon No</th>
				      <th scope="col">Araba</th>
				      <th scope="col">İstasyon</th>
				      <th scope="col">İstasyon Kodu</th>
				      <th scope="col">Başlangıç Tarih</th>
				      <th scope="col">Bitiş Tarih</th>
				      <th scope="col">Ücret</th>
				      <th scope="col">Müşteri Ad Soyad</th>
				      <th scope="col">Müşteri Eposta</th>
				      <th scope="col">Müşteri Telefon</th>
				      <th scope="col">Müşteri Tipi</th>
				      <th scope="col">İşlem Tarihi</th>
				      <th scope="col" class="text-right">İşlem</th>
				    </tr>
				  </thead>
				  <tbody>
			<?
		}

		foreach ($query as $row) {
			?>
					<tr>
				      <th scope="row"><?=$row->rezID?> </th>
				      <th scope="row"><?=$row->araba_bilgi?> </th>
				      <th scope="row"><?=$row->istasyon_adi?> </th>
				      <th scope="row"><?=$row->istasyon_kodu?> </th>
				      <th scope="row"><?php echo date_format(date_create($row->baslangic),"d.m.Y H:i");?> </th>
				      <th scope="row"><?php echo date_format(date_create($row->bitis),"d.m.Y H:i");?> </th>
				      <th scope="row"><?=$row->ucret?> TL </th>
				      <th scope="row"><?=$row->musteri_cinsiyet?> / <?=$row->musteri_adi?> <?=$row->musteri_soyadi?></th>
				      <th scope="row"><?=$row->musteri_eposta?> </th>
				      <th scope="row"><?=$row->musteri_telefon?> </th>
				      <th scope="row"><?=$row->musteri_tip=='company'?'Kurumsal':'Bireysel'?>
					  <?php if($row->musteri_tip=='company'){ ?>
						<table>
							<tr>
								<td>Ünvan</td>
								<td><?=$row->unvan?></td>
							</tr>
							<tr>
								<td>Vergi Dairesi</td>
								<td><?=$row->vergi_dairesi?></td>
							</tr>
							<tr>
								<td>Vergi Numarası</td>
								<td><?=$row->vergi_numarasi?></td>
							</tr>
							<tr>
								<td>Telefon</td>
								<td><?=$row->telefon?></td>
							</tr>
							<tr>
								<td>Şehir</td>
								<td><?=$row->sehir?></td>
							</tr>
							<tr>
								<td>Ülke</td>
								<td><?=$row->ulke?></td>
							</tr>
							<tr>
								<td>Adres</td>
								<td><?=$row->adres?></td>
							</tr>
						</table>
					  <?php } ?>
					  </th>
				      <th scope="row"><?php echo date_format(date_create($row->islem_tarihi),"d.m.Y H:i");?> </th>
				      <td class="text-right">
				      	<button type="button" class="btn btn-danger btn-sm btn-sil" data-idname="id" data-db="siparisler" data-id="<?=$row->id?>">Sil</button>
				      </td>
				    </tr>
			<?
		}
		if ($query->rowCount ()) {
			?>
					</tbody>
				</table>
			<?
		}
	?>
	
	  
	  
</div>
