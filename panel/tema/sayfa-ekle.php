<?php echo !defined("guvenlik") ? die("Hata") : null; 
?>
<div class="shadow p-3">
	<h4>Sayfa Ekle</h4>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<form  name="editorForm" id="editorForm" method="POST" action="inc/islem.php"  onsubmit="return false;">
				<input type="hidden" name="resim" id="resim">
				<input type="hidden" name="islem" value="sayfaekle">
			  <div class="form-group">
			    <label for="baslik">Başlık</label>
			    <input type="text" class="form-control" id="baslik"  name="baslik" >
			  </div>
	
			  
			  <div class="form-group">
			    <label for="icerik">İçerik</label>
			    <textarea name="editor " id="editor" cols="30" rows="10" class="form-control"></textarea>
			    <textarea name="editordetay" id="editordetay" cols="30" rows="10" class="d-none"></textarea>
			  </div>
			  <button type="submit" class="btn btn-dark" onclick="editorFormSucces()">Sayfa Ekle</button>
			</form>
		</div>
		
	</div>
</div>
