<?php echo !defined("guvenlik") ? die("Hata") : null; 
$dil = dilSec();
$query = $db->query("SELECT * FROM ofisler where dil = '$dil' order by id desc")->fetchAll();
dilMenu('ofisler');

?>
<div class="shadow p-3">
	<div class="row">
		<div class="col-md-6"><h4>Ofisler</h4></div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#aracEkle">
       Ofis Ekle
     </button>
   </div>
 </div>

 <hr>
 <table class="table table-bordered table-hover " >
  <thead>
    <tr>
      <th scope="col">Başlık</th>
      <th scope="col" class="text-right">İşlem</th>
    </tr>
  </thead>
  <tbody>
    <?
    foreach ($query as $key => $value) {
      ?>
      <tr>
        <td><?=$value['ad']?></td>
        <td class="text-right">
          <button class="btn btn-danger btn-sm btn-sil" data-db="ofisler" data-idname="id" data-id="<?=$value['id']?>" >Sil</button> 
          <button type="button" class="btn btn-warning btn-sm " data-toggle="modal" data-target="#Duzenle<?=$value['id']?>" >Düzenle</button>
        </td>
      </tr>
      <?
    }
    ?>
  </tbody>
</table>
</div>
<div class="modal fade" id="aracEkle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Araç Ekle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
        <form  name="aracekle" id="aracekle" method="POST" action="inc/islem.php"  onsubmit="return false;">
          <input type="hidden" id="resim" name="resim">
          <input type="hidden" name="islem" value="ofisekle">
          <input type="hidden" name="dil" value="<?php echo $dil?>">
          <div class="form-group">
            <label for="ad">Ofis Adı</label>
            <input type="text" class="form-control" id="ad"  name="ad" >
          </div>
          <div class="form-group">
            <label for="sehir">Şehir</label>
            <input type="text" class="form-control" id="sehir"  name="sehir" >
          </div>
          <div class="form-group">
            <label for="adres">Adres</label>
            <input type="text" class="form-control" id="adres"  name="adres" >
          </div>
          <div class="form-group">
            <label for="map">Map</label>
            <input type="text" class="form-control" id="map"  name="map" >
          </div>
          <div class="form-group">
            <label for="tel">Telefon</label>
            <input type="text" class="form-control" id="tel"  name="tel" >
          </div>
          
          <button type="submit" class="btn btn-dark" onclick="AjaxFormS('aracekle','form_status')">Ekle</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
      </div>
    </div>
  </div>
</div>
<?
foreach ($query as $row) {
  ?>
  <div class="modal fade" id="Duzenle<?=$row['id']?>" tabindex="-1" role="dialog" aria-labelledby="DuzenleLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="DuzenleLabel"> Düzenle</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         
          <form  name="aracedit<?=$row['id']?>" id="aracedit<?=$row['id']?>" method="POST" action="inc/islem.php"  onsubmit="return false;">
            <input type="hidden" name="islem" value="ofisedit">
            <input type="hidden" name="dil" value="<?php echo $dil?>">
            <input type="hidden" name="id" value="<?=$row['id']?>">
            <div class="form-group">
            <label for="ad">Ofis Adı</label>
            <input type="text" class="form-control" id="ad"  name="ad" value="<?=tirnaktemizle($row['ad'])?>">
          </div>
          <div class="form-group">
            <label for="sehir">Şehir</label>
            <input type="text" class="form-control" id="sehir"  name="sehir" value="<?=tirnaktemizle($row['sehir'])?>">
          </div>
          <div class="form-group">
            <label for="adres">Adres</label>
            <input type="text" class="form-control" id="adres"  name="adres" value="<?=tirnaktemizle($row['adres'])?>">
          </div>
          <div class="form-group">
            <label for="map">Map</label>
            <input type="text" class="form-control" id="map"  name="map" value="<?=tirnaktemizle($row['map'])?>">
          </div>
          <div class="form-group">
            <label for="tel">Telefon</label>
            <input type="text" class="form-control" id="tel"  name="tel" value="<?=tirnaktemizle($row['tel'])?>">
          </div>
            <button type="submit" class="btn btn-dark" onclick="AjaxFormS('aracedit<?=$row['id']?>','form_status')">Düzenle</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
        </div>
      </div>
    </div>
  </div>
  <?
}
?>