<?php echo !defined("guvenlik") ? die("Hata") : null; 
$dil = dilSec();
dilMenu('genel-ayarlar');

?>
<div class=" shadow p-3">
	<h4>Genel Ayarlar</h4>
	<hr>
	<ul class="nav nav-tabs" id="myTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Site Ayarları</a>
		</li>
		<li class="nav-item">
			<a class="nav-link " id="text-tab" data-toggle="tab" href="#text" role="tab" aria-controls="text" aria-selected="true">Genel Ayarlar</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="false">Seo</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">İletişim</a>
		</li>
	</ul>
	<div class="tab-content  " id="myTabContent">
		<div class="tab-pane fade show active pt-3" id="home" role="tabpanel" aria-labelledby="home-tab">
			<form  name="gayarlar" id="gayarlar" method="POST" action="inc/ayarlar.php"  onsubmit="return false;">
				<div class="form-group">
					<label for="domain">Site Adresi</label>
					<input type="text" class="form-control" id="domain"name="veri[domain]" value="<?=ayargetir('domain')?>">
					<small class="form-text text-muted">Sonunda / olarak giriniz	</small>
				</div>
			
				<div class="form-group">
					<label for="adminka">Admin Kullanıcı Adı</label>
					<input type="text" class="form-control" id="adminka"  name="veri[adminka]" value="<?=ayargetir('adminka')?>">
				</div>
				<div class="form-group">
					<label for="adminpass">Admin Şifre</label>
					<input type="text" class="form-control" id="adminpass"  name="veri[adminpass]" value="<?=ayargetir('adminpass')?>">
				</div>
				<button type="submit" class="btn btn-dark" onclick="AjaxFormS('gayarlar','form_status')">Kaydet</button>
			</form>
		</div>
		<div class="tab-pane fade pt-3" id="text" role="tabpanel" aria-labelledby="text-tab">
			
			<form  name="textayarlar" id="textayarlar" method="POST" action="inc/ayarlar.php"  onsubmit="return false;">
				<input type="hidden" name="dil" value="<?php echo $dil?>">
				<div class="form-group">
					<label for="site_lang">Lang</label>
					<input type="text" class="form-control" id="site_lang"  name="veri[site_lang]" value="<?=ayargetir('site_lang',$dil)?>">
				</div>
				<div class="form-group">
					<label for="site_adi">Site Adı</label>
					<input type="text" class="form-control" id="site_adi"  name="veri[site_adi]" value="<?=ayargetir('site_adi',$dil)?>">
				</div>
				<div class="form-group">
					<label for="copyright">Copyright</label>
					<input type="text" class="form-control" id="copyright"  name="veri[copyright]" value="<?=ayargetir('copyright',$dil)?>">
				</div>
				<div class="form-group">
					<label for="footer_info">Footer Text</label>
					<textarea name="veri[footer_info]"  cols="30" rows="3" class="form-control"><?=ayargetir('footer_info',$dil)?></textarea>
				</div>
				
				<div class="form-group">
					<label for="footer_info">Head Code</label>
					<textarea name="veri[head_code]"  cols="30" rows="3" class="form-control"><?=ayargetir('head_code',$dil)?></textarea>
				</div>
				
				<div class="form-group">
			    <label for="icerik">Sözleşme İçeriği</label>
				
			    <textarea name="" id="editor" cols="30" rows="20" class="form-control"><?=ayargetir('sozlesme',$dil)?></textarea>
<textarea name="veri[sozlesme]" id="editordetay" cols="30" rows="10" class="d-none"></textarea>
			  </div>
			
				<button type="submit" class="btn btn-dark" onclick="editorFormSucces2()">Kaydet</button>
			</form>
		</div>
		<div class="tab-pane fade pt-3" id="seo" role="tabpanel" aria-labelledby="seo-tab">
			<form  name="seoayalar" id="seoayalar" method="POST" action="inc/ayarlar.php"  onsubmit="return false;">
			<input type="hidden" name="dil" value="<?php echo $dil?>">
				<div class="form-group">
					<label for="site_title">Site Title</label>
					<input type="text" class="form-control" id="site_title" name="veri[site_title]" value="<?=ayargetir('site_title',$dil)?>">
				</div>
				<div class="form-group">
					<label for="site_keyws">Site Arama Kelimeleri</label>
					<input type="text" class="form-control" id="site_keyws" name="veri[site_keyws]" value="<?=ayargetir('site_keyws',$dil)?>">
				</div>
				<div class="form-group">
					<label for="site_desc">Admin Description</label>
					<input type="text" class="form-control" id="site_desc" name="veri[site_desc]"  value="<?=ayargetir('site_desc',$dil)?>">
				</div>
				<button type="submit" class="btn btn-dark" action="inc/ayarlar.php"  onclick="AjaxFormS('seoayalar','form_status')">Kaydet</button>
			</form>
		</div>
		<div class="tab-pane fade pt-3" id="contact" role="tabpanel" aria-labelledby="contact-tab">
			<form  name="iayarlar" id="iayarlar" method="POST" action="inc/ayarlar.php"  onsubmit="return false;">
				<input type="hidden" name="dil" value="<?php echo $dil?>">
				
				<div class="form-group">
					<label for="adres">Adres</label>
					<input type="text" class="form-control" id="adres"  name="veri[adres]"  value="<?=ayargetir('adres',$dil)?>">
				</div>
				<div class="form-group">
					<label for="email">E Posta</label>
					<input type="text" class="form-control" id="email"  name="veri[email]"  value="<?=ayargetir('email',$dil)?>">
				</div>
				<div class="form-group">
					<label for="map">Map Kordinat</label>
					<input type="text" class="form-control" id="map"  name="veri[map]"  value="<?=ayargetir('map',$dil)?>">
				</div>
				<div class="form-group">
					<label for="whatsapp">Whatsapp</label>
					<input type="text" class="form-control" id="whatsapp"  name="veri[whatsapp]"  value="<?=ayargetir('whatsapp',$dil)?>">
				</div>

				<div class="form-group">
					<label for="telefon">Telefon</label>
					<input type="text" class="form-control" id="telefon"  name="veri[telefon]"  value="<?=ayargetir('telefon',$dil)?>">
				</div>

				<div class="form-group">
					<label for="facebook">Facebook</label>
					<input type="text" class="form-control" id="facebook"  name="veri[facebook]"  value="<?=ayargetir('facebook',$dil)?>">
				</div>
				<div class="form-group">
					<label for="twitter">Twitter</label>
					<input type="text" class="form-control" id="twitter"  name="veri[twitter]"  value="<?=ayargetir('twitter',$dil)?>">
				</div>
				<div class="form-group">
					<label for="instagram">Instagram</label>
					<input type="text" class="form-control" id="instagram"  name="veri[instagram]"  value="<?=ayargetir('instagram',$dil)?>">
				</div>
			
				<button type="submit" class="btn btn-dark"  onclick="AjaxFormS('iayarlar','form_status')">Kaydet</button>
			</form>
		</div>
	</div>
</div>