<?php echo !defined("guvenlik") ? die("Hata") : null; 
$dil = dilSec();
$query = $db->query("SELECT * FROM araclar where dil = '$dil' order by arac_id desc")->fetchAll();

dilMenu('araclar');
?>


<div class="shadow p-3">

	<div class="row">
		<div class="col-md-6"><h4>Araçlar</h4></div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#aracEkle">
       Araç Ekle
     </button>
   </div>
 </div>

 <hr>
 <table class="table table-bordered table-hover " >
  <thead>
    <tr>
      <th scope="col">Başlık</th>
      <th scope="col" class="text-right">İşlem</th>
    </tr>
  </thead>
  <tbody>
    <?
    foreach ($query as $key => $value) {
      ?>
      <tr>
        <td><?=$value['araci_adi']?></td>
        <td class="text-right">
          <button class="btn btn-danger btn-sm btn-sil" data-db="araclar" data-idname="arac_id" data-id="<?=$value['arac_id']?>" >Sil</button> 
          <button type="button" class="btn btn-warning btn-sm " data-toggle="modal" data-target="#Duzenle<?=$value['arac_id']?>" >Düzenle</button>
        </td>
      </tr>
      <?
    }
    ?>
  </tbody>
</table>
</div>
<div class="modal fade" id="aracEkle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Araç Ekle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card mb-3">
          <div class="card-header">
            <h6 class="mb-0">Araç Resmi</h6>
          </div>
          <div class="card-body">
            <form  name="icerikresim" id="icerikresim" method="POST" action="inc/icerikresim.php"  onsubmit="return false;">
              <div class="form-group">
                <input type="file"  name="resim" >
                <input type="hidden" name="crop" value="no">
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-dark btn-sm"  onclick="AjaxFormS('icerikresim','form_status')">Resim Yükle</button>
              </div>
            </form>
            <div >
              <img id="ironizleme" src="../images/blank.png" class="img-fluid" >
            </div>
          </div>
        </div>
        <form  name="aracekle" id="aracekle" method="POST" action="inc/islem.php"  onsubmit="return false;">
          <input type="hidden" id="resim" name="resim">
          <input type="hidden" name="islem" value="aekle">
          <input type="hidden" name="dil" value="<?php echo $dil?>">
          <div class="form-group">
            <label for="ad">Araç Adı</label>
            <input type="text" class="form-control" id="ad"  name="ad" >
          </div>
          <div class="form-group">
            <label for="arac_marka">Araç Marka</label>
            <input type="text" class="form-control" id="arac_marka"  name="arac_marka" >
          </div>
          <div class="form-group">
            <label for="yakit">Yakıt</label>
            <input type="text" class="form-control" id="yakit"  name="yakit" >
          </div>
          <div class="form-group">
            <label for="vites">Vites</label>
            <input type="text" class="form-control" id="vites"  name="vites" >
          </div>
          <div class="form-group">
            <label for="kilima">Kilima</label>
            <input type="text" class="form-control" id="kilima"  name="kilima" >
          </div>
          <div class="form-group">
            <label for="kapasite">Kapasite</label>
            <input type="text" class="form-control" id="kapasite"  name="kapasite" >
          </div>
          <div class="form-group">
            <label for="fiyat">Fiyat</label>
            <input type="text" class="form-control" id="fiyat"  name="fiyat" >
          </div>
          <div class="form-group">
            <label for="kapi">Kapı</label>
            <input type="text" class="form-control" id="kapi"  name="kapi" >
          </div>
          <div class="form-group">
            <label for="km">Km</label>
            <input type="text" class="form-control" id="km"  name="km" >
          </div>
		  <div class="form-group">
            <label for="km">Depozit Tutarı</label>
            <input type="text" class="form-control" id="depozit"  name="depozit" >
          </div>
          <div class="form-group">
            <label for="keyw">Durum</label>
            <select class="custom-select" name="durum">
              <option value="1">Aktif</option>
              <option value="0">Pasif</option>
            </select>
          </div>
          <button type="submit" class="btn btn-dark" onclick="AjaxFormS('aracekle','form_status')">Araç Ekle</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
      </div>
    </div>
  </div>
</div>
<?
foreach ($query as $row) {
  ?>
  <div class="modal fade" id="Duzenle<?=$row['arac_id']?>" tabindex="-1" role="dialog" aria-labelledby="DuzenleLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="DuzenleLabel">Araç Düzenle</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="card mb-3">
            <div class="card-header">
              <h6 class="mb-0">Araç Resmi</h6>
            </div>
            <div class="card-body">
              <form  name="icerikresim<?=$row['arac_id']?>" id="icerikresim<?=$row['arac_id']?>" method="POST" action="inc/icerikresim.php"  onsubmit="return false;">
                <input type="hidden" name="id" value="<?=$row['arac_id']?>">
                <div class="form-group">
                  <input type="file"  name="resim" >
                  <input type="hidden" name="crop" value="no">
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-dark btn-sm"  onclick="AjaxFormS('icerikresim<?=$row['arac_id']?>','form_status')">Resim Yükle</button>
                </div>
              </form>
              <div >
                <img id="ironizleme<?=$row['arac_id']?>" src="../i/site/<?=$row['arac_gorsel']?>" class="img-fluid" >
              </div>
            </div>
          </div>
          <form  name="aracedit<?=$row['arac_id']?>" id="aracedit<?=$row['arac_id']?>" method="POST" action="inc/islem.php"  onsubmit="return false;">
            <input type="hidden" name="islem" value="aedit">
            <input type="hidden" name="dil" value="<?php echo $dil?>">
            <input type="hidden" name="id" value="<?=$row['arac_id']?>">
            <input type="hidden" id="resim<?=$row['arac_id']?>" name="resim" value="<?=$row['arac_gorsel']?>">
            <div class="form-group">
              <label for="ad">Araç Adı</label>
              <input type="text" class="form-control" id="ad"  name="ad" value="<?=tirnaktemizle($row['araci_adi'])?>">
            </div>
            <div class="form-group">
            <label for="arac_marka">Araç Marka</label>
            <input type="text" class="form-control" id="arac_marka"  name="arac_marka" value="<?=tirnaktemizle($row['arac_marka'])?>">
          </div>
          <div class="form-group">
            <label for="fiyat">Fiyat</label>
            <input type="text" class="form-control" id="fiyat"  name="fiyat" value="<?=tirnaktemizle($row['fiyat'])?>">
          </div>
          <div class="form-group">
            <label for="yakit">Yakıt</label>
            <input type="text" class="form-control" id="yakit"  name="yakit" value="<?=tirnaktemizle($row['yakit'])?>">
          </div>
          <div class="form-group">
            <label for="vites">Vites</label>
            <input type="text" class="form-control" id="vites"  name="vites" value="<?=tirnaktemizle($row['vites'])?>">
          </div>
          <div class="form-group">
            <label for="kilima">Kilima</label>
            <input type="text" class="form-control" id="kilima"  name="kilima" value="<?=tirnaktemizle($row['kilima'])?>">
          </div>
          <div class="form-group">
            <label for="kapasite">Kapasite</label>
            <input type="text" class="form-control" id="kapasite"  name="kapasite" value="<?=tirnaktemizle($row['kapasite'])?>">
          </div>
          <div class="form-group">
            <label for="kapi">Kapı</label>
            <input type="text" class="form-control" id="kapi"  name="kapi" value="<?=tirnaktemizle($row['kapi'])?>">
          </div>
          <div class="form-group">
            <label for="km">Km</label>
            <input type="text" class="form-control" id="km"  name="km" value="<?=tirnaktemizle($row['km'])?>">
          </div>
<div class="form-group">
            <label for="km">Depozit Miktarı</label>
            <input type="text" class="form-control" id="depozit"  name="depozit" value="<?=tirnaktemizle($row['depozit'])?>">
          </div>
            <div class="form-group">
              <label for="keyw">Durum </label>
              <select class="custom-select" name="durum">
                <option value="1" <? echo $row['arac_durum'] == '1' ? 'selected':'' ?>>Aktif</option>
                <option value="0" <? echo $row['arac_durum'] == '0' ? 'selected':'' ?>>Pasif</option>
              </select>
            </div>
            <button type="submit" class="btn btn-dark" onclick="AjaxFormS('aracedit<?=$row['arac_id']?>','form_status')">Düzenle</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
        </div>
      </div>
    </div>
  </div>
  <?
}
?>