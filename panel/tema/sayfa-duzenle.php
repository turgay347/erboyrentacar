<?php echo !defined("guvenlik") ? die("Hata") : null; 
$detay= $db->query("SELECT * FROM hazirsayfa WHERE id= $_GET[id] ")->fetch(PDO::FETCH_OBJ);
?>
<div class="shadow p-3">
	<h4>Sayfa Düzenle</h4>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<form  name="editorForm" id="editorForm" method="POST" action="inc/islem.php"  onsubmit="return false;">
				<input type="hidden" name="id" value="<?=$detay->id?>">
				<input type="hidden" name="islem" value="sayfaedit">
			  <div class="form-group">
			    <label for="baslik">Başlık</label>
			    <input type="text" class="form-control" id="baslik"  name="baslik" value="<?=tirnaktemizle($detay->baslik)?>" >
			  </div>
			  <div class="form-group">
			    <label for="icerik">İçerik</label>
			    <textarea name="editor " id="editor" cols="30" rows="20" class="form-control"><?=tirnaktemizle($detay->icerik)?></textarea>
			    <textarea name="editordetay" id="editordetay" cols="30" rows="10" class="d-none"></textarea>
			  </div>
			  <button type="submit" class="btn btn-dark" onclick="editorFormSucces()">Düzenle</button>
			</form>
		</div>
	</div>
</div>
