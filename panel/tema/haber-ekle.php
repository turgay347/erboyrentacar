<?php echo !defined("guvenlik") ? die("Hata") : null; 
?>
<div class="shadow p-3">
	<h4>Blog Ekle</h4>
	<hr>
	<div class="row">
		<div class="col-md-8">
			<form  name="blogekles" id="blogekles" method="POST" action="inc/islem.php"  onsubmit="return false;">
				<input type="hidden" name="resim" id="resim">
				<input type="hidden" name="islem" value="blogekle">
			  <div class="form-group">
			    <label for="baslik">Başlık</label>
			    <input type="text" class="form-control" id="baslik"  name="baslik" >
			  </div>

			  <?php formDilSec();?>
	
			  
			  <div class="form-group">
			    <label for="icerik">İçerik</label>
			    <textarea name="editor " id="editor" cols="30" rows="10" class="form-control"></textarea>
			    <textarea name="icerik" id="icerik" cols="30" rows="10" class="d-none"></textarea>
			  </div>
			  <button type="submit" class="btn btn-dark" onclick="blogekle()">Blog Ekle</button>
			</form>
		</div>
		<div class="col-md-4">
			<div class="card">
				<div class="card-header">
					<h6 class="mb-0">Blog Resmi</h6>
				</div>
				<div class="card-body">
					<form  name="blogresim" id="blogresim" method="POST" action="inc/blogresim.php"  onsubmit="return false;">
						<div class="form-group">
							<input type="file"  name="resim" >
							<input type="hidden" name="boyut" value="1">
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-dark btn-sm"  onclick="AjaxFormS('blogresim','form_status')">Resim Yükle</button>
						</div>
			        </form>
			        <div >
			        	<img id="bgonizleme" src="../images/blank.png" class="img-fluid" >
			        </div>
				</div>
			</div>
		</div>
	</div>
</div>
