<?php echo !defined("guvenlik") ? die("Hata") : null; 
$dil = dilSec();
$blogdetay= $db->query("SELECT * FROM bloglar WHERE id='$_GET[id]'")->fetch(PDO::FETCH_OBJ);
?>
<div class="shadow p-3">
	<h4>Haber Düzenle</h4>
	<hr>
	<div class="row">
		<div class="col-md-8">
			<form  name="blogekles" id="blogekles" method="POST" action="inc/islem.php"  onsubmit="return false;">
				<input type="hidden" name="id" value="<?=$blogdetay->id?>">
				<input type="hidden" name="islem" value="blogedit">
				<input type="hidden" name="resim" id="resim" value="<?=$blogdetay->resim?>">
			  <div class="form-group">
			    <label for="baslik">Başlık</label>
			    <input type="text" class="form-control" id="baslik"  name="baslik" value="<?=tirnaktemizle($blogdetay->baslik)?>" >
			  </div>


			  <?php formDilSecEdit($dil)?>
		
		
		
			  <div class="form-group">
			    <label for="icerik">İçerik</label>
			    <textarea name="editor " id="editor" cols="30" rows="5" class="form-control"><?=tirnaktemizle($blogdetay->icerik)?></textarea>
			    <textarea name="icerik" id="icerik" cols="30" rows="10" class="d-none"></textarea>
			  </div>
			  <button type="submit" class="btn btn-dark" onclick="blogekle()">Düzenle</button>
			</form>
		</div>
		<div class="col-md-4">
			<div class="card">
				<div class="card-header">
					<h6 class="mb-0">Haber Resmi</h6>
				</div>
				<div class="card-body">
					<form  name="blogresim" id="blogresim" method="POST" action="inc/blogresim.php"  onsubmit="return false;">
						<div class="form-group">
							<input type="file"  name="resim" > <input type="hidden" name="boyut" value="1">
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-dark btn-sm"  onclick="AjaxFormS('blogresim','form_status')">Resim Yükle</button>
						</div>
			        </form>
			        <div >
			        	<img id="bgonizleme" src="../i/site/<?=$blogdetay->resim?>" class="img-fluid" >
			        </div>
				</div>
			</div>
		</div>
	</div>
</div>
