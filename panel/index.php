<?
ob_start();
@session_start();
define("guvenlik",true);
include '../db/baglan.php';
include 'fonksiyonlar.php';


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin Panel</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <!-- <link rel="stylesheet" href="https://bootswatch.com/4/materia/bootstrap.min.css"> -->
        <link rel="stylesheet" href="css/style.css">
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-1.12.3.js" ></script>
        <link rel='stylesheet' href='https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'>
        <link rel='stylesheet' href='https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css'>
        <script src="https://maps.google.com/maps/api/js?key=AIzaSyBBBaMsydOGDPyL0quPS-6Glqne-bxNOvs&sensor=false&.js"></script>
    </head>
    <body class="<?php echo @$_SESSION['y_id'] == '' ? 'bg-dark':'' ?>">


        <?
            if (@$_SESSION['y_id'] == '') {
                ?>
                        <div class="container py-5">
                          <div class="row justify-content-md-center">

                            <div class="col-md-auto col-md-6">
                             <div class="card">
                                 <div class="card-header">
                                     <h5 class="mb-0">Giriş Sayfası</h5>
                                 </div>
                                <div class="card-body">
                                    <div id="form_status"></div>
                                    <form  name="login" id="login" method="POST" action="inc/login.php"  onsubmit="return false;">
                                      <div class="form-group">
                                        <label for="ka">Kullanıcı Adı</label>
                                        <input type="text" class="form-control" name="ka" >
                                      </div>
                                      <div class="form-group">
                                        <label for="sifre">Şifre</label>
                                        <input type="password" class="form-control" name="sifre" placeholder="Şifre">
                                      </div>
                                      <div class="text-right"><button type="submit" class="btn btn-dark" onclick="AjaxFormS('login','form_status')">Giriş Yap</button></div>
                                    </form>
                                </div>
                             </div>
                            </div>

                          </div>
                        </div>
                <?
            }
            else {
                ?>
                        <div class="wrapper">
                            <nav id="sidebar" class="shadow-lg ">
                                <div class="sidebar-header">
                                    <h3><a href="index.php">Yönetim Paneli</a></h3>
                                </div>

                                <ul class="list-unstyled components">
                                  <li class="first-link ">
                                        <a href="index.php?p=rezervasyonlar" >Rezervasyonlar</a>
                                    </li>
                                    <li class="first-link ">
                                        <a href="index.php?p=araclar" >Araçlar</a>
                                    </li>
                                    <li class="first-link ">
                                        <a href="index.php?p=slider" >Slider</a>
                                    </li>
                                    <li class="first-link ">
                                        <a href="index.php?p=ofisler" >Ofisler</a>
                                    </li>
                                    <li class="first-link ">
                                        <a href="#haberSubMenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Bloglar</a>
                                        <ul class="collapse list-unstyled <? showmenu('menu-3');?>" id="haberSubMenu">
                                            <li class="">
                                                <a href="index.php?p=haberler" >Bloglar</a>
                                            </li>
                                            <li class="">
                                                <a href="index.php?p=haber-ekle" >Blog Ekle</a>
                                            </li>
                                           
                                        </ul>
                                    </li>
                                    <li class="first-link ">
                                    <a href="index.php?p=sayfalar" >Tüm Sayfalar</a>
                                    </li>
                                    <li class="first-link ">
                                    <a href="index.php?p=genel-ayarlar" >Genel Ayarlar</a>
                                    </li>

                              
                                    <li class="first-link ">
                                        <a href="index.php?p=mailler" >Mailler</a>
                                    </li>
                                </ul>
                            </nav>

                            <div id="content">

                                <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow mb-3">
                                    <div class="container-fluid">

                                        <button type="button" id="sidebarCollapse" class="btn btn-info">
                                            <i class="fas fa-align-left"></i>
                                        </button>
                                        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                            <i class="fas fa-align-justify"></i>
                                        </button>

                                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                            <ul class="nav navbar-nav ml-auto">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="cikis.php">Çıkış</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                                <div id="form_status"></div>

                                <?  
                                    if(empty($p)){   
                                        include $pdir.'index.php';
                                    }elseif(file_exists($pdir.$p.'.php')){ 
                                        include $pdir.$p.'.php';
                                    }else{
                                        include "404.php";
                                    }
                                ?>
                            </div>
                        </div>
                <?
            }
        ?>



        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
        <script src="js/jquery.form.js" ></script>
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="libarry/datatables/dataTables.bootstrap4.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
        <script src=""></script>
        <script type="text/javascript" src="libarry/tinymce/tinymce.min.js"></script>

        <script src="js/main.js" ></script>


    </body>
</html>
