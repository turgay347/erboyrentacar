<?php
ob_start();
session_start();
require_once "api.php";
$api = new api();

    $type = (isset($_GET['type']) && $_GET['type']) ? $_GET['type'] : '';

    switch ($type) {
        case 'getStations':
            $name = (isset($_GET['name']) && $_GET['name']) ? $_GET['name'] : '';
            echo $api->getStations($name);
            break;
        case 'submit':
			if(searchFormValidate($_POST)){
				$Filter = $_POST['other'];
				$Filter = json_decode(base64_decode($Filter),TRUE);
				$Group = isset($_POST['group'])?$_POST['group']:"";
				
				if($_POST['from_station_name'] == ''){
					$from = "IZMIR HAVALIMANI IC HAT";
				}else{
					$from = $_POST['from_station_name'];
				}
				
				if($_POST['to_station_name'] == ''){
					$to = "IZMIR HAVALIMANI IC HAT";
				}else{
					$to = $_POST['to_station_name'];
				}

				
				$_SESSION['form_data'] = array(
					'station_name' => $_POST['station_name'],
					'to_station_name' => $from,
					'from_station_name' => $to,
					'station_code' => $_POST['station_code'],
					'from_station_code' => $_POST['from_station_code'],
					'to_station_code' => $_POST['to_station_code'],
					'startdate' => $_POST['startdate'],
					'startclock' => $_POST['startclock'],
					'enddate' => $_POST['enddate'],
					'endclock' => $_POST['endclock'],
					'selection' => $_POST['type'],
					'filters' => $Filter,
					'campaign' => $Group
				);
				echo json_encode(array('success' => 'true','redirect' => '/arac-secimi.php'));
				die();
			}
            break;
        case 'selectcar':
            $_SESSION['form_data']['car_token'] = $_POST['token'];		
            $_SESSION['form_data']['type'] = $_POST['type'];		
			echo json_encode(array('success' => 'true','redirect' => '/arac-ekstra.php'));
            break;
			
		case 'selectextras':
			$_SESSION['form_data']['extras'] = $_POST['extras'];	
			$_SESSION['form_data']['extrasval'] = $_POST['extrasval'];	
			echo json_encode(array('success' => 'true'));
            break;
			
		case 'saveForm':
			$form_result = $api->saveForm($_POST);	
            //$api->saveDb($_POST);	
			echo json_encode($form_result);
            break;
        }
		
		
function searchFormValidate($post){
	
	$error = array();
	if(!$post['station_name'] && !$post['station_code']){
		$error[] = "Teslim yeri seçilmelidir";
	}
	
	if(!$post['startdate'] && !$post['startclock']){
		$error[] = "Alış Tarih ve saati seçilmelidir";
	}
	
	if(!$post['enddate'] && !$post['endclock']){
		$error[] = "Teslim Tarih ve saati seçilmelidir";
	}
	
	if($error){
		return array(
			'status' => 'error',
			'messages' => $error
		);
	}else{
		return array('status' => 'success');
	}
	
	
}
?>

