<?php require('inc/admin.php'); yetkiKontrol('admin');  ?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			require('inc/head.php');
			$genel = $Baglanti->VeriCek("ayarlar","*");
            $ayarlar = array();
            foreach ($genel as $value):
                $ayarlar[$value['ayarKey']] = $value['ayarValue'];
            endforeach;
			if(@Islemler::FormKontrol("POST","frm","frmGeneral")):

			    unset($_POST['frm']);
			    $ok = 1;

			    $postArray = $_POST;

                foreach($postArray as $key => $value){
					
					if($key == 'iletisim_harita'){
						
					}else{
						$value = Islemler::replace($value);
					}
                    if(!$Baglanti->VeriGuncelle("ayarlar",["ayarValue" => $value]," WHERE ayarKey = '$key'")){
                        $ok = 0;
                        break;
                    }
                }
				
				

                $fotoKeys = array('site_logo','site_favicon','sektor_foto','kutu1_foto','kutu2_foto','kutu3_foto','kutu4_foto');

                foreach($fotoKeys as $key){
                    $resim = Islemler::resimyukle($_FILES[$key],Islemler::seo(rand(0,9999999)),"../images/genel/");
                    if($resim[0]=="tamam"){ $_REQUEST[$key] = $resim[1];}else{$_REQUEST[$key]=$ayarlar[$key];}
                    if(!$Baglanti->VeriGuncelle("ayarlar",["ayarValue" => $_REQUEST[$key]]," WHERE ayarKey = '$key'")){$ok=0;}
                }

                if($ok == 1){
                    echo Islemler::alert('Ayarlar güncellendi!','ayarlar.php');
                }else{
                    echo Islemler::alert('Bir hata oluştu!','ayarlar.php');
                }

			endif;
		?>
	</head>
	<body>
		<?php require('inc/header.php'); ?>
		<?php require('inc/sidebar.php'); ?>
		<div class="content">
			<ul class="breadcrumb">
				<li><a href="admin.php"><i class="fa fa-home"></i></a></li>
				<li class="active"><a>Genel Ayarlar</a> </li>
			</ul>
			<form action="" method="post" enctype="multipart/form-data">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab1" data-toggle="tab" style="color:#222;"><i class="fa fa-info"></i> Site Bilgileri</a></li>
                    <li><a href="#tab7" data-toggle="tab" style="color:#222;"><i class="fa fa-flag"></i> Hakkımızda</a></li>
					<!--li><a href="#tab2" data-toggle="tab" style="color:#222;"><i class="fa fa-instagram"></i> Sosyal medya</a></li>
                    <li><a href="#tab4" data-toggle="tab" style="color:#222;"><i class="fa fa-pencil"></i> Odak Noktalarımız</a></li>
                    <li><a href="#tab3" data-toggle="tab" style="color:#222;"><i class="fa fa-pencil"></i> Sektörlerimiz ve Hedefler</a></li>
                    <li><a href="#tab5" data-toggle="tab" style="color:#222;"><i class="fa fa-pencil"></i> Rakamlar</a></li>
                    <li><a href="#tab6" data-toggle="tab" style="color:#222;"><i class="fa fa-pencil"></i> Haberler</a></li>
                    
                    <li><a href="#tab8" data-toggle="tab" style="color:#222;"><i class="fa fa-flag"></i> Sektörler</a></li>
                    <li><a href="#tab9" data-toggle="tab" style="color:#222;"><i class="fa fa-flag"></i> İletişim</a></li-->

				</ul>
				<div class="panel panel-default">
				
					<div class="panel-body">
						<div class="tab-content">
						    	<div name="tab1" id="tab1" class="tab-pane active" >
						    		<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label">Site Adı : </label>
											<input type="text" class="form-control"  name="site_baslik" value="<?=$ayarlar["site_baslik"]; ?>" />
										</div>
                                        <div class="form-group">
                                            <label class="control-label">Logo : </label><br>
                                            <div style="background:#333; display: inline-block;"><img src="../images/genel/<?=$ayarlar['site_logo']?>" height="60px"/></div>
                                            <input type="file" class="form-control"  name="site_logo" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Favicon (16x16 veya 32x32) : </label><br>
                                            <div style="background:#333: inline-block;"><img src="../images/genel/<?=$ayarlar['site_favicon']?>" height="32px"/></div>
                                            <input type="file" class="form-control"  name="site_favicon" />
                                        </div>
										
										<div class="form-group">
											<label class="control-label">Footer Açıklama : </label>
											<input type="text" class="form-control"  name="footer_aciklama" value="<?=$ayarlar["footer_aciklama"]; ?>" />
										</div>
										
										<div class="form-group">
											<label class="control-label">Footer Copyright : </label>
											<input type="text" class="form-control"  name="footer_copyright" value="<?=$ayarlar["footer_copyright"]; ?>" />
										</div>
									</div>
								</div>
							</div>
							<div name="tab2" id="tab2" class="tab-pane" >
								<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Facebook adresi : </label>
									<input type="text" class="form-control"  name="facebook" value="<?=$ayarlar["facebook"]; ?>" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Instagram adresi : </label>
									<input type="text" class="form-control"  name="instagram" value="<?=$ayarlar["instagram"]; ?>" />
								</div>
							</div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Twitter adresi : </label>
                                            <input type="text" class="form-control"  name="twitter" value="<?=$ayarlar["twitter"]; ?>" />
                                        </div>
                                    </div>
                                   
							</div>
							</div>
							
							<div name="tab3" id="tab3" class="tab-pane" >
								<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label class="control-label">Sektörlerimiz ve Hedefler : </label>
									<textarea class="ckeditor" name="sektor_text"><?=$ayarlar['sektor_text']?></textarea>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
                                            <label class="control-label">Fotoğraf : </label><br>
                                            <div style="background:#333: inline-block;"><img src="../images/genel/<?=$ayarlar['sektor_foto']?>" height="200px"/></div>
                                            <input type="file" class="form-control"  name="sektor_foto" />
                                        </div>
							</div>
							
                                   
							</div>
							</div>
							
							<div name="tab4" id="tab4" class="tab-pane" >
								<div class="row">
								<div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Sağ Slogan : </label>
                                            <input type="text" class="form-control"  name="slogan" value="<?=Islemler::replace($ayarlar["slogan"]); ?>" />
                                        </div>
                                    </div>
								<?php for($i = 1; $i <= 4; $i++){ ?>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Kutu <?=$i?> Başlık : </label>
									<input class="form-control" type="text" name="kutu<?=$i?>_baslik" value="<?=$ayarlar['kutu'.$i.'_baslik']?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
                                            <label class="control-label">Kutu <?=$i?> Fotoğraf : </label><br>
                                            <div style="background:#333: inline-block;"><img src="../images/genel/<?=$ayarlar['kutu'.$i.'_foto']?>" height="50px"/></div>
                                            <input type="file" class="form-control"  name="kutu<?=$i?>_foto" />
                                        </div>
							</div>
								<?php } ?>
							
                                   
							</div>
							</div>
							
							<div name="tab5" id="tab5" class="tab-pane" >
								<div class="row">
								<?php for($i = 1; $i <= 4; $i++){ ?>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Sayı <?=$i?> : </label>
									<input class="form-control" type="text" name="sayi<?=$i?>" value="<?=$ayarlar['sayi'.$i]?>">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Alt Başlık <?=$i?>  : </label>
									<input class="form-control" type="text" name="yazi<?=$i?>" value="<?=$ayarlar['yazi'.$i]?>">
								</div>
							</div>
								<?php } ?>
							
                                   
							</div>
							</div>
							
							<div name="tab6" id="tab6" class="tab-pane" >
								<div class="row">
								
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Haberler Açıklama  : </label>
									<textarea class="ckeditor" name="haber_aciklama"><?=$ayarlar['haber_aciklama']?></textarea>
								</div>
							</div>
							
								
							
                                   
							</div>
							</div>
							
							<div name="tab7" id="tab7" class="tab-pane" >
								<div class="row">
								
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Hakkımızda Sayfası  : </label>
									<textarea class="ckeditor" name="hakkimizda_sayfa"><?=$ayarlar['hakkimizda_sayfa']?></textarea>
								</div>
							</div>
							
								
							
                                   
							</div>
							<div class="row">
								
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Vizyon Sayfası  : </label>
									<textarea class="ckeditor" name="vizyon_sayfa"><?=$ayarlar['vizyon_sayfa']?></textarea>
								</div>
							</div>
							
								
							
                                   
							</div>
							<div class="row">
								
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Misyon Sayfası  : </label>
									<textarea class="ckeditor" name="misyon_sayfa"><?=$ayarlar['misyon_sayfa']?></textarea>
								</div>
							</div>
							
								
							
                                   
							</div>
							</div>
							
							<div name="tab8" id="tab8" class="tab-pane" >
								<div class="row">
								
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Sektörler Sayfası  : </label>
									<textarea class="ckeditor" name="sektorler_sayfa"><?=$ayarlar['sektorler_sayfa']?></textarea>
								</div>
							</div>
							
								
							
                                   
							</div>
							</div>
							
							<div name="tab9" id="tab9" class="tab-pane" >
								<div class="row">
								
								
								<div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Harita iframe kodu : </label>
                                            <input type="text" class="form-control"  name="iletisim_harita" value="<?=Islemler::replace($ayarlar["iletisim_harita"]); ?>" />
                                        </div>
                                    </div>
									</div>
									<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">İletişim Bilgileri  : </label>
									<textarea class="ckeditor" name="iletisim_bilgi"><?=$ayarlar['iletisim_bilgi']?></textarea>
								</div>
							</div>
							
								
							
                                   
							</div>
							</div>


						</div>
					</div>
					<div class="panel-footer ">
						<button type="submit" class="btn green">Kaydet</button>
					</div>
				</div>
				<input type="hidden" name="frm" value="frmGeneral" />
			</form>
			<?php require('inc/footer.php'); ?>
		</div>


	</body>
</html>
