<?php require('inc/admin.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <?php require("inc/head.php"); ?>
    <?php
    if(isset($_GET["delete"]))
    {
        $id = $_GET["delete"];
        if($Baglanti->VeriSil("logolar","WHERE logoID = $id"))
        {
            echo Islemler::alert('Başarıyla silindi!','referanslar.php');
        }
        else {
            echo Islemler::alert('Bir hata oluştu!','referanslar.php');
        }
    }
    ?>
</head>
<body>
<?php require("inc/header.php"); ?>
<?php require("inc/sidebar.php"); ?>
<div class="content">
    <ul class="breadcrumb">
        <li><a href="admin.php"><i class="fa fa-home"></i></a></li>
        <li class="active"><a>Referanslar</a></li>
        <li class="last"><a href="pages/referansekle.php">Yeni</a></li>
    </ul>
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Logolar</h3></div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-hover ">
                <thead>
                <tr>
                    <th>Sıra</th>
                    <th>Başlık</th>
                    <th>Fotoğraf</th>
                    <th>İşlem</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($Baglanti->VeriCek("logolar","*"," ORDER BY logoSira ASC") as $sayfa){ ?>
                    <tr>
                        <td>#<?=$sayfa["logoSira"]; ?></td>
                        <td><?=$sayfa["logoBaslik"]; ?></td>
                        <td>
                            <img src="../images/logolar/<?=$sayfa['logoFoto'];?>" width="300px" height="100px"/>
                        </td>
                        <td>
                            <a href="pages/referansduzenle.php?id=<?=$sayfa['logoID']; ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                            <a  href="referanslar.php?delete=<?=$sayfa['logoID'];?>" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>


    <?php require('inc/footer.php'); ?>
</div>

</body>
</html>
