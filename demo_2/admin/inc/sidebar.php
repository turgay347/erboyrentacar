<div class="sidebar">
    <ul class="sidebar">
        <li><a href="admin.php"><i class="fa fa-home"></i> Ana Sayfa</a></li>
		<li><a href="slider.php"><i class="fa fa-photo"></i> Slider Yönetimi</a></li>
        <!--li><a href="referanslar.php"><i class="fa fa-flag"></i> Referanslar</a></li-->
        <li><a href="blog.php"><i class="fa fa-newspaper-o"></i> Ürünler</a></li>
        <li><a href="tanitim.php"><i class="fa fa-newspaper-o"></i> Tanıtımlar</a></li>
        <?php if($admin[0]['yetki'] == 'admin'){ ?>
        <li><a href="iletisim.php"><i class="fa fa-envelope-o"></i> İletişim</a></li>
        <li><a href="yoneticiler.php"><i class="fa fa-cog"></i> Yöneticiler</a></li>
        
        <li><a href="ayarlar.php"><i class="fa fa-cogs"></i> Genel Ayarlar</a></li>
        <?php } ?>
    	<li><a href="logout.php"><i class="fa fa-sign-out"></i> Çıkış Yap</a></li>
    </ul>
</div>
