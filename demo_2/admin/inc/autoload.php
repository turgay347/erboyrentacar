<?php
function __autoload($class_name) {
    $dir = "_class/".$class_name.".php";

    if(file_exists($dir))
    {
      require_once $dir;
    }
    elseif(file_exists("../".$dir))
    {
      require_once "../".$dir;
    }
    elseif(file_exists("admin/".$dir))
    {
      require_once("admin/".$dir);
    }

}
?>
