<?php require('inc/admin.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <?php require("inc/head.php"); ?>
    <?php
    if(isset($_GET["delete"]))
    {
        $id = $_GET["delete"];
        if($Baglanti->VeriSil("iletisim","WHERE iletisim_id = $id"))
        {
            echo Islemler::alert('Başarıyla silindi!','iletisim.php');
        }
        else {
            echo Islemler::alert('Bir hata oluştu!','iletisim.php');
        }
    }
    ?>
</head>
<body>
<?php require("inc/header.php"); ?>
<?php require("inc/sidebar.php"); ?>
<div class="content">
    <ul class="breadcrumb">
        <li><a href="admin.php"><i class="fa fa-home"></i></a></li>
        <li class="active"><a>İletişim</a></li>
    </ul>
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Formlar</h3></div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-hover ">
                <thead>
                <tr>
                    <th>Ad Soyad</th>
                    <th>E-posta</th>
					<th>Başlık</th>
                    <th>Mesaj</th>
                    <th>IP Adresi</th>
                    <th>Tarih</th>
                    <th>İşlem</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($Baglanti->VeriCek("iletisim","*","WHERE 1=1 ORDER BY iletisim_id DESC") as $sayfa){ ?>
                    <tr>
                        <td><?=$sayfa["iletisim_ad"]; ?></td>
                        <td><?=$sayfa["iletisim_email"]; ?></td>
                        <td><?=$sayfa["iletisim_konu"]; ?></td>
                        <td><?=$sayfa["iletisim_mesaj"]; ?></td>
                        <td><?=$sayfa["iletisim_ip"]; ?></td>
                        <td><?=date("d.m.Y H:i",strtotime($sayfa["iletisim_tarih"])); ?></td>

                        <td>
                            <a  href="iletisim.php?delete=<?=$sayfa['iletisim_id'];?>" class="btn btn-xs btn-danger"><i class="fa fa-times"></i> Sil</a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php require('inc/footer.php'); ?>
</div>

</body>
</html>
