<?php require('inc/admin.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <?php require("inc/head.php"); ?>
    <?php
    if(isset($_GET["delete"]))
    {
        $id = $_GET["delete"];
        if($Baglanti->VeriSil("blog","WHERE blogID = $id"))
        {
            echo Islemler::alert('Başarıyla silindi!','blog.php');
        }
        else {
            echo Islemler::alert('Bir hata oluştu!','blog.php');
        }
    }
    if(isset($_GET["deleteKg"]))
    {
        $id = $_GET["deleteKg"];
        if($Baglanti->VeriSil("blog_kategoriler","WHERE bkID = $id"))
        {
            echo Islemler::alert('Başarıyla silindi!','blog.php');
        }
        else {
            echo Islemler::alert('Bir hata oluştu!','blog.php');
        }
    }
    ?>
</head>
<body>
<?php require("inc/header.php"); ?>
<?php require("inc/sidebar.php"); ?>
<div class="content">
    <ul class="breadcrumb">
        <li><a href="admin.php"><i class="fa fa-home"></i></a></li>
        <li class="active"><a>Ürünler</a></li>
        <li class="last"><a href="pages/blogekle.php">Yeni</a></li>
    </ul>
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Ürünler</h3></div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-hover ">
                <thead>
                <tr>
                    <th>Başlık</th>
                    <th>Url</th>
                    <th>Yayınlanma Tarihi</th>
                    <th>İşlem</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($Baglanti->VeriCek("blog","*","WHERE 1=1 ORDER BY blogTarih DESC") as $sayfa){ ?>
                    <tr>
                        <td><?=$sayfa["blogBaslik"]; ?></td>
                        <td><?=$sayfa["blogSeo"]; ?></td>
                        <td><?=date("d.m.Y H:i",strtotime($sayfa["blogTarih"])); ?></td>

                        <td>
                             <a href="pages/blogduzenle.php?id=<?=$sayfa['blogID']; ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                            <a  href="blog.php?delete=<?=$sayfa['blogID'];?>" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php require('inc/footer.php'); ?>
</div>

</body>
</html>
