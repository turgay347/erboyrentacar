<?php require('../inc/admin.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <?php
    require('../inc/head.php');
    if(@Islemler::FormKontrol("POST","frm","frmAddCategories"))
    {

        $resim = Islemler::resimyukle($_FILES["logoFoto"],Islemler::seo(rand(0,9999999)),"../../images/logolar/");
        if($resim[0]=="tamam"){ $_REQUEST["logoFoto"] = $resim[1];}
        if($Baglanti->FormVeriEkle('logolar',["logoBaslik","logoSira","logoFoto"]))
        {
            echo Islemler::alert("Başarıyla eklendi!","pages/referansekle.php?type=0");
        }
        else {
            echo Islemler::alert("Bir hata oluştu!","pages/referansekle.php?type=0");
        }
    }
    ?>
</head>
<body>
<?php require('../inc/header.php'); ?>
<?php require('../inc/sidebar.php'); ?>
<div class="content">
    <ul class=" breadcrumb">
        <li><a href="admin.php"><i class="fa fa-home"></i></a> </li>
        <li><a href="referanslar.php">Referanslar</a> </li>
        <li>Yeni Ekle</li>
    </ul>
    <form action="" method="post"  enctype="multipart/form-data">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Yeni Ekle</h3></div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label">Sıra :</label>
                    <input type="number" name="logoSira" class="form-control" required />
                </div>

                <div class="form-group">
                    <label class="control-label">Başlık :</label>
                    <input type="text" name="logoBaslik" maxlength="60" class="form-control" />
                </div>

                <div class="form-group">
                    <label class="control-label">Fotoğraf seç :</label>
                    <input type="file" class="form-control" name="logoFoto">
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Kaydet</button>
            </div>
            <input type="hidden" name="frm" value="frmAddCategories" />
        </div>
    </form>
    <?php require('../inc/footer.php'); ?>
</div>

</body>
</html>
