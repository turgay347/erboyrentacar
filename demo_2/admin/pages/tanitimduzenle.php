<?php require('../inc/admin.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <?php
    require("../inc/head.php");
    if(isset($_GET["id"]))
    {
        $id = $_GET["id"];
        $sayfalar = $Baglanti->VeriCek("urunler","*","WHERE id = $id");
        if(@Islemler::FormKontrol('POST','frm','frmEditPages'))
        {


                $resim = Islemler::resimyukle($_FILES["foto"],Islemler::seo(rand(0,9999999)),"../../images/tanitim/");
                if($resim[0]=="tamam"){ $_REQUEST["foto"] = $resim[1];}else{
                    $_REQUEST["foto"] = $sayfalar[0]["foto"];
                }
				
	
						$_REQUEST['baslik1'] = Islemler::replace($_REQUEST['baslik1']);
						$_REQUEST['baslik2'] = Islemler::replace($_REQUEST['baslik2']);
						$_REQUEST['baslik3'] = Islemler::replace($_REQUEST['baslik3']);
						$_REQUEST['aciklama'] = Islemler::replace($_REQUEST['aciklama']);
						$_REQUEST['butonurl'] = Islemler::replace($_REQUEST['butonurl']);

            if($Baglanti->FormVeriGuncelle("urunler",["baslik1","baslik2","baslik3","aciklama","foto","butonurl","tip"],"WHERE id = $id")){
                echo Islemler::alert("Başarıyla güncellendi!","pages/tanitimduzenle.php?id=$id");
            }
            else {
                echo Islemler::alert("Bir hata oluştu!","pages/tanitimduzenle.php?id=$id");
            }
        }
    }
    ?>
</head>
<body>
<?php require('../inc/header.php'); ?>
<?php require('../inc/sidebar.php'); ?>
<div class="content">
    <ul class=" breadcrumb">
        <li><a href="admin.php"><i class="fa fa-home"></i></a> </li>
        <li><a href="tanitim.php">Tanıtımlar</a></li>
        <li>Tanıtım Düzenle</li>
    </ul>
    <form action="" method="post"  enctype="multipart/form-data">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="form-group">
                    <label class="control-label">Başlık (Kalın) :</label>
                    <input type="text" name="baslik1" value="<?=$sayfalar[0]['baslik1']?>" maxlength="60" class="form-control" />
                </div>
				
				
                <div class="form-group">
                    <label class="control-label">Başlık (Kırmızı) :</label>
                    <input type="text" name="baslik2" value="<?=$sayfalar[0]['baslik2']?>" maxlength="60" class="form-control" />
                </div>
				
				
                <div class="form-group">
                    <label class="control-label">Başlık (ince) :</label>
                    <input type="text" name="baslik3" value="<?=$sayfalar[0]['baslik3']?>" maxlength="60" class="form-control" />
                </div>
				


                <div class="form-group">
                    <label class="control-label">Açıklama :</label>
                    <textarea class="ckeditor" name="aciklama"><?=$sayfalar[0]['aciklama']?></textarea>
                </div>
				
		
				
	

                    <div class="form-group">
						<img src="../images/tanitim/<?=$sayfalar[0]['foto']?>" width="300px" height="100px"/><br>
                        <label class="control-label">Fotoğraf seç (değiştirmek istemiyorsanız yükleme yapmayınız):</label>
                        <input type="file" class="form-control" name="foto">
                    </div>
					
					<div class="form-group">
                    <label class="control-label">Buton URL :</label>
                    <input type="text" name="butonurl" value="<?=$sayfalar[0]['butonurl']?>" maxlength="60" class="form-control" />
                </div>
				
				<div class="form-group">
                    <label class="control-label">Gösterim</label>
                    <select name="tip" class="form-control">
						<option value="0" <?php if($sayfalar[0]['tip'] == 0){echo 'selected';} ?>>Açıklama solda, fotoğraf sağda</option>
						<option value="1"  <?php if($sayfalar[0]['tip'] == 1){echo 'selected';} ?>>Açıklama sağda, fotoğraf solda</option>
					</select>
                </div>
					
					

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Kaydet</button>
            </div>
        </div>
        <input type="hidden" name="frm" value="frmEditPages" />
    </form>
    <?php require('../inc/footer.php'); ?>
</div>

</body>
</html>
