<?php require('../inc/admin.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <?php
    require('../inc/head.php');
    if(@Islemler::FormKontrol("POST","frm","frmAddCategories"))
    {

            $resim = Islemler::resimyukle($_FILES["blogFoto"],Islemler::seo(rand(0,9999999)),"../../images/blog/");
            if($resim[0]=="tamam"){ $_REQUEST["blogFoto"] = $resim[1];}else{
                $_REQUEST["blogFoto"] = "";
            }
			
			$resim = Islemler::resimyukle($_FILES["blogTags"],Islemler::seo(rand(0,9999999)),"../../images/blog/");
            if($resim[0]=="tamam"){ $_REQUEST["blogTags"] = $resim[1];}else{
                $_REQUEST["blogTags"] = "";
            }
			
			$_REQUEST['blogBaslik'] = Islemler::replace($_REQUEST['blogBaslik']);
			$_REQUEST['blogAciklama'] = Islemler::replace($_REQUEST['blogAciklama']);
			$_REQUEST['blogTags'] = Islemler::replace($_REQUEST['blogTags']);

			//$_REQUEST["blogAciklama"] = str_replace("'","\'",$_REQUEST["blogAciklama"]);
            $_REQUEST['blogSeo'] = Islemler::seo(rand(1111,9999)."-".$_REQUEST['blogBaslik']);

        if($Baglanti->FormVeriEkle('blog',["blogBaslik","blogAciklama","blogFoto","blogSeo","blogTags"]))
        {
            echo Islemler::alert("Başarıyla eklendi!","pages/blogekle.php");
        }
        else {
            echo Islemler::alert("Bir hata oluştu!","pages/blogekle.php");
        }
    }
    ?>
</head>
<body>
<?php require('../inc/header.php'); ?>
<?php require('../inc/sidebar.php'); ?>
<div class="content">
    <ul class=" breadcrumb">
        <li><a href="admin.php"><i class="fa fa-home"></i></a> </li>
        <li><a href="blog.php">Ürünler</a> </li>
        <li>Yeni Ekle</li>
    </ul>
    <form action="" method="post"  enctype="multipart/form-data">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Yeni Ekle</h3></div>
            <div class="panel-body">



                <div class="form-group">
                    <label class="control-label">Başlık :</label>
                    <input type="text" name="blogBaslik" maxlength="60" class="form-control" />
                </div>
				
				

                <div class="form-group">
                    <label class="control-label">Açıklama :</label>
                    <textarea class="ckeditor" name="blogAciklama"></textarea>
                </div>
				


                    <div class="form-group">
                        <label class="control-label">Fotoğraf seç :</label>
                        <input type="file" class="form-control" name="blogFoto">
                    </div>
					
					<div class="form-group">
                        <label class="control-label">Logo seç :</label>
                        <input type="file" class="form-control" name="blogTags">
                    </div>




            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Kaydet</button>
            </div>
            <input type="hidden" name="frm" value="frmAddCategories" />
        </div>
    </form>
    <?php require('../inc/footer.php'); ?>
</div>

</body>
</html>
