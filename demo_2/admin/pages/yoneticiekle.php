<?php require('../inc/admin.php'); yetkiKontrol('admin');  ?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			require('../inc/head.php');

			if(isset($_POST['frm']) and $_POST['frm']=='frmAddUsers'){
				$_REQUEST["sifre"] = Islemler::sifrele($_POST["sifre"]);
				if($Baglanti->FormVeriEkle("kullanicilar",["adsoyad","email","sifre","yetki"])){
					echo Islemler::alert("Başarıyla eklendi!","pages/yoneticiekle.php");
				}else{
					echo Islemler::alert('Bir hata oluştu!',"pages/yoneticiekle.php");
				}
			}
		?>
	</head>
	<body>
		<?php require('../inc/header.php'); ?>
		<?php require('../inc/sidebar.php'); ?>
		<div class="content">
			<ul class=" breadcrumb">
				<li><a href="admin.php"><i class="fa fa-home"></i></a> </li>
				<li><a href="yoneticiler.php">Yöneticiler</a> </li>
				<li><a>Yeni Ekle</a></li>
			</ul>
			<form action="" method="post" enctype="multipart/form-data">
				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Yeni Ekle</h3></div>
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label">Ad Soyad :</label>
							<input type="text" name="adsoyad" class="form-control" required />
						</div>
						<div class="form-group">
							<label class="control-label">Email :</label>
							<input type="email" name="email" class="form-control" required />
						</div>
						<div class="form-group">
							<label class="control-label">Şifre :</label>
							<input type="password" name="sifre" class="form-control" required />
						</div>
                        <div class="form-group">
                            <label class="control-label">Tip : </label>
                            <select name="yetki" class="form-control">
                                <option value="admin">Admin</option>
                                <option value="editor">Editör</option>
                            </select>
                        </div>
					</div>
					<div class="panel-footer">
						<button type="submit" class="btn btn-success">Kaydet</button>
					</div>
					<input type="hidden" name="frm" value="frmAddUsers" />
				</div>
			</form>
			<?php require('../inc/footer.php'); ?>
		</div>

	</body>
</html>
