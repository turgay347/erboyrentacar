<?php require('../inc/admin.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <?php
    require("../inc/head.php");
    if(isset($_GET["id"]))
    {
        $id = $_GET["id"];
        $sayfalar = $Baglanti->VeriCek("logolar","*","WHERE logoID = $id");
        if(@Islemler::FormKontrol('POST','frm','frmEditPages'))
        {
            $resim = Islemler::resimyukle($_FILES["logoFoto"],Islemler::seo(rand(0,999999)),"../../images/logolar");
            if($resim[0]=="tamam"){ $_REQUEST["logoFoto"] = $resim[1];}else{
                $_REQUEST["logoFoto"] = $sayfalar[0]['logoFoto'];
            }

            if($Baglanti->FormVeriGuncelle("logolar",["logoBaslik","logoSira","logoFoto"],"WHERE logoID = $id")){
                echo Islemler::alert("Başarıyla güncellendi!","pages/referansduzenle.php?id=$id");
            }
            else {
                echo Islemler::alert("Bir hata oluştu!","pages/referansduzenle.php?id=$id");
            }
        }
    }
    ?>
</head>
<body>
<?php require('../inc/header.php'); ?>
<?php require('../inc/sidebar.php'); ?>
<div class="content">
    <ul class=" breadcrumb">
        <li><a href="admin.php"><i class="fa fa-home"></i></a> </li>
        <li><a href="referanslar.php">Referanslar</a></li>
        <li>Düzenle</li>
    </ul>
    <form action="" method="post"  enctype="multipart/form-data">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label">Sıra :</label>
                    <input type="number" name="logoSira" class="form-control" value="<?=$sayfalar[0]['logoSira']?>" required />
                </div>

                <div class="form-group">
                    <label class="control-label">Başlık :</label>
                    <input type="text" name="logoBaslik" maxlength="60" value="<?=$sayfalar[0]['logoBaslik']?>" class="form-control" />
                </div>

                <div class="form-group">
				<img src="../images/logolar/<?=$sayfalar[0]['logoFoto']?>" width="300px" height="100px"/><br >
                    <label class="control-label">Fotoğraf seç (değiştirilmeyecekse seçmeyiniz) :</label>
                    <input type="file" class="form-control" name="logoFoto">
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Kaydet</button>
            </div>
        </div>
        <input type="hidden" name="frm" value="frmEditPages" />
    </form>
    <?php require('../inc/footer.php'); ?>
</div>

</body>
</html>
