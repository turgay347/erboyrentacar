<?php require('../inc/admin.php'); ?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			require('../inc/head.php');
			if(@Islemler::FormKontrol("POST","frm","frmAddCategories"))
			{

				$resim = Islemler::resimyukle($_FILES["sliderFoto"],Islemler::seo(rand(0,9999999)),"../../images/slider/");
				$_REQUEST['sliderBaslikBeyaz'] = Islemler::replace($_REQUEST['sliderBaslikBeyaz']);
					$_REQUEST['sliderAciklama'] = Islemler::replace($_REQUEST['sliderAciklama']);
				if($resim[0]=="tamam"){ $_REQUEST["sliderFoto"] = $resim[1];}
				if($Baglanti->FormVeriEkle('slider',["sliderSira","sliderBaslikBeyaz","sliderAciklama","sliderButonUrl","sliderFoto"]))
				{
					echo Islemler::alert("Başarıyla eklendi!","pages/sliderekle.php");
				}
				else {
					echo Islemler::alert("Bir hata oluştu!","pages/sliderekle.php");
				}
			}
		?>
	</head>
	<body>
		<?php require('../inc/header.php'); ?>
		<?php require('../inc/sidebar.php'); ?>
		<div class="content">
			<ul class=" breadcrumb">
				<li><a href="admin.php"><i class="fa fa-home"></i></a> </li>
				<li><a href="slider.php">Slider</a> </li>
				<li>Yeni Ekle</li>
			</ul>
			<form action="" method="post"  enctype="multipart/form-data">
				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Yeni Ekle</h3></div>
					<div class="panel-body">
					<div class="form-group">
							<label class="control-label">Sıra :</label>
							<input type="number" name="sliderSira" class="form-control" required />
						</div>

                        <div class="form-group">
                            <label class="control-label">Slider başlık</label>
                            <input type="text" name="sliderBaslikBeyaz" maxlength="60" class="form-control" />
                        </div>
						<!--
						<div class="form-group">
                            <label class="control-label">Slider alt başlık</label>
                            <input type="text" name="sliderBaslikMavi" maxlength="60" class="form-control" />
                        </div>
						
						<div class="form-group">
                            <label class="control-label">Buton yazı</label>
                            <input type="text" name="sliderButonText" maxlength="60" class="form-control" />
                        </div>
						-->
                        <div class="form-group">
                            <label class="control-label">Buton URL</label>
                            <input type="text" name="sliderButonUrl" class="form-control" />
                        </div>
						
						<div class="form-group">
							<label class="control-label">Slider açıklama :</label>
							<textarea class="form-control" name="sliderAciklama"></textarea>
						</div>
						
						<div class="form-group">
							<label class="control-label">Fotoğraf seç :</label>
							<input type="file" class="form-control" name="sliderFoto">
						</div>
					</div>
					<div class="panel-footer">
						<button type="submit" class="btn btn-success">Kaydet</button>
					</div>
					<input type="hidden" name="frm" value="frmAddCategories" />
				</div>
			</form>
			<?php require('../inc/footer.php'); ?>
		</div>

	</body>
</html>
