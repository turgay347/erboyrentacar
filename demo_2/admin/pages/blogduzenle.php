<?php require('../inc/admin.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <?php
    require("../inc/head.php");
    if(isset($_GET["id"]))
    {
        $id = $_GET["id"];
        $sayfalar = $Baglanti->VeriCek("blog","*","WHERE blogID = $id");
        if(@Islemler::FormKontrol('POST','frm','frmEditPages'))
        {


                $resim = Islemler::resimyukle($_FILES["blogFoto"],Islemler::seo(rand(0,9999999)),"../../images/blog/");
                if($resim[0]=="tamam"){ $_REQUEST["blogFoto"] = $resim[1];}else{
                    $_REQUEST["blogFoto"] = $sayfalar[0]["blogFoto"];
                }
				
				$resim = Islemler::resimyukle($_FILES["blogTags"],Islemler::seo(rand(0,9999999)),"../../images/blog/");
                if($resim[0]=="tamam"){ $_REQUEST["blogTags"] = $resim[1];}else{
                    $_REQUEST["blogTags"] = $sayfalar[0]["blogTags"];
                }
						$_REQUEST['blogBaslik'] = Islemler::replace($_REQUEST['blogBaslik']);
			$_REQUEST['blogAciklama'] = Islemler::replace($_REQUEST['blogAciklama']);

            if($Baglanti->FormVeriGuncelle("blog",["blogBaslik","blogAciklama","blogFoto","blogTags"],"WHERE blogID = $id")){
                echo Islemler::alert("Başarıyla güncellendi!","pages/blogduzenle.php?id=$id");
            }
            else {
                echo Islemler::alert("Bir hata oluştu!","pages/blogduzenle.php?id=$id");
            }
        }
    }
    ?>
</head>
<body>
<?php require('../inc/header.php'); ?>
<?php require('../inc/sidebar.php'); ?>
<div class="content">
    <ul class=" breadcrumb">
        <li><a href="admin.php"><i class="fa fa-home"></i></a> </li>
        <li><a href="blog.php">Ürünler</a></li>
        <li>Ürün Düzenle</li>
    </ul>
    <form action="" method="post"  enctype="multipart/form-data">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="form-group">
                    <label class="control-label">Başlık :</label>
                    <input type="text" name="blogBaslik" value="<?=$sayfalar[0]['blogBaslik']?>" maxlength="60" class="form-control" />
                </div>
				


                <div class="form-group">
                    <label class="control-label">Açıklama :</label>
                    <textarea class="ckeditor" name="blogAciklama"><?=$sayfalar[0]['blogAciklama']?></textarea>
                </div>
				
		
				
	

                    <div class="form-group">
						<img src="../images/blog/<?=$sayfalar[0]['blogFoto']?>" width="300px" height="100px"/><br>
                        <label class="control-label">Fotoğraf seç (değiştirmek istemiyorsanız yükleme yapmayınız):</label>
                        <input type="file" class="form-control" name="blogFoto">
                    </div>
					
					<div class="form-group">
						<img src="../images/blog/<?=$sayfalar[0]['blogTags']?>" width="300px" height="100px"/><br>
                        <label class="control-label">Logo seç (değiştirmek istemiyorsanız yükleme yapmayınız):</label>
                        <input type="file" class="form-control" name="blogTags">
                    </div>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Kaydet</button>
            </div>
        </div>
        <input type="hidden" name="frm" value="frmEditPages" />
    </form>
    <?php require('../inc/footer.php'); ?>
</div>

</body>
</html>
