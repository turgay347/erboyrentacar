<?php require('../inc/admin.php'); ?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			require("../inc/head.php");
			if(isset($_GET["id"]))
			{
				$id = $_GET["id"];
				$sayfalar = $Baglanti->VeriCek("slider","*","WHERE sliderID = $id");
				if(@Islemler::FormKontrol('POST','frm','frmEditPages'))
				{
					$resim = Islemler::resimyukle($_FILES["sliderFoto"],Islemler::seo(rand(0,999999)),"../../images/slider");
					$_REQUEST['sliderBaslikBeyaz'] = Islemler::replace($_REQUEST['sliderBaslikBeyaz']);
					$_REQUEST['sliderAciklama'] = Islemler::replace($_REQUEST['sliderAciklama']);
					if($resim[0]=="tamam"){ $_REQUEST["sliderFoto"] = $resim[1];}else{
						$_REQUEST["sliderFoto"] = $sayfalar[0]['sliderFoto'];
					}
					
					if($Baglanti->FormVeriGuncelle("slider",["sliderSira","sliderBaslikBeyaz","sliderAciklama","sliderFoto","sliderButonUrl"],"WHERE sliderID = $id")){
							echo Islemler::alert("Başarıyla güncellendi!","pages/sliderduzenle.php?id=$id");
					}
					else {
						echo Islemler::alert("Bir hata oluştu!","pages/sliderduzenle.php?id=$id");
					}
				}
			}
		?>
	</head>
	<body>
		<?php require('../inc/header.php'); ?>
		<?php require('../inc/sidebar.php'); ?>
		<div class="content">
			<ul class=" breadcrumb">
				<li><a href="admin.php"><i class="fa fa-home"></i></a> </li>
				<li><a href="slider.php">Slider</a></li>
				<li>Slider Düzenle</li>
			</ul>
			<form action="" method="post"  enctype="multipart/form-data">
				<div class="panel panel-default">
					<div class="panel-body">
					<div class="form-group">
							<label class="control-label">Sıra :</label>
							<input type="number" name="sliderSira" class="form-control" value="<?=$sayfalar[0]['sliderSira']?>" required />
						</div>


						<div class="form-group">
							<label class="control-label">Slider başlık :</label>
							<input type="text" name="sliderBaslikBeyaz" maxlength="60" value="<?=$sayfalar[0]['sliderBaslikBeyaz']?>" class="form-control" />
						</div>
						<!--
						<div class="form-group">
							<label class="control-label">Slider alt başlık :</label>
							<input type="text" name="sliderBaslikMavi" maxlength="60" value="<?=$sayfalar[0]['sliderBaslikMavi']?>" class="form-control" />
						</div>
						
						<div class="form-group">
							<label class="control-label">Buton yazı :</label>
							<input type="text" name="sliderButonText" maxlength="60" value="<?=$sayfalar[0]['sliderButonText']?>" class="form-control" />
						</div>
						
						-->

                        <div class="form-group">
                            <label class="control-label">Buton URL</label>
                            <input type="text" name="sliderButonUrl" maxlength="60" value="<?=$sayfalar[0]['sliderButonUrl']?>" class="form-control" />
                        </div>
						
						<div class="form-group">
							<label class="control-label">Slider açıklama :</label>
							<textarea class="form-control" name="sliderAciklama"><?=$sayfalar[0]['sliderAciklama']?></textarea>
						</div>
						
						<div class="form-group">
							<img src="../images/slider/<?=$sayfalar[0]['sliderFoto']?>" width="300px" height="100px"/><br >
							<label class="control-label">Fotoğraf seç (değiştirilmeyecekse seçmeyiniz) :</label>
							<input type="file" class="form-control" name="sliderFoto">
						</div>
					</div>
					<div class="panel-footer">
						<button type="submit" class="btn btn-success">Kaydet</button>
					</div>
				</div>
				<input type="hidden" name="frm" value="frmEditPages" />
			</form>
			<?php require('../inc/footer.php'); ?>
		</div>

	</body>
</html>
