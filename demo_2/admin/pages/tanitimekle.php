<?php require('../inc/admin.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <?php
    require('../inc/head.php');
    if(@Islemler::FormKontrol("POST","frm","frmAddCategories"))
    {

            $resim = Islemler::resimyukle($_FILES["foto"],Islemler::seo(rand(0,9999999)),"../../images/tanitim/");
            if($resim[0]=="tamam"){ $_REQUEST["foto"] = $resim[1];}else{
                $_REQUEST["foto"] = "";
            }
			

			$_REQUEST['baslik1'] = Islemler::replace($_REQUEST['baslik1']);
			$_REQUEST['baslik2'] = Islemler::replace($_REQUEST['baslik2']);
			$_REQUEST['baslik3'] = Islemler::replace($_REQUEST['baslik3']);
			$_REQUEST['aciklama'] = Islemler::replace($_REQUEST['aciklama']);
			$_REQUEST['butonurl'] = Islemler::replace($_REQUEST['butonurl']);


        if($Baglanti->FormVeriEkle('urunler',["baslik1","baslik2","baslik3","foto","aciklama","butonurl","tip"]))
        {
            echo Islemler::alert("Başarıyla eklendi!","pages/tanitimekle.php");
        }
        else {
            echo Islemler::alert("Bir hata oluştu!","pages/tanitimekle.php");
        }
    }
    ?>
</head>
<body>
<?php require('../inc/header.php'); ?>
<?php require('../inc/sidebar.php'); ?>
<div class="content">
    <ul class=" breadcrumb">
        <li><a href="admin.php"><i class="fa fa-home"></i></a> </li>
        <li><a href="tanitim.php">Tanıtımlar</a> </li>
        <li>Yeni Ekle</li>
    </ul>
    <form action="" method="post"  enctype="multipart/form-data">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Yeni Ekle</h3></div>
            <div class="panel-body">



                <div class="form-group">
                    <label class="control-label">Başlık (Kalın) :</label>
                    <input type="text" name="baslik1" maxlength="60" class="form-control" />
                </div>
				<div class="form-group">
                    <label class="control-label">Başlık (Kırmızı) :</label>
                    <input type="text" name="baslik2" maxlength="60" class="form-control" />
                </div>
				<div class="form-group">
                    <label class="control-label">Başlık (İnce) :</label>
                    <input type="text" name="baslik3" maxlength="60" class="form-control" />
                </div>
				
				

                <div class="form-group">
                    <label class="control-label">Açıklama :</label>
                    <textarea class="ckeditor" name="aciklama"></textarea>
                </div>
				


                    <div class="form-group">
                        <label class="control-label">Fotoğraf seç :</label>
                        <input type="file" class="form-control" name="foto">
                    </div>
					
					<div class="form-group">
                    <label class="control-label">Buton URL :</label>
                    <input type="text" name="butonurl" class="form-control" />
                </div>
				
				<div class="form-group">
                    <label class="control-label">Gösterim</label>
                    <select name="tip" class="form-control">
						<option value="0">Açıklama solda, fotoğraf sağda</option>
						<option value="1">Açıklama sağda, fotoğraf solda</option>
					</select>
                </div>
				
				




            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Kaydet</button>
            </div>
            <input type="hidden" name="frm" value="frmAddCategories" />
        </div>
    </form>
    <?php require('../inc/footer.php'); ?>
</div>

</body>
</html>
