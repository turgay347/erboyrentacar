<?php require('inc/admin.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <?php require("inc/head.php"); ?>
    <?php
    if(isset($_GET["delete"]))
    {
        $id = $_GET["delete"];
        if($Baglanti->VeriSil("urunler","WHERE id = $id"))
        {
            echo Islemler::alert('Başarıyla silindi!','tanitim.php');
        }
        else {
            echo Islemler::alert('Bir hata oluştu!','tanitim.php');
        }
    }

    ?>
</head>
<body>
<?php require("inc/header.php"); ?>
<?php require("inc/sidebar.php"); ?>
<div class="content">
    <ul class="breadcrumb">
        <li><a href="admin.php"><i class="fa fa-home"></i></a></li>
        <li class="active"><a>Tanıtımlar</a></li>
        <li class="last"><a href="pages/tanitimekle.php">Yeni</a></li>
    </ul>
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Tanıtımlar</h3></div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-hover ">
                <thead>
                <tr>
                    <th>Başlık</th>
                    <th>Foto</th>
                    <th>İşlem</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($Baglanti->VeriCek("urunler","*","WHERE 1=1 ORDER BY id DESC") as $sayfa){ ?>
                    <tr>
                        <td><?=$sayfa["baslik1"]; ?> <?=$sayfa["baslik2"]; ?> <?=$sayfa["baslik3"]; ?></td>
                        <td><img src="../images/tanitim/<?=$sayfa["foto"]; ?>" width="300px" height="100px"/></td>

                        <td>
                             <a href="pages/tanitimduzenle.php?id=<?=$sayfa['id']; ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                            <a  href="tanitim.php?delete=<?=$sayfa['id'];?>" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php require('inc/footer.php'); ?>
</div>

</body>
</html>
