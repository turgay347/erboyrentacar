<?php require('inc/user.php');

if(@Islemler::FormKontrol("POST","token",Islemler::get_token()) && @Islemler::FormKontrol("POST","frm","frmLogin"))
{
    $email = Islemler::post('email');
    $sifre = Islemler::sifrele(Islemler::post('sifre'));
    $admin=$Bag->VeriCek("kullanicilar","*","WHERE email='$email' AND sifre='$sifre'");


    if($admin)
    {
        $_SESSION["admin_id"] = $admin[0]["id"];
        $message = '<div class="alert alert-success">Giriş yapılıyor!</div><br/>';
        Islemler::admin_redirect('admin.php');
    }
    else {
        $message = '<div class="alert alert-danger">Kullanıcı adı ve ya şifre hatalı!</div><br/>';
    }
}else{
    $message = "";
}

Islemler::create_token(); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1.0" name="viewport" />
		<meta name="MobileOptimized" content="320">
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
		<link href="css/theme.css" rel="stylesheet" type="text/css"/>
		<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

		<link href="http:://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>

		<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	</head>
	<body class="login">
    <div class="col-md-6 col-md-offset-3">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-success">
                <div class="panel-heading text-center">
                    <img src="../images/genel/<?php echo $ayarlar['site_logo']; ?>"/>
                </div>
                <form action="#" method="post">
                    <div class="panel-body">
                        <div class="form-group">
                            <input  type="email" name="email" required class="form-control" placeholder="E-posta adresi" />

                        </div>
                        <div class="form-group">
                            <input type="password" name="sifre" required class="form-control" placeholder="Şifre" />
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-success btn-block">Giriş Yap</button>
                    </div>
                    <input type="hidden" name="token" value="<?=Islemler::get_token()?>" />
                    <input type="hidden" name="frm" value="frmLogin" />
                </form>
            </div>
            <?php
            echo $message;
            ?>
        </div>
    </div>

	</body>
</html>
