<?php
class Islemler
{
  public static function diziyiDonustur($dizi,$olarak = '')
  {
    $inQuery = "";
    for($i = 0; $i < count($dizi); $i++){ if($olarak == ''){ $inQuery .= $dizi[$i].','; } else { $inQuery .= $olarak.','; } }
    $inQuery = substr($inQuery,0,strlen($inQuery)-1);
    return $inQuery;
  }

  public static function FormKontrol($type,$name,$value)
  {
    if(strtoupper($type) == "POST" && $_POST[$name] == $value)
    {
      return true;
    }
    elseif (strtoupper($type) == "GET" && $_GET[$name] == $value) {
      return true;
    }
    return false;
  }

  public static function oturumKontrol()
  {
    if($_SESSION):
      return true;
    endif;
      return false;
  }

  public static function oturumBaslat($email='',$sifre='')
  {

  }
  public static function base_url($url=null){
  	    if (isset($_SERVER['HTTP_HOST'])) {
  	        $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
  	      	$core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
  	       	$core = (!empty($core[0]))? $core[0].'/' :  '';
  	       	$return =  sprintf( "%s://%s", $http, $_SERVER['HTTP_HOST'],  $core).'/';
  	    }else{
  	       	$return = 'http://localhost/';
  	    }

  	    return ($url==null)? $return : $return.$url ;
  	}
  public static function sifrele($sifre){ return md5(md5(md5(trim($sifre)))); }
  public static function redirect($url=null){
  		$return = ($url==null)? 'location.reload()' : 'window.location.href="'.self::base_url($url).'"';
  		echo "<script>setInterval(function(){ $return; },1000);</script>";
  	}

  public static function admin_redirect($url=null){
  		$return = ($url==null)? "location.reload()" : "window.location.href=\"".self::base_url("admin/$url").'"';
  		echo "<script>setInterval(function(){ $return; },1000);</script>";
  	}

  public static function alert($deger,$link=''){ $a = '<script>alert("'.$deger.'");window.location="'.$link.'";</script>'; return $a; }

  public static function resimyukle($resim,$prefix,$dhedef){
  		$yer = $resim['tmp_name'];
  		$tip = $resim['type'];
  		$boyut = $resim['size'];
  		$max_boyut=10000000;
  		if($boyut<$max_boyut){
  			$uzanti =	strtolower(substr($resim["name"], strrpos($resim["name"], ".")));
  			$izinverilen=array(".jpg", ".jpeg", ".png", ".ico");
  			if(!in_array($uzanti, $izinverilen)){
  			return array("hata",Islemler::g("Foto�raf�n t�r� tan�namad�, ge�erli t�rler: ").implode(" ", $izinverilen));}
  			$yeniad = "".$prefix.$uzanti."";
  			$hedef = $dhedef.'/'.$yeniad;
  			if (move_uploaded_file($yer, $hedef)){
  				return array("tamam",$yeniad);
  			}else{
  				return array("hata",Islemler::g("yeni dosya yaz�lamad�: ").$hedef);
  			}
  		}
  	}
  	public static function g($a){	return $a;	}

    public static function seo($baslik){
  		$TR=array('�','�','�','�','�','�','�','�','�','�','�','�');
  		$EN=array('c','c','i','i','s','s','g','g','o','o','u','u');
  		$baslik= str_replace($TR,$EN,$baslik);
  		$baslik=mb_strtolower($baslik,'UTF-8');
  		$baslik=preg_replace('#[^-a-zA-Z0-9_, ]#','',$baslik);
  		$baslik=trim($baslik);
  		$baslik= preg_replace('#[-_ ]+#','-',$baslik);
  		return $baslik;
  	}
	
	public static function replace($x){
		$x = str_ireplace("'",  "&apos;", $x);
		$x = str_ireplace("\\", "&bsol;", $x);
		$x = str_ireplace('"',  "&quot;", $x);
		return $x;
	}

}
?>