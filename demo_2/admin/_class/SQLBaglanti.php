<?php
date_default_timezone_set('Europe/Istanbul');
class SQLBaglanti extends SQLConfig
{
  private static $Baglanti;

  public function __construct()
  {
    try {
      self::$Baglanti = new PDO("mysql:host=$this->host;dbname=$this->dbname;charset=$this->charset",$this->username,$this->password);
    } catch (PDOException $e) {
      echo $e->getMessage();
    }
  }

  public function VeriCek($tablo = '',$sutunlar='*',$kosul='')
  {
    $Veri = self::$Baglanti->query("SELECT $sutunlar FROM $tablo $kosul")->fetchAll(PDO::FETCH_ASSOC);

    return $Veri;
  }

  public function VeriEkle($tablo = '',$sutunlarVeDegerler=array())
  {
    $keys = Islemler::diziyiDonustur(array_keys($sutunlarVeDegerler));
    $values = array_values($sutunlarVeDegerler);
    $countValues = Islemler::diziyiDonustur($values,"?");

    $Durum = self::$Baglanti->prepare("INSERT INTO $tablo ($keys) VALUES ($countValues)");
    $Durum->execute($values);

    if(self::$Baglanti->lastInsertId() != 0):
      return true;
    endif;
    return false;
  }

  public function VeriGuncelle($tablo = '',$sutunlarVeDegerler=[],$where='')
  {
    $keys = array_keys($sutunlarVeDegerler);
    $values = array_values($sutunlarVeDegerler);
    $countValues = Islemler::diziyiDonustur($values,"?");
    $queryString = '';
    for($i = 0; $i < count($keys); $i++)
    {
      $queryString .= $keys[$i]." = '".$values[$i]."',";
    }
    $queryString = "UPDATE $tablo SET ".substr($queryString,0,strlen($queryString)-1).$where;
    $Durum = self::$Baglanti->query($queryString);
    if($Durum):
      return true;
    endif;
    return false;
  }

  public function FormVeriEkle($tablo = '',$sutunlar = [])
  {
    $sutunlarVeri = Islemler::diziyiDonustur($sutunlar);
    $isaretler = Islemler::diziyiDonustur($sutunlar,"?");
    $degerler = [];

    for($i = 0; $i < count($sutunlar); $i++)
    {
      array_push($degerler,$_REQUEST[$sutunlar[$i]]);
    }

    $ekle = self::$Baglanti->prepare("INSERT INTO $tablo ($sutunlarVeri) VALUES ($isaretler)");
    $ekle->execute($degerler);

    if(self::$Baglanti->lastInsertId() != 0):
      return self::$Baglanti->lastInsertId();
    endif;
    return false;
  }

  public function FormVeriGuncelle($tablo = '',$sutunlar = [],$where = '')
  {
    $queryString = '';
      foreach($sutunlar as $sutun):
        $queryString .= $sutun." = '".$_REQUEST[$sutun]."',";
      endforeach;
    $queryString = substr($queryString,0,strlen($queryString)-1);
    $queryString = "UPDATE $tablo SET ".$queryString.$where;
    $guncelle = self::$Baglanti->query($queryString);
    if($guncelle):
      return true;
    endif;
    return false;
  }

  public function VeriSil($tablo = '',$where = '')
  {
    $queryString = "DELETE FROM $tablo $where";
    $sil = self::$Baglanti->query($queryString);
    if($sil):
      return true;
    endif;
    return false;
  }

}


class Islemler
{
  public static function diziyiDonustur($dizi,$olarak = '')
  {
    $inQuery = "";
    for($i = 0; $i < count($dizi); $i++){ if($olarak == ''){ $inQuery .= $dizi[$i].','; } else { $inQuery .= $olarak.','; } }
    $inQuery = substr($inQuery,0,strlen($inQuery)-1);
    return $inQuery;
  }

  public static function FormKontrol($type,$name,$value)
  {
    if(strtoupper($type) == "POST" && $_POST[$name] == $value)
    {
      return true;
    }
    elseif (strtoupper($type) == "GET" && $_GET[$name] == $value) {
      return true;
    }
    return false;
  }

  public static function oturumKontrol()
  {
    if($_SESSION):
      return true;
    endif;
      return false;
  }

  public static function oturumBaslat($email='',$sifre='')
  {

  }
  public static function base_url($url=null){
  	    if (isset($_SERVER['HTTP_HOST'])) {
  	        $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
  	      	$core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
  	       	$core = (!empty($core[0]))? $core[0].'/' :  '';
  	       	$return =  sprintf( "%s://%s", $http, $_SERVER['HTTP_HOST'],  $core).'/';
  	    }else{
  	       	$return = 'http://localhost/';
  	    }

  	    return ($url==null)? $return : $return.$url ;
  	}
  public static function sifrele($sifre){ return md5(md5(md5(trim($sifre)))); }
  public static function redirect($url=null){
  		$return = ($url==null)? 'location.reload()' : 'window.location.href="'.self::base_url($url).'"';
  		echo "<script>setInterval(function(){ $return; },1000);</script>";
  	}

  public static function admin_redirect($url=null){
  		$return = ($url==null)? "location.reload()" : "window.location.href=\"".self::base_url("admin/$url").'"';
  		echo "<script>setInterval(function(){ $return; },1000);</script>";
  	}

  public static function alert($deger,$link=''){ $a = '<script>alert("'.$deger.'");window.location="'.$link.'";</script>'; return $a; }

  public static function resimyukle($resim,$prefix,$dhedef){
  		$yer = $resim['tmp_name'];
  		$tip = $resim['type'];
  		$boyut = $resim['size'];
  		$max_boyut=10000000;
  		if($boyut<$max_boyut){
  			$uzanti =	strtolower(substr($resim["name"], strrpos($resim["name"], ".")));
  			$izinverilen=array(".jpg", ".jpeg", ".png", ".bmp");
  			if(!in_array($uzanti, $izinverilen)){
  			return array("hata",Islemler::g("Fotoğrafın türü tanınamadı, geçerli türler: ").implode(" ", $izinverilen));}
  			$yeniad = "".$prefix.$uzanti."";
  			$hedef = $dhedef.'/'.$yeniad;
  			if (move_uploaded_file($yer, $hedef)){
  				return array("tamam",$yeniad);
  			}else{
  				return array("hata",Islemler::g("yeni dosya yazılamadı: ").$hedef);
  			}
  		}
  	}
  	public static function g($a){	return $a;	}

    public static function seo($baslik){
  		$TR=array('ç','Ç','ı','İ','ş','Ş','ğ','Ğ','ö','Ö','ü','Ü');
  		$EN=array('c','c','i','i','s','s','g','g','o','o','u','u');
  		$baslik= str_replace($TR,$EN,$baslik);
  		$baslik=mb_strtolower($baslik,'UTF-8');
  		$baslik=preg_replace('#[^-a-zA-Z0-9_, ]#','',$baslik);
  		$baslik=trim($baslik);
  		$baslik= preg_replace('#[-_ ]+#','-',$baslik);
  		return $baslik;
  	}

  	public static function create_token(){
        $_SESSION["token"] = sha1(rand());
    }

    public static function get_token(){
        return $_SESSION["token"];
    }

    public static function post($varname){
      return strip_tags(addslashes($_POST[$varname]));
    }

    public static function get($varname){
        return strip_tags(addslashes($_GET[$varname]));
    }
	
		public static function replace($x){
		$x = str_replace("'",  "&apos;", $x);
		$x = str_replace("\\", "&bsol;", $x);
		$x = str_replace('"',  "&quot;", $x);
		return $x;
	}
	
	public static function ay_getir($x){
		$ay_listesi = array(
    '01' => 'Ocak',
    '02' => 'Şubat',
    '03' => 'Mart',
    '04' => 'Nisan',
    '05' => 'Mayıs',
    '06' => 'Haziran',
    '07' => 'Temmuz',
    '08' => 'Ağustos',
    '09' => 'Eylül',
    '10' => 'Ekim',
    '11' => 'Kasım',
    '12' => 'Aralık'
);

$ay = $ay_listesi[$x];

return $ay;
	}
}
?>

