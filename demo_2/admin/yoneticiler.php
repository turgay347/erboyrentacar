<?php require('inc/admin.php'); yetkiKontrol('admin');  ?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			require("inc/head.php");
			if(isset($_GET["delete"]))
			{
				$id = $_GET["delete"];
				if($Baglanti->VeriSil("kullanicilar","WHERE id = $id"))
				{
						echo Islemler::alert('Başarıyla silindi!','yoneticiler.php');
				}
				else {
						echo Islemler::alert('Bir hata oluştu!','yoneticiler.php');
				}
			}
		?>
	</head>
	<body>
		<?php require('inc/header.php'); ?>
		<?php require('inc/sidebar.php'); ?>
		<div class="content">
			<ul class="breadcrumb">
				<li><a href="admin.php"><i class="fa fa-home"></i></a></li>
				<li class="active"><a>Yöneticiler</a></li>
				<li class="last"><a href="pages/yoneticiekle.php">Yeni Ekle</a></li>
			</ul>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Yöneticiler</h3>
				</div>
				<div class="panel-body">
					<table class="table table-striped table-bordered table-hover ">
						<thead>
						   <tr>
								<th>Tip</th>
								<th>Ad Soyad</th>
								<th>Email</th>
								<th>Kayıt Tarihi</th>
								<th>İşlem</th>
						   </tr>
						</thead>
						<tbody>
							<?php foreach($Baglanti->VeriCek("kullanicilar","*","WHERE 1=1 ORDER BY id DESC") as $list){ ?>
							<tr>
								<td><?=$list["yetki"]; ?></td>
								<td><?=$list["adsoyad"]; ?></td>
								<td><?=$list["email"]; ?></td>
								<td><?=date("d.m.Y H:i",strtotime($list["tarih"])); ?></td>

								<td>
									<a href="pages/yoneticiduzenle.php?id=<?=$list["id"]; ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
									<a  href="yoneticiler.php?delete=<?=$list["id"];?>" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
								</td>
						   	</tr>
						   	<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
			<?php require('inc/footer.php'); ?>
		</div>

	</body>

</html>
