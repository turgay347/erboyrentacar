﻿<?php require('inc/admin.php'); ?>
<!DOCTYPE html>
<html>
	<head>
		<?php require("inc/head.php"); ?>
		<?php
			if(isset($_GET["delete"]))
			{
				$id = $_GET["delete"];
				if($Baglanti->VeriSil("slider","WHERE sliderID = $id"))
				{
					echo Islemler::alert('Başarıyla silindi!','slider.php');
				}
				else {
					echo Islemler::alert('Bir hata oluştu!','slider.php');
				}
			}
		?>
	</head>
	<body>
		<?php require("inc/header.php"); ?>
		<?php require("inc/sidebar.php"); ?>
		<div class="content">
			<ul class="breadcrumb">
				<li><a href="admin.php"><i class="fa fa-home"></i></a></li>
				<li class="active"><a>Slider</a></li>
				<li class="last"><a href="pages/sliderekle.php">Yeni</a></li>
			</ul>
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Slider</h3></div>
				<div class="panel-body">
					<table class="table table-striped table-bordered table-hover ">
						<thead>
						   <tr>
								<th>Sıra</th>
								<th>Fotoğraf</th>
								<th>İşlem</th>
						   </tr>
						</thead>
						<tbody>
							<?php foreach($Baglanti->VeriCek("slider","*","ORDER BY sliderSira ASC") as $sayfa){ ?>
							<tr>
                                <td>
                                    #<?=$sayfa['sliderSira'];?>
                                </td>
								<td>
									<img src="../images/slider/<?=$sayfa['sliderFoto'];?>" width="300px" height="100px">
								</td>
								<td>
									<a  href="pages/sliderduzenle.php?id=<?=$sayfa['sliderID'];?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
									<a  href="slider.php?delete=<?=$sayfa['sliderID'];?>" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
								</td>
						   </tr>
						   <?php } ?>
						</tbody>
					</table>
				</div>
			</div>
				<?php require('inc/footer.php'); ?>
		</div>

	</body>
</html>
