<div class="black"></div>
	<nav class="navbar navbar-expand-lg navbar-light " id="nav">
	<div class="container">
  <a class="navbar-brand" href="index.php">
	<img src="images/logo.png"/>
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">


    </ul>
    <a class="p-4 btnleft"  href="#"><i class="fa fa-search"></i></a>
    <a class="p-4 btnright text-light toggleMenu" href="#"><i class="fa fa-bars"></i></a>
  </div>
  </div>

</nav>
  <div id="acilirMenu" >
	<div class="container">
		<div class="row">
        <div class="col-lg-2 col-md-6 mb-4 mb-md-0 text-left">
          <h5 class="">Kurumsal</h5>

          <ul class="list-unstyled mb-0">
            <li class="mt-3">
              <a href="hakkimizda.php" class="">Hakkımızda</a>
            </li>
            <li class="mt-3">
              <a href="vizyon.php" class="">Vizyon</a>
            </li>
            <li class="mt-3">
              <a href="misyon.php" class="">Misyon</a>
            </li>

          </ul>
        </div>
		<div class="col-lg-2 col-md-6 mb-4 mb-md-0 text-left">
          <h5 class="">Ürünler</h5>

          <ul class="list-unstyled mb-0">
            <li class="mt-3">
              <a href="#!" class="">Bakış Kapı</a>
            </li>
            <li class="mt-3">
              <a href="#!" class="">Panel</a>
            </li>
            <li class="mt-3">
              <a href="#!" class="">Kapak</a>
            </li>

          </ul>
        </div>
		<div class="col-lg-2 col-md-6 mb-4 mb-md-0 text-left">
          <h5 class="">Bayiler</h5>

          <ul class="list-unstyled mb-0">
             <li class="mt-3">
              <a href="#!" class="">Bakış Kapı</a>
            </li>
            <li class="mt-3">
              <a href="#!" class="">Panel</a>
            </li>
            <li class="mt-3">
              <a href="#!" class="">Kapak</a>
            </li>

          </ul>
        </div>
		<div class="col-lg-2 col-md-6 mb-4 mb-md-0 text-left">
          <h5 class="">Blog</h5>

          
        </div>
		<div class="col-lg-4 col-md-6 mb-4 mb-md-0 text-left">
          <h5 class="">İletişim</h5>

          <ul class="list-unstyled mb-0">
            <li class="mt-3">
              <a href="#!" class="text-white pr-3"><i class="fa fa-facebook-square"></i></a>
              <a href="#!" class="text-white pr-3"><i class="fa fa-twitter"></i></a>
              <a href="#!" class="text-white pr-3"><i class="fa fa-google-plus"></i></a>
              <a href="#!" class="text-white pr-3"><i class="fa fa-instagram"></i></a>
            </li>
            

          </ul>
        </div>
		</div>
	</div>
  </div>
<header>