<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/swiper-bundle.min.js"></script>
	<script>
    var swiper = new Swiper('.s3', {
        slidesPerView: <?php if(isMobile()){echo 1;}else{echo 3;} ?>,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,

        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
	
	var menuDurum = 0;
	
	$(".toggleMenu").click(function(){
	
		$(".black").toggle();
		$("#acilirMenu").toggle();
		
		if(menuDurum == 0){$("#nav").addClass("imp");menuDurum = 1;}else{menuDurum = 0;$("#nav").removeClass("imp");}
	})
</script>