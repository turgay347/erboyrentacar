<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/style.css"/>
	<link rel="stylesheet" href="css/swiper-bundle.min.css"/>
	<link rel="stylesheet" href="css/swiper-edit.css"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<meta charset="utf8"/>
	<style>
	.carousel-item {
  height: 100vh;
  min-height: 350px;
  background: no-repeat center center scroll;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

        .s3 {
            width: 100%;
            height: 100%;
        }

        .s3 .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }
		
		.s3 .swiper-slide{
			position:relative;
			cursor:pointer;
		}
		
		.s3 .swiper-slide .ic-cont{
			position:absolute;
			background:rgba(237,28,36,0.75);
			    width: 80%;
				padding:20px;
				text-align:left;
				display:none;
				transition: all 0.5s ease;
		}
		
		.ic-cont{
			background:rgba(237,28,36,0.75);
				padding:20px;
				text-align:left;
				transition: all 0.5s ease;
		}
		
		.s3 .swiper-slide:hover > .ic-cont{
		display:block;
		}
		
		.s3 .swiper-slide h3{
			color:#fff;
			font-family: FF SB;
			font-size:2em;
		}
		
		.s3 .swiper-slide img{
			margin-bottom:40px;
			margin-top:25px;
		}

		
		
    </style>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">