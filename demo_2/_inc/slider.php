  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
	<?php $i = 1; foreach($sliders as $slider){ ?>
      <li data-target="#carouselExampleIndicators" data-slide-to="<?=$i-1?>" class="<?=$i==1?'active':''?>"></li>
	<?php $i++; } ?>
    </ol>
	
    <div class="carousel-inner" role="listbox">
	<?php $i = 1; foreach($sliders as $slider){ ?>
      <div class="carousel-item <?=$i==1?'active':''?>" style="background-image: url('images/slider/<?=$slider['sliderFoto']?>'); ">
        <div class="carousel-caption d-none d-md-block p-5 text-left">
          <h2 class="display-4"><?php echo $slider['sliderBaslikBeyaz']; ?></h2>
          <p class="lead py-4"><?php echo $slider['sliderAciklama']; ?></p>
		<a href="<?php echo $slider['sliderButonUrl']; ?>" class="btn btn-primary kesfet p-0">KEŞFET &nbsp; &nbsp; <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
		</div>
      </div>
	<?php $i++; } ?>
	  
	  

    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
  </div>
</header>