<!-- Footer -->
<footer class="bg-dark text-center text-white pt-5 pb-3 mt-5">
  <!-- Grid container -->
  <div class="container-fluid p-4">
    <!-- Section: Social media -->


    <!-- Section: Links -->
    <section class="d-flex justify-content-center">
      <!--Grid row-->
      <div class="row">
        <!--Grid column-->
        <div class="col-lg-5 col-md-6 mb-4 mb-md-0">

          <ul class="list-unstyled mb-0 text-left">
            <li>
              <a href="#!" class="text-white"><img src="images/logo.png"/></a>
            </li>
            <li class="mt-4">
              Lorem ipsum iv ipsum iv ipsum iv ipsum iv ipsum iv ipsum iv
            </li>
            <li class="mt-3">
              <i class="fa fa-envelope"></i> info@bakis.com.tr
            </li>
            <li class="mt-3">
              <i class="fa fa-phone"></i> +90 232 234 12 12
            </li>
          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-2 col-md-6 mb-4 mb-md-0 text-left">
          <h5 class="">Kurumsal</h5>

          <ul class="list-unstyled mb-0">
            <li class="mt-3">
              <a href="#!" class="text-white">Hakkımızda</a>
            </li>
            <li class="mt-3">
              <a href="#!" class="text-white">Misyonumuz</a>
            </li>
            <li class="mt-3">
              <a href="#!" class="text-white">Vizyonumuz</a>
            </li>
            <li class="mt-3">
              <a href="#!" class="text-white">Kataloglar</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-2 col-md-6 mb-4 mb-md-0 text-left">
          <h5 class="">Ürünler</h5>

          <ul class="list-unstyled mb-0">
            <li class="mt-3">
              <a href="#!" class="text-white">Bakış Kapı</a>
            </li>
            <li class="mt-3">
              <a href="#!" class="text-white">Bakış Kapak</a>
            </li>
            <li class="mt-3">
              <a href="#!" class="text-white">Acrylic Panel</a>
            </li>

          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0 text-left">
          <h5 class="">Adres</h5>

          <ul class="list-unstyled mb-0">
            <li class="mt-3">
              Lorem ipsum 123 Street
            </li>
            <li class="mt-3">
             İzmir
            </li>
			<li class="mt-3 smicons">
				 <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fa fa-facebook"></i
      ></a>

      <!-- Twitter -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fa fa-twitter"></i
      ></a>

      <!-- Google -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fa fa-google"></i
      ></a>
			</li>
            
          </ul>
        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </section>
	    <section class="mb-4">
     

     
    </section>
    <!-- Section: Social media -->
    <!-- Section: Links -->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3">
    Copyright 2021 tüm hakları saklıdır.
  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->