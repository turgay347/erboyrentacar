<?php require 'admin/inc/user.php'; 

if(@Islemler::FormKontrol("POST","token",Islemler::get_token()) && @Islemler::FormKontrol("POST","frm","frmLog")){
		$iletisim_konu = "İletişim Formu";
		$iletisim_ad = Islemler::post('iletisim_ad');
		$iletisim_email = Islemler::post('iletisim_email');
		$iletisim_mesaj = Islemler::post('iletisim_mesaj');
		
		if(!empty($iletisim_konu) && !empty($iletisim_ad) && !empty($iletisim_email) && !empty($iletisim_mesaj)){
			
			$Bag->VeriEkle("iletisim",[
				"iletisim_konu" => $iletisim_konu,
				"iletisim_ad" => $iletisim_ad,
				"iletisim_email" => $iletisim_email,
				"iletisim_mesaj" => $iletisim_mesaj,
				"iletisim_ip" => getIP(),
			]);
			
			$message = "<script>alert('Form başarılı bir şekilde iletildi!');</script>";
		
		}else{
			$message = "<script>alert('Lütfen tüm alanları doldurunuz!');</script>";
		}
		
}

if(isset($message)){
	echo $message;
}

?>
<html>
<head>
	<title></title>
	<?php require('_inc/head.php'); ?>

</head>
<body>
		<?php require('_inc/menu.php');	?>
	<?php require('_inc/slider.php');	?>
	

<div class="container-fluid">
<div class="row">
	<div class="col-md-3 pl-5 pt-title">
		<h2>Özenle tasarlanmış ürünlerimiz</h2>
		<div class="mt-5">
		<div class="swiper-button-prev">
			<i class="fa fa-long-arrow-left" aria-hidden="true"></i>
		</div>
		<div class="swiper-button-next">
		<i class="fa fa-long-arrow-right" aria-hidden="true"></i>

		</div>
		</div>
		
	</div>
	<div class="col-md-9">
	<div class="swiper-container s3">
        <div class="swiper-wrapper">
		<?php foreach($allblogs as $blog){ ?>
            <div class="swiper-slide"><img class="img img-responsive" height="420px" src="images/blog/<?php echo $blog['blogFoto'] ?>">
				<div class="ic-cont" onclick="window.location.href='urun-detay.php?url=<?=$blog['blogSeo']?>'">
					<h3><?php echo $blog['blogBaslik']; ?></h3>
					<div class="ic-img"><img src="images/blog/<?php echo $blog['blogTags'] ?>"/></div>
					<a href="urun-detay.php?url=<?=$blog['blogSeo']?>" class="btn btn-primary kesfet2 p-0">KEŞFET <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> 
				</div>
			</div>
		<?php } ?>
        </div>
        <!-- Add Arrows -->
        
    </div>
	</div>
</div>

</div>

<div class="container-fluid">
<?php foreach($Bag->VeriCek("urunler","*"," ORDER BY id DESC") as $urun){ 
	if($urun['tip'] == 0){
		?>
		<div class="row fts">
	<div class="col-md-6 p-5 solkisim mt-auto mb-auto">
		<h3 class="mb-4"><span class="sb"><?php echo $urun['baslik1']; ?> <br><span style="font-weight:bold; color:#ed1c24;"><?php echo $urun['baslik2']; ?></span></span> <?php echo $urun['baslik3']; ?></h3>
		<p class="mb-4"><?php echo $urun['aciklama']; ?></p>
		<a class="btn btn-primary text-uppercase kesfet3" href="<?php echo $urun['butonurl']; ?>">Keşfet</a>
	</div>
	<div class="col-md-6 ht" style="background-image:url('images/tanitim/<?php echo $urun['foto']; ?>'); "></div>
</div>
		<?php
	}else{
		?>
		<div class="row fts">
<div class="col-md-6 ht" style="background-image:url('images/tanitim/<?php echo $urun['foto']; ?>'); "></div>
	<div class="col-md-6 p-5 sagkisim mt-auto mb-auto">
		<h3 class="mb-4"><span class="sb"><?php echo $urun['baslik1']; ?> <br><span style="font-weight:bold; color:#ed1c24;"><?php echo $urun['baslik2']; ?></span></span> <?php echo $urun['baslik3']; ?></h3>
		<p class="mb-4"><?php echo $urun['aciklama']; ?></p>
		<a class="btn btn-primary text-uppercase kesfet3">Keşfet</a>
	</div>
	
</div>	
		<?php
	}
?>

<?php } ?><br>

<div class="row rclib">
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-6 px-0 ">
				<img src="images/af1.jpg" width="100%" height="350px"/>
			</div>
			<div class="col-md-6 pt-5 pl-4">
				<h4>2021 kapı trendleri</h4>
				<p class="my-4">
					If the path is beautiful, let us not ask where it leads. my religion is very simple. my religion is kindness.
				</p>
				<a href="#" class="btn btn-primary kesfet p-0">KEŞFET <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-6 px-0">
				<img src="images/af2.jpg" width="100%" height="350px"/>
			</div>
			<div class="col-md-6 pt-5 pl-4">
				<h4>Endüstriyel stil mutfaklar</h4>
				<p class="my-4">
					If the path is beautiful, let us not ask where it leads. my religion is very simple. my religion is kindness.
				</p>
				<a href="#" class="btn btn-primary kesfet p-0">KEŞFET <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="row">
			
			<div class="col-md-6 pt-5 pr-0 pl-4">
				<h4>Minimalist mutfak tasarımı</h4>
				<p class="my-4">
					If the path is beautiful, let us not ask where it leads. my religion is very simple. my religion is kindness.
				</p>
				<a href="#" class="btn btn-primary kesfet p-0">KEŞFET <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			</div>
			<div class="col-md-6 pr-0 pl-0">
				<img src="images/af3.jpg"  width="100%" height="350px"/>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="row">
			
			<div class="col-md-6 pt-5 pr-0  pl-4">
				<h4>Ekletik ev dekorasyonu</h4>
				<p class="my-4">
					If the path is beautiful, let us not ask where it leads. my religion is very simple. my religion is kindness.
				</p>
				<a href="#" class="btn btn-primary kesfet p-0">KEŞFET <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			</div>
			<div class="col-md-6 pr-0  pl-0">
				<img src="images/af4.jpg"  width="100%" height="350px"/>
			</div>
		</div>
	</div>
</div>

</div>

<div class="container communication mt-5">
	<div class="row justify-content-center">
		<div class="col-md-4">
			<h2 class="mb-4">Bize<br>Ulaşın</h2>
			<p>If the path is beautiful, let us not ask where it leads. my religion is very simple. my religion is kindness.</p>
		</div>
		<div class="col-md-6">
			<form method="POST" action="#" id="communicationForm">
				 <div class="form-group">
					<input type="text" name="iletisim_ad" class="form-control" placeholder="İsim Soyisim">
				  </div>
				  <div class="form-group">
					<input type="text" name="iletisim_email" class="form-control" placeholder="E-mail adresi">
				  </div>
				  <div class="form-group">
					<textarea class="form-control" name="iletisim_mesaj" rows="7" placeholder="Konu"></textarea>
				  </div>
				  <input type="hidden" name="token" value="<?=Islemler::get_token()?>" />
				  <input type="hidden" name="frm" value="frmLog" />
				  <div class="form-group">
					<input type="submit" value="GÖNDER" class="btn btn-primary btnaltform pull-right"/>
				  </div>
			</form>
		</div>
	</div>
</div>
<?php require('_inc/footer.php'); ?>
	<?php require('_inc/script.php'); ?>
</body>
</html>