<div class="er-topnav er-topnav-dark">
                <div class="container">
                    <div class="left">
                        <a href="tel:<?=ayargetir('telefon',$dil)?>" class="er-wp"><img src="assets/img/call-center_white.png" alt="Call Center" /> <?=ayargetir('telefon',$dil)?></a>
                        <a href="https://api.whatsapp.com/send?phone=<?=ayargetir('whatsapp',$dil)?>Whatsapp&data=&app_absent=" target="_blank" class="er-wp"><img src="assets/img/whatsapp.png" alt="Whatsapp" /> <?=ayargetir('whatsapp',$dil)?></a>
						<a target="_blank" href="https://play.google.com/store/apps/details?id=com.vevona.erboycarrentacar&hl=tr&gl=US" class="er-wp"><img src="assets/img/google-play.png" alt="Google Play Store" /> </a>
						<a target="_blank" href="https://apps.apple.com/tr/app/erboycar-ara%C3%A7-kiralama/id1291969310?l=tr" class="er-wp"><img src="assets/img/app-store.png" alt="App Store" /> </a>
					</div>
                    <div class="right">
                        <!--a href="?dil=almanca" class="er-flag"><img src="assets/img/germany.png" alt="Germany" /></a--> 
                        <select class="" onchange="birimDegistir()" id="langBar">
							<option value="tl" <?php if($_COOKIE['birim'] == 'tl'){ echo "selected"; } ?>>Türk Lirası TRY</option>
							<option value="dolar" <?php if($_COOKIE['birim'] == 'dolar'){ echo "selected"; } ?>>Dollar $</option>
							<option value="euro" <?php if($_COOKIE['birim'] == 'euro'){ echo "selected"; } ?>>Euro €</option>
							<option value="sterlin" <?php if($_COOKIE['birim'] == 'sterlin'){ echo "selected"; } ?>>Sterlin £</option>
						</select>
						<a href="?dil=ingilizce" class="er-flag"><img src="assets/img/uk.png" alt="UK" /></a>
                        <a href="?dil=turkce" class="er-flag"><img src="assets/img/turkey.png" alt="Turkey" /></a>
                    </div>
                </div>
    </div>

    <div class="er-nav er-nav-light">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <a class="navbar-brand" href="<?php echo ayargetir('domain','turkce')?>"><img src="assets/img/logo-green.png" alt="Logo"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item"><a class="nav-link" href="hakkimizda"><? echo hakkimizda?></a></li>
                                <li class="nav-item"><a class="nav-link" href="arac-filomuz"><? echo aracFilomuz?></a></li>
                                <li class="nav-item"><a class="nav-link" href="ofislerimiz"><? echo ofislerimiz?></a></li>
                                <li class="nav-item"><a class="nav-link" href="kiralama-kosullari"><? echo kiralamaKosullari?></a></li>
                                <li class="nav-item"><a class="nav-link" href="iletisim"><? echo iletisim?></a></li>
                            </ul>
                </div>
            </div>
        </nav>
    </div>