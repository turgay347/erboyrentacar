<?php 

	$inf = array(
		'purchaseAmount ' => '0.05',
	);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://3dsecure.vakifbank.com.tr:4443/MPIAPI/MPI_Enrollment.aspx');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($inf));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));

	$result = curl_exec($ch);
	$xml = simplexml_load_string($result, "SimpleXMLElement", LIBXML_NOCDATA);
	
	echo '<pre>';
	print_r($xml);
echo '</pre>';
?>