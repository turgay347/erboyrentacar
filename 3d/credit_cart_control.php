<?php

$curl = curl_init();

$rw = array("a" => "b");
$rw = json_encode($rw);

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://3dsecuretest.vakifbank.com.tr:4443/MPIAPI/MPI_Enrollment.aspx',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => 'merchantID=000100000013506&merchantPassword=123456&purchaseAmount=0.05&verifyEnrollmentRequestID=1049&pan=4119790166544284&expiryDate=2404&currency=949&brandName=100&successURL=https://erboyrentacar.com/3d/basarili.php&failureURL=https://erboyrentacar.com/3d/basarisiz.php&SessionInfo='.$rw,
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/x-www-form-urlencoded',
    'Cookie: TS011f24f5=018697b855dde519f063ec8750977f6fea70f3bd9b72c53f27db865ef240973471e6bfc2de3ec1e70554b1db0c14b6adf0be320190'
  ),
));

$result = curl_exec($curl);
curl_close($curl);

$xml = simplexml_load_string($result, "SimpleXMLElement", LIBXML_NOCDATA);
echo "<pre>";
print_r($xml);
echo "</pre>";

$extracted_data = $xml->Message->VERes;

if($extracted_data->Status == "Y" || $extracted_data->Status == "A"){
	echo "Ödeme devam ediyor.";

?>

<form method="POST" id="submit3dForm" action="<?php echo $extracted_data->ACSUrl; ?>">
	<input type="hidden" name="PaReq" value="<?php echo $extracted_data->PaReq; ?>">
	<input type="hidden" name="TermUrl" value="<?php echo $extracted_data->TermUrl; ?>">
	<input type="hidden" name="MD" value="<?php echo $extracted_data->MD; ?>">
	<input type="submit" value="3D Ödemeye Geç"/>
</form>

<?php

}else{
	echo "Bir sorun oluştu";
}

