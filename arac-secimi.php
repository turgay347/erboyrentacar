<?php
ob_start();
session_start(); 
require_once "api.php";
$api = new api();
$search = $api->Search($_SESSION['form_data']);

$price = array_column($search['cars'], 'price');
$secim = "asc"; $sorting = SORT_ASC;
if(@$_GET['sira'] == 'desc'){$secim = "desc"; $sorting = SORT_DESC;}
array_multisort($price, $sorting, $search['cars']);

include 'baglanti.php';


$time = new DateTime('00:00');
    $close = new DateTime('24:00');
    while ($time < $close) {
        $day[] = $time->format('H:i');
        $time->modify('+15 minutes');
    }
	
$hasgroup = $_SESSION['form_data']['campaign'] != ''?true:false;
$campaign = array();
if($hasgroup){
	$camps = json_decode(file_get_contents("http://carsysapi.erboycar.com.tr:3224/monthly-campaign?start_date=".date("Y-m-d")),TRUE);
	foreach($camps as $camp){
		if($camp['_id'] == $_SESSION['form_data']['campaign']){
			$campaign = $camp;
			break;
		}
	}
}


?>
<?php if(!$_SESSION['form_data']){?>
    <script>
        alert("Oturum Süresi sona erdi. Lütfen Tekrar seçim yapınız");
        window.location.href = "/";
    </script>
<?php }else{
	/*echo '<pre>';
	print_r($_SESSION['form_data']);
	echo '</pre>';*/
	if(empty($_SESSION['form_data']['station_name'])){
		$_SESSION['form_data']['station_name'] = "IZMIR HAVALIMANI  IC HAT";
	}
	
} ?>
<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erboy</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="assets/fonts/Montserrat/stylesheet.css">
	<style>
	.ui-datepicker{
		background:#17995a !important;
	}
	
	.ui-datepicker .ui-datepicker-header{
		background:transparent !important;
		border:0px !important;
		color:#fff !important;
	}
	
	.ui-datepicker-calendar{
		color:#fff !important;
	}
	
	.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight{
		background:rgba(0,0,0,0.6) !important;
		color:#fff !important;
		border:0px !important;
	}
	
	.ui-state-active{
		background:rgba(0,0,0,0.7) !important;
		color:#fff !important;
		border:0px !important;
	}
	
	.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active{
		background:transparent !important;
		border:0px !important;
		color:#fff !important;
		text-align:center !important;
	}
</style>
	<style>
	.vehicle-filter li.selected {
    background-color: #037b00;
    color: #fff !important;
}
	ul.vehicle-filter {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    list-style: none;
	padding-left:10px;
}
	ul.vehicle-filter li {
    margin-right: 15px;
    border: 1px solid #037b00;
    list-style: none;
    color: #000;
    font-size: 16px;
    font-weight: 600;
    padding: 10px 15px;
    display: block;
    text-transform: uppercase;
    cursor: pointer;
}

.vehicle-filter a{
	color:#000;
}

.vehicle-filter a:hover{
	color:#000;
	text-decoration:none;
}

.vehicle-filter .selected a{
	color:#fff;
}
	</style>
</head>
<body>


		<?php 
		require('header.php');
		require('headerIc.php');
	?>

    <section class="page">
        <div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="list-group">
        <ul class="vehicle-filter">
            <li class="all selected" id="allSelect"><?php echo hepsi; ?></li>
            <li class="fuel" data="DIZEL" onclick="FilterFuelType()"><?php echo dizel; ?></li>
            <li class="fuel" data="BENZIN"><?php echo benzin; ?></li>
            <li class="transmission" data="OTOMATIK"><?php echo otomatik; ?></li>
            <li class="transmission" data="MANUEL"><?php echo manuel; ?></li>
			<li data="ARTAN" class="<?php if($secim == 'asc'){ echo "selected"; } ?>"><a href="arac-secimi.php?sira=asc"><?php echo artan; ?></a></li>
			<li data="AZALAN" class="<?php if($secim == 'desc'){ echo "selected"; } ?>"><a href="arac-secimi.php?sira=desc"><?php echo azalan; ?></a></li>
        </ul>
    </div>
				</div>
			</div>
            <div class="row">
    
                <div class="col-lg-8">
                    
					<?php if($hasgroup): ?>
						<div class="alert alert-primary">Şu anda <?=$campaign['vehicle_group']?> tipi araçlar için aylık kampanya görüntülüyorsunuz</div>
					<?php endif; ?>
					
                    <div class="er-brand-list">
                        <ul class="nav nav-pills" id="myTab" role="tablist">
                            <?php /*
                            $i = 0;
                            if(isset($search['brands'])):foreach($search['brands'] as $brand){?>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link <?php echo ($i == 0) ? 'active':'';?>" id="<?php echo $brand;?>-tab" data-toggle="tab" href="#<?php echo $brand;?>" role="tab" aria-controls="<?php echo $brand;?>" aria-selected="true">
                                    <img src="assets/img/brands/<?php echo $brand;?>.png" alt="<?php echo $brand;?>">
                                </a>
                            </li>
                            <?php 
                            $i++;
                            } endif;*/ ?>
                            
                        </ul>
                    </div>
                      
                    <div class="tab-content">
                        <?php  
                        $i = 0;
                        if($search['brands']){
                        foreach($search['brands'] as $brand){
								
							?>
                        <div class="tab-pane show <?php echo ($i == 0) ? 'active fade':'';?>" id="<?php echo $brand;?>" role="tabpanel">
                            
                            <?php if(isset($search['cars'])){  foreach($search['cars'] as $cars){
								
								if($campaign['vehicle_group'] == $cars['group'] || $campaign['vehicle_group'] == ''){
									
								
								?>
                
                            <div class="er-car" data-fuel-type="<?php echo $cars['fuel_type'];?>" data-transmission-type="<?php echo $cars['transmission_type'];?>">
                                <div class="body">
                                    <img src="<?php echo $cars['image_url'];?>" alt="<?php echo $cars['model'];?>" class="img-car">
                                    <div class="content">
                                        <h2><?php echo $cars['model'];?></h2>
                                        <span>Araç Grubu <?php echo $cars['group'];?></span>
                                        <ul class="list-unstyled">
                                            <li><img src="assets/img/features/oil.png" alt="Icon"> <?php echo $cars['fuel_type'];?></li>
                                            <li><img src="assets/img/features/gear.png" alt="Icon"> <?php echo $cars['transmission_type'];?></li>
                                            <li><img src="assets/img/features/air.png" alt="Icon"> Klimalı</li>
                                            <li><img src="assets/img/features/profile.png" alt="Icon"> <?php echo $cars['number_of_passengers'];?> Kişi</li>
                                            <li><img src="assets/img/features/car.png" alt="Icon"> <?php echo $cars['number_of_doors'];?> Kapı</li>
                                            <li><img src="assets/img/features/racing.png" alt="Icon"> <?php echo $cars['daily_km'];?> Km</li>
                                            <li><img src="assets/img/features/age.png" alt="Icon"> +<?php echo $cars['driver_age'];?></li>
                                            <li><img src="assets/img/features/badge.png" alt="Icon"> +<?php echo $cars['min_licence_age'];?></li>
                                            <li><img src="assets/img/features/wallet.png" alt="Icon"> <?php echo $cars['deposit'];?>₺</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="button">
                                    <div class="row">
									<?php if(!$hasgroup): ?>
                                        <div class="col-lg-6">
                                            <a href="#" class="btn btn-outline-success btn-block selectcart" data-type="office" data-token="<?php echo $cars['_token'];?>">
                                                <?php echo gunluk; ?> <strong><?php echo number_format($cars['price']/$oran,2);?> <?php echo $sembol; ?></strong>'den <?php echo $search['rentalDays'];?> <?php echo gun; ?> <strong><?php echo number_format($cars['price']*$search['rentalDays']/$oran,2);?> <?=$sembol?></strong><br>
                                                <span><strong><?php echo ofisteOde; ?></strong></span>
                                            </a>
                                        </div>
                                        <div class="col-lg-6">
                                            <a href="#" class="btn btn-success btn-block selectcart" data-type="creditcart" data-token="<?php echo $cars['_token'];?>">
                                                <?php echo gunluk; ?> <strong><?php echo number_format($cars['discounted_daily_price']/$oran,2);?> <?php echo $sembol; ?></strong>'den <?php echo $search['rentalDays'];?> <?php echo gun; ?> <strong><?php echo number_format($cars['discounted_daily_price']*$search['rentalDays']/$oran,2);?> <?=$sembol?></strong><br>
                                                <span><strong><?php echo krediOde; ?></strong></span>
                                            </a>
                                        </div>
                                    
									<?php else: ?>
									
							
										<div class="col-lg-6">
                                            <a href="#" class="btn btn-outline-success btn-block selectcart" data-type="office" data-token="<?php echo $cars['_token'];?>">
                                                <?php echo $search['rentalDays'];?> <?php echo gun; ?> <strong><?=$campaign['price']?> ₺</strong> <strong>Ofiste Öde</strong></span>
                                            </a>
                                        </div>
										<div class="col-lg-6">
                                            <a href="#" class="btn btn-success btn-block selectcart" data-type="creditcart" data-token="<?php echo $cars['_token'];?>">
                                                <?php echo $search['rentalDays'];?> <?php echo gun; ?> <strong><?php echo number_format($cars['discounted_daily_price']*$search['rentalDays'],2);?>₺</strong><br>
                                                <span><strong><?php echo ofisteOde; ?></strong></span>
                                            </a>
                                        </div>
									<?php endif; ?>
									</div>
                                </div>
                            </div>
                            <?php  ?>
							<?php } } }else{
							echo '<div class="alert alert-primary"> Seçilen kriterlere ait araç bulunamadı</div>';
						} ?>

                            
                        </div>
                        
                        <?php 
                        $i++;
                        }
                        }else{?>
                            <div class="alert alert-primary"> Seçilen kriterlere ait araç bulunamadı</div>
                        <?php } ?>

                        
                        
                    </div>
    
                </div>

                <div class="col-lg-4">
                    <div class="er-brand-filter">
                        <form action="" id="search-form" autocomplete="off">
						<?php if($_SESSION['form_data']['selection'] == 1): ?>
                            <div class="form-group">
                                <label for="input1"><?php echo alisTeslimYeri ?></label>
								<div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><img src="assets/img/location.png" alt="Location" /></div>
                                                </div>
                                                <input type="text" class="form-control" id="from-and-to-station" value="<?=$_SESSION['form_data']['station_name'];?>" name="station_name" autocomplete="off" />
												<input type="hidden" id="from-and-to-station-code" value="<?=$_SESSION['form_data']['station_code'];?>" name="station_code" />
                                                <ul class="liveresult">
												</ul>
                                            </div>
                            </div>
						<?php else: ?>
						<div class="form-group">
                                <label for="input1"><?php echo alisYeri ?></label>
								<div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><img src="assets/img/location.png" alt="Location" /></div>
                                                </div>
                                                <input type="text" class="form-control" id="from-station" value="<?=$_SESSION['form_data']['from_station_name'];?>" name="from_station_name" autocomplete="off" />
												<input type="hidden" id="from-station-code" value="<?=$_SESSION['form_data']['from_station_code'];?>" name="from_station_code" />
                                                <ul class="liveresult2">
												</ul>
                                            </div>
                            </div>
							<div class="form-group">
                                <label for="input1"><?php echo teslimYeri ?></label>
								<div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><img src="assets/img/location.png" alt="Location" /></div>
                                                </div>
                                                <input type="text" class="form-control" id="to-station" value="<?=$_SESSION['form_data']['to_station_name'];?>" name="to_station_name" autocomplete="off" />
												<input type="hidden" id="to-station-code" value="<?=$_SESSION['form_data']['to_station_code'];?>" name="to_station_code" />
                                                <ul class="liveresult3">
												</ul>
                                            </div>
                            </div>
						<?php endif; ?>
                            <div class="form-row">
                                <div class="col-md-7">
                                    <div class="form-group">
									<label for="tarih1"><?php echo alisTarihi ?></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend"><div class="input-group-text" id="tarih1ust"><img src="assets/img/calender.png" alt="Calendar"></div></div>
                                            <input type="text" name="startdate" class="form-control calendar" id="tarih1" placeholder="gg/aa/yyyy" value="<?=$_SESSION['form_data']['startdate'];?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                   <div class="form-group">
                                        <label for="input3"><?php echo alisSaati ?></label>
                                        <div class="input-group">
                                        <div class="er-select">
                                            <div class="icon"><img src="assets/img/time.png" alt="Time"></div>
                                            <select name="startclock" class="custom-select">
											<?php foreach($day as $d){ ?>
                                                <option <?=$d == $_SESSION['form_data']['startclock'] ? 'selected' : ''?> value="<?php echo $d; ?>"><?php echo $d; ?></option>
											<?php } ?>
                                            </select>
                                        </div>
										</div>
                                   </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="tarih1"><?php echo teslimTarihi; ?></label>
										<div class="input-group">
                                            <div class="input-group-prepend"><div class="input-group-text" id="tarih2ust"><img src="assets/img/calender.png" alt="Calendar"></div></div>
                                            <input type="text" value="<?=$_SESSION['form_data']['enddate'];?>" name="enddate" class="form-control calendar" id="tarih2" placeholder="gg/aa/yyyy">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="input5"><?php echo teslimsaati; ?></label>
										<div class="input-group">
                                        <div class="er-select">
                                            <div class="icon"><img src="assets/img/time.png" alt="Time"></div>
                                            <select name="endclock" class="custom-select">
											<?php foreach($day as $d){ ?>
                                                <option <?=$d == $_SESSION['form_data']['endclock'] ? 'selected' : ''?> value="<?php echo $d; ?>"><?php echo $d; ?></option>
											<?php } ?>
                                            </select>
                                        </div>
										</div>
                                    </div>
                                </div>
                            </div>
                        <!--
                        <div class="er-bf-box">
                            <h5><img src="assets/img/features/oil.png" alt="Oil"> Yakıt</h5>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" <?php if($_SESSION['form_data']['filters']['dizel'] == 1){ echo 'checked'; }?> class="custom-control-input" id="FilterfuelDizel">
                                <label class="custom-control-label" for="FilterfuelDizel" >Dizel</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" <?php if($_SESSION['form_data']['filters']['benzin'] == 1){ echo 'checked'; }?> class="custom-control-input" id="FilterfuelBenzin">
                                <label class="custom-control-label" for="FilterfuelBenzin">Benzin</label>
                            </div>
                        </div>
                        <div class="er-bf-box">
                            <h5><img src="assets/img/features/setting.png" alt="Setting"> Vites</h5>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" <?php if($_SESSION['form_data']['filters']['otomatik'] == 1){ echo 'checked'; }?> class="custom-control-input" id="FilterTransmissionOto">
                                <label class="custom-control-label" for="FilterTransmissionOto">Otomatik</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" <?php if($_SESSION['form_data']['filters']['manuel'] == 1){ echo 'checked'; }?> class="custom-control-input" id="FilterTransmissionMan">
                                <label class="custom-control-label" for="FilterTransmissionMan">Manuel</label>
                            </div>
                        </div>
						-->
						<button type="submit" class="btn btn-success btn-block"><?php echo araclariListele?></button>
                    </form>
					</div>
                </div>
    
            </div>
        </div>
    </section>
    <?php include('footer.php');?>

    
    <script>
	
	var SelectionType = <?php echo $_SESSION['form_data']['selection']; ?>;
	
	var selectedFuel = '';
	var selectedTransmission = '';
	
	$(".fuel").click(function(){
		$(".all").removeClass('selected');
		$(".fuel").removeClass('selected');
		$(this).addClass('selected');
		var val = $(this).attr('data');
		selectedFuel = val;
		if(selectedTransmission != ''){
			$('.er-car[data-fuel-type="'+val+'"][data-transmission-type="'+selectedTransmission+'"]').show();
			$('.er-car[data-fuel-type!="'+val+'"]').hide();
		}else{
			$('.er-car[data-fuel-type="'+val+'"]').show();
			$('.er-car[data-fuel-type!="'+val+'"]').hide();
		}
		
	});
	
	$(".transmission").click(function(){
		$(".all").removeClass('selected');
		$(".transmission").removeClass('selected');
		$(this).addClass('selected');
		var val = $(this).attr('data');
		selectedTransmission = val;
		if(selectedFuel != ''){
			$('.er-car[data-transmission-type="'+val+'"][data-fuel-type="'+selectedFuel+'"]').show();
			$('.er-car[data-transmission-type!="'+val+'"]').hide();
		}else{
			$('.er-car[data-transmission-type="'+val+'"]').show();
			$('.er-car[data-transmission-type!="'+val+'"]').hide();
		}
	});
	
	$(".all").click(function(){
		$(".fuel").removeClass('selected');
		$(".transmission").removeClass('selected');
		$(".all").addClass('selected');
		$('.er-car').show();
	})
	
        function FilterFuelType(){
			
            if($('#FilterfuelDizel2').is(':checked')){
                $('.er-car[data-fuel-type="DİZEL"]').show();
            }else{
                $('.er-car[data-fuel-type="DİZEL"]').hide();
            }
            
            if($('#FilterfuelBenzin2').is(':checked')){
                $('.er-car[data-fuel-type="BENZİN"]').show();
            }else{
                $('.er-car[data-fuel-type="BENZİN"]').hide();
            }
            
            if(!$('#FilterfuelDizel2').is(':checked') && !$('#FilterfuelBenzin2').is(':checked')){
                $('.er-car[data-fuel-type="DİZEL"]').show();
                $('.er-car[data-fuel-type="BENZİN"]').show();
            }
            
        }
        
        function FilterTransmission(){
            if($('#FilterTransmissionOto').is(':checked')){
                $('.er-car[data-transmission-type="OTOMATİK"]').show();
            }else{
                $('.er-car[data-transmission-type="OTOMATİK"]').hide();
            }
            
            if($('#FilterTransmissionMan').is(':checked')){
                $('.er-car[data-transmission-type="MANUEL"]').show();
            }else{
                $('.er-car[data-transmission-type="MANUEL"]').hide();
            }
            
            if(!$('#FilterTransmissionOto').is(':checked') && !$('#FilterTransmissionMan').is(':checked')){
                $('.er-car[data-transmission-type="OTOMATİK"]').show();
                $('.er-car[data-transmission-type="MANUEL"]').show();
            }
            
        }
        
        $('.selectcart').on('click',function(e){
            var token = $(this).data('token');
            var type = $(this).data('type');
            
            e.preventDefault();
            
            $.ajax({
                url: 'functions.php?type=selectcar',
                type: 'post',
                data: {token:token,type:type},
                dataType: 'json',
                success: function(response) {
                    if(response.success == 'true'){
                        window.location.href = response.redirect;
                    }
                    
                }
            });
            
            
            
        });
    </script>
    
</body>
</html>