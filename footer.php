,<footer class="er-footer">
            <div class="container">
                <img src="assets/img/logo-footer.png" alt="Logo Footer" class="er-footer-logo" />
                <div class="row">
                    <div class="col-md-4">
                        <p class="er-footer-text">
                            <?php echo ayargetir('footer_info',$dil);?>
                        </p>
                        <img src="assets/img/banks.png" alt="Cards" class="dnm" />
                    </div>
                    <div class="col-md-2">
                        <ul class="list-unstyled">
                            <li><a href="hakkimizda"><? echo hakkimizda?></a></li>
                            <li><a href="arac-filomuz"><? echo aracFilomuz?></a></li>
                            <li><a href="ofislerimiz"><? echo ofislerimiz?></a></li>
                        </ul>
                        <img src="assets/img/cards.png" alt="Cards" class="dnm" />
                    </div>
                    <div class="col-md-2">
                        <ul class="list-unstyled">
                            <li><a href="kiralama-kosullari"><? echo kiralamaKosullari?></a></li>
                            <li><a href="blog"><? echo blog?></a></li>
                            <li><a href="iletisim"><? echo iletisim?></a></li>
                        </ul>
                        <ul class="list-unstyled social">
                            <li>
                                <a href="<?php echo ayargetir('facebook',$dil);?>"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li>
                                <a href="<?php echo ayargetir('twitter',$dil);?>"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="<?php echo ayargetir('instagram',$dil);?>"><i class="fab fa-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <div class="er-footer-mail">
                            <p><?php echo emailText?></p>
                            <form action="" id="mail-form">
                                <div class="form-group"><input type="email" class="form-control" name="email" placeholder="<?php echo emailPlaeholder?>" /></div>
                                <button class="btn btn-success btn-block" type="submit"><?php echo gonder?> <img src="assets/img/send.png" alt="Send" /></button>
                            </form>
                        </div>
                    </div>
                </div>
                <p class="er-footer-copyright"><?php echo ayargetir('copyright',$dil)?></p>
            </div>
        </footer>
        <script src="<?php echo ayargetir('domain','turkce')?>vendor/jquery/jquery-3.5.1.min.js"></script>
        <script src="<?php echo ayargetir('domain','turkce')?>vendor/bootstrap/popper.min.js"></script>
        <script src="<?php echo ayargetir('domain','turkce')?>vendor/bootstrap/bootstrap.min.js"></script>
        <script src="<?php echo ayargetir('domain','turkce')?>vendor/owlcarousel/owl.carousel.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   	    <script src="<?php echo ayargetir('domain','turkce')?>assets/js/main.js"></script>
        <script>
		
			
		
			function campaign(t){
				<?php 
					$time = strtotime(date("Y-m-d"));
					$final = date("Y-m-d", strtotime("+30 day", $time));
				?>
				$("#tarih2").val("<?=$final?>");
				$.ajax({
                    url: "functions.php?type=submit",
                    type: "post",
                    data: $("#search-form").serialize()+"&group="+t,
                    dataType: "json",
                    success: function (response) {
                        if (response.success == "true") {
                            window.location.href = response.redirect;
                        }
                    },
                });
			}
		
            $("#search-form").on("submit", function (e) {
				
                e.preventDefault();
				var start_date = $('#tarih1').val();
				var end_date = $('#tarih2').val();
				
				if(SelectionType == 1){
					if(!$('#from-and-to-station-code').val()){
					alert("Teslim yeri seçilmelidir");
					return false;
					
				}
				}else{
					if(!$('#from-station-code').val() && !$('#to-station-code').val()){
					alert("Alış/Teslim yeri seçilmelidir");
					return false;
					
					}
				}
				
				

				
				
				if(!end_date || !start_date ){
					
					alert("Teslim tarihi ve alış tarihi seçilmelidir");
					return false;
				}
				
				var dates1 = start_date.split("-");
				var st_date = dates1[1]+"/"+dates1[0]+"/"+dates1[2];
				var start_date_time = new Date(st_date).getTime();
				
				var dates2 = end_date.split("-");
				var en_date = dates2[1]+"/"+dates2[0]+"/"+dates2[2];
				var end_date_time = new Date(en_date).getTime();
				
				

				if(end_date_time <= start_date_time ){
					alert("Teslim tarihi alış tarihinden önce olamaz");
					return false;
				}
				/*
								var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
var dateTime = date+' '+time;
var allStartTime = start_date +" "+ start_date_time;

console.log(dateTime);
console.log(allStartTime);

return false;*/
				
				var dizel = 0;
				var benzin = 0;
				var manuel = 0;
				var otomatik = 0;
				
				var other = {};
				
				if (($("#FilterfuelDizel").length > 0)){
								

					if ($('input#FilterfuelDizel').is(':checked')) {
						dizel = 1;
					}
					if ($('input#FilterfuelBenzin').is(':checked')) {
						benzin = 1;
					}
					if ($('input#FilterTransmissionOto').is(':checked')) {
						otomatik = 1;
					}
					if ($('input#FilterTransmissionMan').is(':checked')) {
						manuel = 1;
					}
					
					other = {
						'dizel' : dizel,
						'benzin' : benzin,
						'otomatik' : otomatik,
						'manuel' : manuel,
					};
					
				}
				
				
				other = JSON.stringify(other);
				other = btoa(other);
				
				
				
				
                $.ajax({
                    url: "functions.php?type=submit",
                    type: "post",
                    data: $("#search-form").serialize()+"&other="+other+"&type="+SelectionType,
                    dataType: "json",
                    success: function (response) {
                        if (response.success == "true") {
                            window.location.href = response.redirect;
                        }
                    },
                });
            });
        </script>
        <script>
            $("#mail-form").submit(function(e) {

                e.preventDefault(); 
                var form = $(this);
                var url = form.attr('action');

                $.ajax({
                    url: 'email.php',
                    type: 'POST',
                    dataType:'json',
                    data: form.serialize(),
                    success: function(msg){
                        if(msg.durum == 'ok') {
                            alert( msg.message );
                            location.reload();
                        }
                        else if(msg.durum == 'no') {
                            alert( msg.message );
                        }
                      
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert(XMLHttpRequest.responseText);
                    }
                });

            }); 
			
			function birimDegistir(){
				d = document.getElementById("langBar").value;
				window.location.href="https://www.erboyrentacar.com/?birim="+d;
			}
        </script>
    </body>
</html>