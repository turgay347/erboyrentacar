<?php echo !defined("guvenlik") ? die("Hata") : null;
$pageTitle = aracFilomuz . '- ' . ayargetir('site_title',$dil);
require('header.php');
require('headerIc.php');

$araclar = $db->query("SELECT * FROM araclar where dil = '$dil' order by arac_id  desc ")->fetchAll();

?>

    <section class="er-blog-area">
        <div class="container">
            <div class="er-card er-step-invoice clearfix">
                <?php 
                    foreach ($araclar as $key => $value) {
                        ?>
                            <div class="bg-light p-2 mb-4">
                                <div class="row ">
                                    <div class="col-md-3">
                                        <img src="i/site/<?php echo $value['arac_gorsel']?>" alt="Car" class="img-car">
                                    </div>
                                    <div class="col-md-7">
                                        <h2><?php echo $value['araci_adi']?></h2>
                                        <span><?php echo $value['arac_marka']?></span>
                                        <ul class="list-unstyled">
                                            <li><img src="assets/img/features/oil.png" alt="Icon"> <?php echo $value['yakit']?></li>
                                            <li><img src="assets/img/features/gear.png" alt="Icon"> <?php echo $value['vites']?></li>
                                            <li><img src="assets/img/features/air.png" alt="Icon"> <?php echo $value['kilima']?></li>
                                            <li><img src="assets/img/features/profile.png" alt="Icon"> <?php echo $value['kapasite']?></li>
                                            <li><img src="assets/img/features/car.png" alt="Icon"> <?php echo $value['kapi']?></li>
                                            <li><img src="assets/img/features/wallet.png" alt="Icon"> <?php echo $value['fiyat']?></li>
                                            <li><img src="assets/img/wallet-filled-money-tool.png" alt="Icon"> <?php echo $value['depozit']?></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="iletisim" class="btn btn-success btn-block"><?php echo rezervasyonYapBtn ?></a>
                                    </div>
                                </div>
                            </div>
                        <?php
                    }
                ?>
            </div>
        </div>
    </section>


<?php include('footer.php');