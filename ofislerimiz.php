<?php echo !defined("guvenlik") ? die("Hata") : null;
$pageTitle = ofislerimiz . '- ' . ayargetir('site_title',$dil);
require('header.php');
require('headerIc.php');

$ofisler = $db->query("SELECT * FROM ofisler where dil = '$dil' order by id  desc ")->fetchAll();

?>

    <section class="er-blog-area">
        <div class="container">
            <div class="er-card er-step-invoice clearfix">
                <?php 
                    foreach ($ofisler as $key => $value) {
                        ?>
                            <div class="bg-light p-2 mb-4">
                                <div class="row ">
                                    <div class="col-md-10">
                                        <h2><?php echo $value['ad']?> / <?php echo $value['sehir']?></h2>
                                        <p><?php echo $value['adres']?></p>
                                        <p><a href="tel:<?php echo $value['tel']?>"><?php echo $value['tel']?></a></p>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="<?php echo $value['map']?>" class="btn btn-success btn-block"><?php echo haritadaAc ?></a>
                                    </div>
                                </div>
                            </div>
                        <?php
                    }
                ?>
            </div>
        </div>
    </section>


<?php include('footer.php');