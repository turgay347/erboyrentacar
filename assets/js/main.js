$('.er-banner-slider').owlCarousel({
    loop: true,
    margin: 10,
    nav: false,
    dots: false,
    center: true,
    items: 1,
    responsive: {
        992: {
            stagePadding: 400,
        }
    }
})

$('.er-hc-slider').owlCarousel({
    loop: false,
    margin: 10,
    nav: true,
    dots: false,
    navText: ["<img src='assets/img/slider-left.png'>","<img src='assets/img/slider-right.png'>"],
    responsive: {
        0: {
            items: 1
        },
        992: {
            items: 3
        }
    }
})


$(function() {
	dateToday = new Date();
    $("#tarih1").datepicker({
		minDate: dateToday,
		
        monthNames: [ "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık" ],
        dayNamesMin: [ "Pa", "Pt", "Sl", "Ça", "Pe", "Cu", "Ct" ],
        dateFormat: 'dd-mm-yy'
    }); 
	dateToday2 = new Date();
	dateToday2.setDate(dateToday2.getDate() + 1);
    $("#tarih2").datepicker({
		minDate: dateToday2,
        monthNames: [ "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık" ],
        dayNamesMin: [ "Pa", "Pt", "Sl", "Ça", "Pe", "Cu", "Ct" ],
        dateFormat: 'dd-mm-yy'
    });
});

$("#tarih1").change(function(){
	t1val = $("#tarih1").val();
	t2val = $("#tarih2").val();
	if(t1val > t2val){
df = new Date();
	df.setDate(df.getDate() + 1);

alert(df);
			//$("#tarih2").val(dt);
	}
	
	
})

$('#tarih1ust').click(function(){
    $('#tarih1').datepicker('show');
});
$('#tarih2ust').click(function(){
    $('#tarih2').datepicker('show');
});

$('#from-and-to-station').click(debounce(liveSearch, 0));
$('#from-station').click(debounce(liveSearch2, 0));
$('#to-station').click(debounce(liveSearch3, 0));
$('#from-and-to-station').keyup(debounce(liveSearch, 500));
$('#from-station').keyup(debounce(liveSearch2, 500));
$('#to-station').keyup(debounce(liveSearch3, 500));

	function debounce(func, wait, immediate) {
		var timeout;
		return function () {
			var context = this, args = arguments;
			var later = function () {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	};

	function liveSearch(){
		var inputVal = $(this).val();
		var resultDropdown = $(this).siblings(".liveresult");
        $.get('functions.php', {type:"getStations",name: inputVal}).done(function(data){
            $('.liveresult').show();
            $('.liveresult').html(data);
        });
	}
	
	function liveSearch2(){
		var inputVal = $(this).val();
		var resultDropdown = $(this).siblings(".liveresult2");
        $.get('functions.php', {type:"getStations",name: inputVal}).done(function(data){
            $('.liveresult2').show();
            $('.liveresult2').html(data);
        });
	}
	
	function liveSearch3(){
		var inputVal = $(this).val();
		var resultDropdown = $(this).siblings(".liveresult3");
        $.get('functions.php', {type:"getStations",name: inputVal}).done(function(data){
            $('.liveresult3').show();
            $('.liveresult3').html(data);
        });
	}

    $(document.body).on('click','.liveresult a', function() {
        $('#from-and-to-station').val($(this).text());
        $('#from-and-to-station-code').val($(this).data('code'));
        $('.liveresult').hide();
    })
	
	$(document.body).on('click','.liveresult2 a', function() {
        $('#from-station').val($(this).text());
        $('#from-station-code').val($(this).data('code'));
        $('.liveresult2').hide();
    })
	
	$(document.body).on('click','.liveresult3 a', function() {
        $('#to-station').val($(this).text());
        $('#to-station-code').val($(this).data('code'));
        $('.liveresult3').hide();
    })
	
	$(document).mouseup(function(e) 
{
    var container = $(".liveresult, .liveresult2, .liveresult3");

    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        container.hide();
    }
});

function resetStation(){
	$("from-and-to-station").val('');
	$('from-and-to-station').click();
}