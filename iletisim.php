<?php echo !defined("guvenlik") ? die("Hata") : null;
$pageTitle = iletisim . '- ' . ayargetir('site_title',$dil);
require('header.php');
require('headerIc.php');


?>

<div class="container py-3">

		<div class="row">
			<div class="col-md-3 mb-3 mb-sm-0">
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				<a class="nav-link active" id="v-pills-info-tab" data-toggle="pill" href="#v-pills-info" role="tab" aria-controls="v-pills-info" aria-selected="true"><?php echo iletisimBilgileri ?></a>
				<a class="nav-link" id="v-pills-form-tab" data-toggle="pill" href="#v-pills-form" role="tab" aria-controls="v-pills-form" aria-selected="false"><?php echo iletisimFormu ?></a>
				</div>
			</div>
			<div class="col-md-9">
				<div class="tab-content" id="v-pills-tabContent">
					<div class="tab-pane fade show active" id="v-pills-info" role="tabpanel" aria-labelledby="v-pills-info-tab">
						<div class="row">
							<div class="col-md-4">
								<div class="address">

								<h5><strong>Adres</strong></h5>
								<p><?=ayargetir('adres',$dil)?></p>
								</div>
								<div class="email mb-3">
								<h5><strong>E Mail:</strong></h5>
								<ul class="list-unstyled">
									<li><a href="mailto:<?=ayargetir('email',$dil)?>"><?=ayargetir('email',$dil)?></a></li>
								</ul>
								</div>
								<div class="phone">
									<h5><strong>Telefon:</strong></h5>
									<ul class="list-unstyled">
									<li> <?=ayargetir('telefon',$dil)?></li>
								</ul>

								</div>
								

							</div>
							<div class="col-md-8">
								<div class="embed-responsive embed-responsive-16by9">
									<iframe class="embed-responsive-item" src="https://maps.google.com/maps?q=<?=ayargetir('map')?>&hl=tr&z=14&amp;output=embed" allowfullscreen></iframe>
								</div>

							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="v-pills-form" role="tabpanel" aria-labelledby="v-pills-form-tab">
						<div class="card">
								<div class="card-body">
									<form id="iletisim-form">
										<div class="form-group">
											<label for="adsoyad"><?php echo adsoyad ?></label>
											<input type="text" class="form-control" id="adsoyad" name="adsoyad" >
										</div>
										<div class="form-group">
											<label for="eposta"><?php echo ePostaAdresi ?></label>
											<input type="email" class="form-control" id="eposta" name="eposta" aria-describedby="emailHelp">
										</div>
										<div class="form-group">
											<label for="telefonNumarsi"><?php echo telefonNumarsi ?></label>
											<input type="text" class="form-control" id="telefonNumarsi" name="telefonNumarasi" >
										</div>
										<div class="form-group">
											<label for="mesaj"><?php echo mesaj ?></label>
											<textarea class="form-control" id="mesaj" name="mesaj" rows="3"></textarea>
										</div>
										<button type="submit" class="btn btn-success"><?php echo gonder ?></button>
									</form>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>


</div>


<?php include('footer.php');
?>
<script>
$("#iletisim-form").submit(function(e) {

	e.preventDefault(); 
	var form = $(this);
	var url = form.attr('action');

	$.ajax({
		url: 'mail.php',
		type: 'POST',
		dataType:'json',
		data: form.serialize(),
		success: function(msg){
			alert( msg.message );
			if(msg.durum == 'ok') {
				location.reload();
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert(XMLHttpRequest.responseText);
		}
	});

}); 
</script>
