<?php echo !defined("guvenlik") ? die("Hata") : null;
$pageTitle = blog . '- ' . ayargetir('site_title',$dil);
require('header.php');
require('headerIc.php');

$bloglar = $db->query("SELECT * FROM bloglar where dil = '$dil' order by id desc limit 0 , 10")->fetchAll();
$bloglarSide = $db->query("SELECT * FROM bloglar where dil = '$dil' order by id desc limit 10 , 7")->fetchAll();

?>



    <section class="er-blog-area">
        <div class="container">
            <div class="row">

                <div class="col-lg-9">

                    <?php  foreach ($bloglar as $key => $value) {
                        ?>
                            <div class="er-post">
                                <div class="top">
                                    <img src="i/site/<?php echo $value['resim']?>" alt="<?php echo $value['baslik']?>" class="img-post">
                                </div>
                                <div class="content">
                                    <h2><?php echo $value['baslik']?></h2>
                                    <p><?php echo substr(strip_tags($value['icerik']), 0, 200)    ?>...</p>
                                    <a href="blog/<?php echo $value['seo']?>" class="more"><?php echo dahaFazlaOku ?> <i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        <?php
                        
                    }?>

            


                </div>

                <div class="col-lg-3">
                        
                        <?php $i = 0; foreach ($bloglarSide as $key => $value) {
                        ?>  
                            <div class="er-post mini">
                                <div class="top">
                                    <img src="i/site/<?php echo $value['resim']?>" alt="<?php echo $value['baslik']?>" class="img-post">

                                </div>
                                <div class="content">
                                    <h2><?php echo $value['baslik']?></h2>
                                    <p><?php echo substr(strip_tags($value['icerik']), 0, 100)    ?>...</p>
                                    <a href="blog/<?php echo $value['seo']?>" class="more"><?php echo dahaFazlaOku ?> <i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        
                        <?php
                            }
                        ?>
                            

                    
                </div>

            </div>
        </div>
    </section>


<?php include('footer.php');