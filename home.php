<?php echo !defined("guvenlik") ? die("Hata") : null; 
$sliders = $db->query("SELECT * FROM slider where dil = '$dil' order by id desc ")->fetchAll();
$araclar = $db->query("SELECT * FROM araclar where dil = '$dil' order by arac_id desc limit 0,5 ")->fetchAll();
$bloglar = $db->query("SELECT * FROM bloglar where dil = '$dil' order by id desc limit 0,5 ")->fetchAll();

$time = new DateTime("00:00");
    $close = new DateTime('24:00');
    while ($time < $close) {
        $day[] = $time->format('H:i');
        $time->modify('+15 minutes');
    }
	

$time = date('H',strtotime(date("H:i")) + 3600);

require('header.php');?>
<style>
	.ui-datepicker{
		background:#17995a;
	}
	
	.ui-datepicker .ui-datepicker-header{
		background:transparent;
		border:0px;
		color:#fff;
	}
	
	.ui-datepicker-calendar{
		color:#fff;
	}
	
	.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight{
		background:rgba(0,0,0,0.6);
		color:#fff;
		border:0px;
	}
	
	.ui-state-active{
		background:rgba(0,0,0,0.7) !important;
		color:#fff;
		border:0px;
	}
	
	.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active{
		background:transparent;
		border:0px;
		color:#fff;
		text-align:center;
	}
	
	.form-control{
		background:#fff !important;
		color: #17995a !important;
	}
</style>

        <section class="er-header">
            <div class="er-topnav">
                <div class="container">
                    <div class="left">
<a href="tel:<?=ayargetir('telefon',$dil)?>" class="er-wp"><img src="assets/img/call-center_white.png" alt="Call Center" /> <?=ayargetir('telefon',$dil)?></a>                    <a target="_blank" href="https://api.whatsapp.com/send?phone=<?=ayargetir('whatsapp',$dil)?>Whatsapp&data=&app_absent=" class="er-wp"><img src="assets/img/whatsapp.png" alt="Whatsapp" /> <?=ayargetir('whatsapp',$dil)?></a>
                    <a href="https://play.google.com/store/apps/details?id=com.vevona.erboycarrentacar&hl=tr&gl=US" target="_blank" class="er-wp"><img src="assets/img/google-play.png" alt="Google Play Store" /> </a>
					<a href="https://apps.apple.com/tr/app/erboycar-ara%C3%A7-kiralama/id1291969310?l=tr" class="er-wp" target="_blank"><img src="assets/img/app-store.png"  alt="App Store" /> </a>
					</div>
                    <div class="right">
					<select class="" onchange="birimDegistir()" id="langBar">
							<option value="tl" <?php if($_COOKIE['birim'] == 'tl'){ echo "selected"; } ?>>Türk Lirası TRY</option>
							<option value="dolar" <?php if($_COOKIE['birim'] == 'dolar'){ echo "selected"; } ?>>Dollar $</option>
							<option value="euro" <?php if($_COOKIE['birim'] == 'euro'){ echo "selected"; } ?>>Euro €</option>
							<option value="sterlin" <?php if($_COOKIE['birim'] == 'sterlin'){ echo "selected"; } ?>>Sterlin £</option>
						</select>
                        <!--a href="?dil=almanca" class="er-flag"><img src="assets/img/germany.png" alt="Germany" /></a--> 
                        <a href="?dil=ingilizce" class="er-flag"><img src="assets/img/uk.png" alt="UK" /></a>
                        <a href="?dil=turkce" class="er-flag"><img src="assets/img/turkey.png" alt="Turkey" /></a>
                    </div>
                </div>
            </div>
            <div class="er-nav">
                <nav class="navbar navbar-expand-lg">
                    <div class="container">
                        <a class="navbar-brand" href="<?php echo ayargetir('domain','turkce')?>"><img src="assets/img/logo-menu.png" alt="Logo" /></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
                        <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item"><a class="nav-link" href="hakkimizda"><? echo hakkimizda?></a></li>
                                <li class="nav-item"><a class="nav-link" href="arac-filomuz"><? echo aracFilomuz?></a></li>
                                <li class="nav-item"><a class="nav-link" href="ofislerimiz"><? echo ofislerimiz?></a></li>
                                <li class="nav-item"><a class="nav-link" href="kiralama-kosullari"><? echo kiralamaKosullari?></a></li>
                                <li class="nav-item"><a class="nav-link" href="iletisim"><? echo iletisim?></a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <div class="er-banner">
                <div class="container">
                    <h2 class="title"><?php echo heroText?></h2>
                    <div class="er-banner-form">
                        <form action="" id="search-form" autocomplete="off">
                            <div class="form-row align-items-start">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
										<div class="alisteslimyeri">
                                            <label for="from-and-to-station"><?php echo alisTeslimYeri ?></label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-map-marker greencolor"></i></div>
                                                </div>
                                                <input type="text" class="form-control" id="from-and-to-station" placeholder="IZMIR HAVALIMANI IC HAT" name="station_name" autocomplete="off" />
												<input type="hidden" id="from-and-to-station-code" value="IZM52" name="station_code" />
                                                <ul class="liveresult">
												</ul>
                                            </div>
											</div>
											<div class="row">
												<div class="col-md-12 alisyeri d-none">
                                            <label for="from-station"><?php echo alisYeri ?></label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-map-marker greencolor"></i></div>
                                                </div>
                                                <input type="text" class="form-control" id="from-station" placeholder="IZMIR HAVALIMANI IC HAT" name="from_station_name" autocomplete="off" />
												<input type="hidden" id="from-station-code" value="IZM52" name="from_station_code" />
                                                <ul class="liveresult2">
												</ul>
                                            </div>
                                        </div>
										
											</div>
                                        </div>
										
                                        <div class="col-md-2">
                                        <label for="tarih1"><?php echo alisTarihi ?></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend"><div class="input-group-text" id="tarih1ust"><i class="fa fa-calendar greencolor"></i></div></div>
                                            <input type="text" name="startdate" class="form-control calendar" id="tarih1" value="<?=date("d-m-Y")?>" placeholder="gg/aa/yyyy">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="input3"><?php echo alisSaati ?></label>
                                        <div class="er-select">
                                            <div class="icon"><i class="fa fa-clock greencolor"></i></div>
                                            <select name="startclock" class="custom-select">
                                               <?php foreach($day as $d){ ?>
                                                <option <?=$d == $time.":30" ? 'selected':''?> value="<?php echo $d; ?>"><?php echo $d; ?></option>
											<?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="tarih2"><?php echo teslimTarihi ?></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend"><div class="input-group-text" id="tarih2ust"><i class="fa fa-calendar greencolor"></i></div></div>
                                            <input type="text" name="enddate" value="<?=date("d-m-Y", time() + 86400)?>" class="form-control calendar" id="tarih2" placeholder="gg/aa/yyyy">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="input5"><?php echo teslimsaati ?></label>
                                        <div class="er-select">
                                            <div class="icon"><i class="fa fa-clock greencolor"></i></div>
                                            <select name="endclock" class="custom-select">
                                                   <?php foreach($day as $d){ ?>
                                                <option <?=$d == $time.":30" ? 'selected':''?> value="<?php echo $d; ?>"><?php echo $d; ?></option>
											<?php } ?>
                                            </select>
                                        </div>
                                    </div>
									
                                    </div>
									
                                </div>

                            </div>
							<div class="row">
										<div class="col-md-4 mt-4 diff">
											<div class="t-checkbox"></div> <label for="diff_checkbox" class="text-capitalize" style="position:relative; top:-10px; left:10px;"><?php echo farkli; ?></label>
										</div>
										<div class="col-md-4 teslimyeri d-none">
                                            <label for="to-station"><?php echo teslimYeri ?></label>
											<button class="closeBtnExtra">x</button>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text"><i class="fa fa-map-marker greencolor"></i></div>
                                                </div>
                                                <input type="text" class="form-control" id="to-station" placeholder="IZMIR HAVALIMANI IC HAT" name="to_station_name" autocomplete="off" />
												<input type="hidden" id="to-station-code" value="IZM52" name="to_station_code" />
                                                <ul class="liveresult3">
												</ul>
                                            </div>
                                        </div>
										<div class="col-md-8">
										<button type="submit" style="margin-top:15px;" class="btn btn-primary btn-block"><?php echo araclariListele?></button>
										</div>
									</div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme er-banner-slider">
                <?php foreach ($sliders as $key => $value) {
                    ?>
                        <div class="item">
                            <?php if (!empty($value['text1'])) {
                                ?>
                                    <div class="er-bs-box">
                                        <div class="content"><span class="quantity"><?php echo $value['text1'] ?></span> <span class="name"><?php echo $value['text2'] ?></span> <span class="price"><?php echo $value['fiyat'] ?></span></div>
                                    </div>
                                <?php
                            }
                            ?>
                            <div class="er-bs-content"><img src="i/site/<?php echo $value['resim']?>" alt="<?php echo $value['ad']?>" /> <a href="<?php echo $value['link']?>" class="btn btn-success"><?PHP echo rezervasyonYapBtn?></a></div>
                        </div>
                    <?
                }
                ?>
            </div>
            <!--div class="er-banner-brands">
                <div class="container">
                    <ul class="list-unstyled">
                        <li>
                            <div class="img"><img src="assets/img/brands/fiat.png" alt="Fiat" /></div>
                        </li>
                        <li>
                            <div class="img"><img src="assets/img/brands/citroen.png" alt="Fiat" /></div>
                        </li>
                        <li>
                            <div class="img"><img src="assets/img/brands/mercedes.png" alt="Fiat" /></div>
                        </li>
                        <li>
                            <div class="img"><img src="assets/img/brands/ford.png" alt="Fiat" /></div>
                        </li>
                        <li>
                            <div class="img"><img src="assets/img/brands/volkswagen.png" alt="Fiat" /></div>
                        </li>
                        <li>
                            <div class="img"><img src="assets/img/brands/bmw.png" alt="Fiat" /></div>
                        </li>
                        <li>
                            <div class="img"><img src="assets/img/brands/nissan.png" alt="Fiat" /></div>
                        </li>
                        <li>
                            <div class="img"><img src="assets/img/brands/audi.png" alt="Fiat" /></div>
                        </li>
                        <li>
                            <div class="img"><img src="assets/img/brands/honda.png" alt="Fiat" /></div>
                        </li>
                    </ul>
                </div>
            </div-->
        </section>
        <section class="er-home-cars">
            <div class="container">
                <h4><?php echo aylikKiralamaKapmanyaliAraclar?></h4>
                <div class="owl-carousel owl-theme er-hc-slider">

                    <?php 
					$araclar = json_decode(file_get_contents("http://carsysapi.erboycar.com.tr:3224/monthly-campaign?start_date=".date("Y-m-d")),TRUE);
                        foreach ($araclar as $key => $value) {
							if($value['active'] == 1){
                            ?>
                                <div class="item">
                                    <div class="hc-box">
                                        <img src="assets/img/car-home.png" alt="Car" />
                                        <div class="name"><?php echo $value['description']?></div>
                                        <table>
                                            <tr>
                                                <td>Aylık Kampanya</td>
                                                <td><?php echo $value['fiyat']?></td>
                                            </tr>
                                        </table>
                                        <a href="javascript:void(0)" onclick="campaign('<?php echo $value['_id'] ?>')" class="btn btn-success btn-block"><?php echo rezervasyonYapBtn?></a>
                                    </div>
                                </div>
                            <?php
                        } }
                    ?>
                    
                

                </div>
            </div>
        </section>
        <section class="er-app">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6"><img src="assets/img/phones.png" alt="Phones" class="img-phone" /></div>
                    <div class="col-md-6">
                        <div class="content">
                            <h3><?php echo uygulamaBaslik?></h3>
                            <p><?php echo uygulamaText?></p>
                            <a href="#" class="app"><img src="assets/img/app-store.png" alt="App Store" /></a> <a href="#" class="app"><img src="assets/img/google-play.png" alt="Google Play" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="er-phone">
            <div class="container">
                <div class="content">
                    <h3><?php echo bannerText?></h3>
                    <p>
                        <?php echo bannerText2?>
                    </p>
                    <a href="tel:<?=ayargetir('telefon',$dil)?>" class="btn btn-success"><img src="assets/img/icon-tel.png" alt="Icon" /> <?=ayargetir('telefon',$dil)?></a>
                </div>
            </div>
        </section>
        <section class="er-home-blog">
            <?php // echo count($bloglar)?>
            <div class="container">
                <div class="head">
                    <div class="left">
                        <h2><?php echo blog?></h2>
                        <p><?php echo blogText?></p>
                    </div>
                </div>
                <div class="row">

                    <?php if (count($bloglar) > 0 ) {?>
                        <div class="col-md-12 col-lg-6">
                            <a href="blog/<?php echo $bloglar[0]['seo']?>" class="unstyled">
                                <div class="er-blog-video" style="background-image: url(i/site/<?php echo $bloglar[0]['resim']?>);">
                                    <div class="alt">
                                        <div class="content">
                                            <h3><?php echo $bloglar[0]['baslik']?></h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="col-md-12 col-lg-6">
                        <div class="row">
                            <?php if (count($bloglar) > 1 ) {?>
                                <div class="col-md-6">
                                    <a href="blog/<?php echo $bloglar[1]['seo']?>" class="unstyled">
                                        <div class="er-blog-vertical">
                                            <img src="i/site/<?php echo $bloglar[1]['resim']?>" alt="<?php echo $bloglar[1]['baslik']?>" class="img-blog img-fluid" />
                                            <div class="body">
                                                <div class="content">
                                                    <h3><?php echo $bloglar[1]['baslik']?></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>

                            <?php if (count($bloglar) > 2 ) {?>
                                <div class="col-md-6">
                                    <a href="blog/<?php echo $bloglar[2]['seo']?>" class="unstyled">
                                        <div class="er-blog-vertical">
                                            <img src="i/site/<?php echo $bloglar[2]['resim']?>" alt="<?php echo $bloglar[2]['baslik']?>" class="img-blog img-fluid" />
                                            <div class="body">
                                                <div class="content">
                                                    <h3><?php echo $bloglar[2]['baslik']?></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                            
                        </div>
                    </div>
                </div>
                <div class="row">

                    <?php if (count($bloglar) > 3 ) {?>
                        <div class="col-md-6">
                            <a href="blog/<?php echo $bloglar[3]['seo']?>" class="unstyled">
                                <div class="er-blog-horizontal">
                                    <img src="i/site/<?php echo $bloglar[3]['resim']?>" alt="<?php echo $bloglar[3]['baslik']?>" class="img-blog" />
                                    <div class="body">
                                        <div class="content"><h3><?php echo $bloglar[3]['baslik']?></h3></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                    
                    <?php if (count($bloglar) > 0 ) {?>
                        <div class="col-md-6">
                            <a href="blog/<?php echo $bloglar[4]['seo']?>" class="unstyled">
                                <div class="er-blog-horizontal">
                                    <img src="i/site/<?php echo $bloglar[4]['resim']?>" alt="<?php echo $bloglar[4]['baslik']?>" class="img-blog" />
                                    <div class="body">
                                        <div class="content"><h3><?php echo $bloglar[4]['baslik']?></h3></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                    
                </div>

            </div>
        </section>
<?php require('footer.php');?>       
<script>
SelectionType = 1;
	$(".diff").click(function(){
		$(".diff").hide();
			SelectionType = 2;
			$(".alisteslimyeri").addClass('d-none');
			$(".alisyeri").removeClass('d-none');
			$(".teslimyeri").removeClass('d-none');
	});
	
	$(".closeBtnExtra").click(function(){
		SelectionType = 1;
			$(".alisteslimyeri").removeClass('d-none');
			$(".alisyeri").addClass('d-none');
			$(".teslimyeri").addClass('d-none');
			$(".diff").show();
			return false;
	});
</script>