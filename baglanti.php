<?php 
include 'db/baglan.php';



function dilAyarlar($dil){
  global $db;
  $ayarlar = null;
  $ayarlar = $db->query("select * from ayarlar where dil = '$dil' ")->fetchAll(); 
  return  $ayarlar;
}

$ayarlar = dilAyarlar($_SESSION['dil']);
$dil = $_SESSION['dil'];
include 'lang/' .$dil. '.php';
// print_r($ayarlar);

$ayarlartr = dilAyarlar('turkce');
$ayarlarEn = dilAyarlar('ingilizce');
$ayarlarArap = dilAyarlar('arapca');
$ayarlarAlmanca = dilAyarlar('almanca');
function ayargetir($deger = "all", $dil = 'turkce') {
	
	global $ayarlartr ;
	global $ayarlarEn;
	global $ayarlarArap ;
	global $ayarlarAlmanca ;
  
	if ($dil != null) {
	  if ($dil == 'ingilizce') {
		$ayarlar = $ayarlarEn;
	  }
	  elseif ($dil == 'almanca') {
		$ayarlar = $ayarlarAlmanca;
	  }
	  elseif ($dil == 'turkce') {
		$ayarlar = $ayarlartr;
	  }
	  elseif ($dil == 'arapca') {
		$ayarlar = $ayarlarArap;
	  }
	}
	else {
	  $ayarlar = $ayarlar;
	  
	}

	//print_r($ayarlar);
	if ($deger == 'all') {
	  $ayar = $ayarlar;
	}
	else {
	  if (!empty($ayarlar)) {
		foreach ($ayarlar as $key => $item) {
		  if (array_search($deger,$item)) {
			$ayar= $item['ayar'];
			$ayar = html_entity_decode($ayar, ENT_QUOTES,"UTF-8");
		  }
		}
	  }
	  else {
		return array();
	  }
	}
	return $ayar;
  }
  
?>