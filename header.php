<!DOCTYPE html>
<html lang="<?=ayargetir('site_lang',$dil)?>">
    <head>
        <script id='pixel-script-poptin' src='https://cdn.popt.in/pixel.js?id=89f7b1007db63' async='true'></script>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <?php  
            if ($p == 'home.php') {
                echo '<title>' . ayargetir('site_title',$dil). '</title>';
            }
            else {

                echo '<title>' . $pageTitle . '</title>';
            }
        ?>
		
		
		<meta id="keywords" name="keywords" content="<?=ayargetir('site_keyws',$dil)?>" />
	<meta id="description" name="description" content="<?=ayargetir('site_desc',$dil)?>" />
        

        <link rel="stylesheet" href="<?php echo ayargetir('domain','turkce')?>assets/css/style.css" />
        <base href="<?php echo ayargetir('domain','turkce')?>">
        <link rel="stylesheet" href="<?php echo ayargetir('domain','turkce')?>vendor/bootstrap/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo ayargetir('domain','turkce')?>vendor/owlcarousel/owl.carousel.min.css" />
        <link rel="stylesheet" href="<?php echo ayargetir('domain','turkce')?>vendor/owlcarousel/owl.theme.default.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
        <link rel="stylesheet" href="<?php echo ayargetir('domain','turkce')?>assets/fonts/Montserrat/stylesheet.css" />
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <div style="position: relative;bottom: 0;left: 0;" id="juicier-container" data-account-id="zFDuH1RRPIYy3bkCCC685WKobkU2"><a style="font-size: 8px;color: #19191f36;text-decoration: none;" href="https://prooffactor.com" target="_blank">Powered by ProofFactor - Social Proof Notifications</a></div><script type="text/javascript" src="https://cdn.prooffactor.com/javascript/dist/1.0/jcr-widget.js?account_id=zFDuH1RRPIYy3bkCCC685WKobkU2"></script>
		<?php echo ayargetir('head_code', $dil); ?>
	</head>
    <body>
    