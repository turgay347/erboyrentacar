<?php echo !defined("guvenlik") ? die("Hata") : null;
$sayfaDetay = kiralamaSorgu($dil);
$pageTitle = $sayfaDetay->baslik;
require('header.php');
require('headerIc.php');

?>


    

    <section class="er-blog-page">
        <img src="assets/img/blog/blog-main.jpg" alt="Blog" class="er-bp-img">
        <div class="container">
            
           

            <div class="content">

                <h1><?php echo $sayfaDetay->baslik?></h1>

                <?php echo $sayfaDetay->icerik?>

            </div>

            <div class="share">
                <span><?php echo share ?></span>
                <a target="_blank" href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo $url?>" class="linkedin"><i class="fab fa-linkedin-in"></i></a>
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url?>" class="facebook"><i class="fab fa-facebook-f"></i></a>
                <a target="_blank" href="https://twitter.com/intent/tweet?url=<?=urlencode($url)?>" class="twitter"><i class="fab fa-twitter"></i></a>
            </div>

        </div>
    </section>


<?php include('footer.php');