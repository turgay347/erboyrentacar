<?php

define('hakkimizda','Hakkımızda');
define('aracFilomuz','Araç Filomuz');
define('ofislerimiz','Ofislerimiz');
define('kiralamaKosullari','Kiralama Koşulları');
define('blog','Blog');
define('iletisim','İletişim');
define('heroText','Sana ideal bir araba bulalım');
define('alisTeslimYeri','ALIŞ / TESLIM YERI');
define('alisTarihi','ALIŞ / ALIŞ TARIHI');
define('alisSaati','ALIŞ SAATI');
define('teslimTarihi','TESLIM TARIHI');
define('teslimsaati','TESLIM SAATI'); 
define('rezervasyonYapBtn','Rezervasyon Yap');
define('haritadaAc','Haritada Aç');
define('aylikKiralamaKapmanyaliAraclar','AYLIK KIRALAMALARDA KAMPANYALI ARAÇLAR');
define('bannerText','AYLIK KIRALAMALARDA KAMPANYALI ARAÇLAR');
define('bannerText2','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod.');
define('araclariListele','ARAÇLARI LİSTELE');
define('dahaFazlaOku','Daha Fazla Oku');
define('tumunuGor','Tümünü Gör'); 
define('blogText','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam.');
define('uygulamaBaslik','Hemen uygulamayı indirin ve bir araba kiralayın');
define('emailPlaeholder','E Posta Adresinizi Yazınız');
define('gonder','Gönder');
define('share','Arkadaşlarınla Paylaş');
define('emailText', 'En Güncel Haberlerden Ve Duyurulardan Haberdar Olmak İçin E-Mail Adresinizi Bırakınız');
define('uygulamaText','Erboycar son yıllarda yaptığı teknoloji odaklı yatırımlarına bir yenisi daha ekleyerek android mobil uygulamasını Google Play üzerinden yayına sundu. Yayınladığımız günden bu yana…');