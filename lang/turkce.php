<?php

define('hakkimizda','Hakkımızda');
define('aracFilomuz','Araç Filomuz');
define('ofislerimiz','Ofislerimiz');
define('kiralamaKosullari','Kiralama Koşulları');
define('blog','İzmir Araç Kiralama Fırması Erboy!');
define('iletisim','İletişim');
define('heroText','Size hemen ideal bir araba bulalım.');
define('alisTeslimYeri','Alış / Teslim Yeri');
define('alisYeri','Alış Yeri');
define('teslimYeri','Teslim Yeri');
define('alisTarihi','Alış Tarihi');
define('alisSaati','Alış Saati');
define('teslimTarihi','Teslim Tarihi');
define('teslimsaati','Teslim Saati'); 
define('rezervasyonYapBtn','Rezervasyon Yap');
define('iletisimBilgileri','İletişim Bilgileri');
define('iletisimFormu','İletişim Formu');
define('haritadaAc','Haritada Aç');
define('mailKayitBasarili','Mail Gönderimi Başarılı');
define('mailAdresiHatali','Mail Adresi Hatalı');
define('mailZatenKayitli','Mail zaten kayıtlı');
define('adsoyadGir','Ad Soyad Girmelisiniz');
define('telefonGir','Telefon Numarası Girmelisiniz');
define('aylikKiralamaKapmanyaliAraclar','AYLIK KIRALAMALARDA KAMPANYALI ARAÇLAR');
define('bannerText','AYLIK KIRALAMALARDA KAMPANYALI ARAÇLAR');
define('bannerText2','Araç Kiralamanın En Ekonomik Ve Kolay Telefonu.');
define('araclariListele','Araçları Listele');
define('dahaFazlaOku','Daha Fazla Oku');
define('tumunuGor','Tümünü Gör'); 
define('blogText','Araç Kiralama Seçenekleri ve Fiyatları
Araç kiralamak, belli bir yaşa gelmiş her insanın günün birinde ihtiyacı olan veya olacak hizmetlerden biridir. Genelde insanların, çeşitli seyahatlerde aldığı bu hizmet, hayatın diğer alanlarında da kolaylıklar sağlamaktadır. Profesyonel yapısı ve uzman personeliyle Erboy, müşteri memnuniyeti açısından yüksek kalitede hizmet sunması ile dikkat çekmektedir. Araç kiralama alanında her türlü ihtiyacınızı karşılayan Erboy, müşterilerine güvenli ve hızlı hizmetler sunmasıyla öne çıkmaktadır. İzmir merkezli araç kiralama ofisleri ve Türkiye’nin diğer birçok şehrinde bulunan araç iade ofisleri ile beraber, araba kiralamak oldukça keyifli bir deneyim haline gelmektedir.');
define('uygulamaBaslik','Hemen uygulamayı indirin ve bir araba kiralayın');
define('emailPlaeholder','E Posta Adresinizi Yazınız');
define('gonder','Gönder');
define('adsoyad','Ad Soyad');
define('ePostaAdresi','E Posta Adresi');
define('telefonNumarsi','Telefon Numarası');
define('mesaj','Mesaj');
define('share','Arkadaşlarınla Paylaş');
define('emailText', 'En Güncel Haberlerden Ve Duyurulardan Haberdar Olmak İçin E-Mail Adresinizi Bırakınız');
define('uygulamaText','Erboycar son yıllarda yaptığı teknoloji odaklı yatırımlarına bir yenisi daha ekleyerek android mobil uygulamasını Google Play üzerinden yayına sundu. Yayınladığımız günden bu yana…');
define('ofisteOde','OFİSTE ÖDE');
define('krediOde','KREDİ KARTI İLE ÖDE');
define('hepsi','HEPSİ');
define('dizel','DİZEL');
define('benzin','BENZİN');
define('otomatik','OTOMATİK');
define('manuel','MANUEL');
define('gunluk','Günlük');
define('farkli',' Farklı Yerde Bırakmak Istiyorum');
define('eksurucu','EK SÜRÜCÜ');
define('bebekkoltugu','BEBEK / ÇOCUK KOLTUĞU');
define('navigasyon','NAVİGASYON');
define('ilavekm150','İLAVE KM(150km)');
define('hgs','HGS');
define('fullsigorta','FULL SİGORTA');
define('minihasar','MİNİ HASAR SİGORTASI');
define('lcf','LCF');
define('muafiyet','MUAFİYET SİGORTASI');
define('ilavekm250','İLAVE KM (250KM)');
define('ilavekm400','İLAVE KM (400KM)');
define('yukseltici','YÜKSELTİCİ');
define('gunlukSecim','GÜNLÜK');
define('kiralamaSecim','KİRALAMA BAŞINA');
define('adim1','Araç Seçimi');
define('adim2','Araç Ekstraları');
define('adim3','Müşteri Bilgileri');
define('adim4','Rezervasyon Onay');
define('ekle','EKLE');
define('cikar','ÇIKART');
define('s1','Kişisel Bilgiler');
define('s2','Sayın');
define('s3','Müşteri Tipi');
define('s4','Şahıs');
define('s5','Kurumsal');
define('s6','Adınız');
define('s7','Soyadınız');
define('s8','İletişim Bilgileri');
define('s9','E-Posta adresiniz');
define('s10','Telefon Numaranız');
define('s11','Bay');
define('s12','Bayan');
define('s13','Kurumsal Fatura Bilgisi');
define('s14','Ünvan');
define('s15','Vergi Dairesi');
define('s16','Vergi Numarası');
define('s17','Telefon');
define('s18','Şehir');
define('s19','Ülke');
define('s20','Adres');
define('s21','Ödeme Bilgileri');
define('s22','Kart Numarası');
define('s23','Kart Üzerindeki İsim');
define('s24','Son Kullanma Tarihi');
define('s25','Ay');
define('s26','Yıl');
define('s27','Not');
define('artan','Artan Sıralama');
define('azalan','Azalan Sıralama');

