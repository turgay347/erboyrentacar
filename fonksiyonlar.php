<?php
session_start();
include 'db/baglan.php';

function noBand() { 
  $gzip_pres = true;  
  function gzipKontrol()  
  {  
      $kontrol = str_replace(" ","",  
          strtolower($_SERVER['HTTP_ACCEPT_ENCODING'])  
      );  
      $kontrol = explode(",", $kontrol);  
      return in_array("gzip", $kontrol);  
  }  
  function bosluksil($kaynak)  
  {  
    $kaynak = preg_replace('#^\s*//.+$#m', null, $kaynak); 
    $kaynak = preg_replace("/\s+/", ' ', $kaynak); 
    $kaynak = preg_replace('/<!--(.|\s)*?-->/', null, $kaynak); 
    $kaynak = trim($kaynak); 
    return $kaynak; 
  }  
  function kaynak_presle($kaynak)  
  {  
      global $gzip_pres;  
      $sayfa_cikti = bosluksil($kaynak);  
      if (!gzipKontrol() || headers_sent() || !$gzip_pres)   
          return $sayfa_cikti;  
      header("Content-Encoding: gzip");  
      return gzencode($sayfa_cikti);  
  }  
  ob_start("kaynak_presle");   
}
noBand();

$url = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

@$sayfa = @$_GET["s"];
@$v = @$_GET["v"];


if (empty($_SESSION['dil'])) {
	$_SESSION['dil'] = dilSec();
}
if (!empty($_GET["dil"])) {
	$_SESSION['dil'] = dilSec();
}


$ayarlar = dilAyarlar($_SESSION['dil']);
$dil = $_SESSION['dil'];
include 'lang/' .$dil. '.php';
// print_r($ayarlar);

$ayarlartr = dilAyarlar('turkce');
$ayarlarEn = dilAyarlar('ingilizce');
$ayarlarArap = dilAyarlar('arapca');
$ayarlarAlmanca = dilAyarlar('almanca');

function blogSorgu($v) {
	global $db;

	return  $db->query("SELECT * FROM bloglar WHERE seo='$v'")->fetch(PDO::FETCH_OBJ);
}
function hakkimizdaSorgu($dil){
    global $db;
	
    if ($dil == 'turkce') {
        $sayfaDetay = $db->query("SELECT * FROM hazirsayfa WHERE id=5 ")->fetch(PDO::FETCH_OBJ);
        return $db->query("SELECT * FROM hazirsayfa WHERE id=5 ")->fetch(PDO::FETCH_OBJ);
    }
    else if ($dil == 'almanca') {
        $sayfaDetay = $db->query("SELECT * FROM hazirsayfa WHERE id=6 ")->fetch(PDO::FETCH_OBJ);
        return $sayfaDetay;
    }
    else if ($dil == 'ingilizce') {
        $sayfaDetay = $db->query("SELECT * FROM hazirsayfa WHERE id=7 ")->fetch(PDO::FETCH_OBJ);
        return $sayfaDetay;
    }
    else if ($dil == 'arapca') {
        $sayfaDetay = $db->query("SELECT * FROM hazirsayfa WHERE id=8 ")->fetch(PDO::FETCH_OBJ);
        return $sayfaDetay;
    }
    else {
		
        $sayfaDetay = $db->query("SELECT * FROM hazirsayfa WHERE id=5 ")->fetch(PDO::FETCH_OBJ);
        return $sayfaDetay;
    }
}
function kiralamaSorgu($dil){
    global $db;
	
    if ($dil == 'turkce') {
        $sayfaDetay = $db->query("SELECT * FROM hazirsayfa WHERE id=1 ")->fetch(PDO::FETCH_OBJ);
        return $sayfaDetay;
    }
    else if ($dil == 'almanca') {
        $sayfaDetay = $db->query("SELECT * FROM hazirsayfa WHERE id=2 ")->fetch(PDO::FETCH_OBJ);
        return $sayfaDetay;
    }
    else if ($dil == 'ingilizce') {
        $sayfaDetay = $db->query("SELECT * FROM hazirsayfa WHERE id=3 ")->fetch(PDO::FETCH_OBJ);
        return $sayfaDetay;
    }
    else if ($dil == 'arapca') {
        $sayfaDetay = $db->query("SELECT * FROM hazirsayfa WHERE id=4 ")->fetch(PDO::FETCH_OBJ);
        return $sayfaDetay;
    }
    else {
		
        $sayfaDetay = $db->query("SELECT * FROM hazirsayfa WHERE id=1 ")->fetch(PDO::FETCH_OBJ);
        return $sayfaDetay;
    }
}



function dilSec(Type $var = null){
  if (empty($_GET['dil'])) {
    return 'turkce';
  }
  else {
	  if($_GET['dil'] == 'ingilizce'){
		  setcookie("birim","dolar");
	  }
    return $_GET['dil'];
  }
}
function dilAyarlar($dil){
  global $db;
  $ayarlar = null;
  $ayarlar = $db->query("select * from ayarlar where dil = '$dil' ")->fetchAll(); 
  return  $ayarlar;
}

function ayargetir($deger = "all", $dil = 'turkce') {
	
	global $ayarlartr ;
	global $ayarlarEn;
	global $ayarlarArap ;
	global $ayarlarAlmanca ;
  
	if ($dil != null) {
	  if ($dil == 'ingilizce') {
		$ayarlar = $ayarlarEn;
	  }
	  elseif ($dil == 'almanca') {
		$ayarlar = $ayarlarAlmanca;
	  }
	  elseif ($dil == 'turkce') {
		$ayarlar = $ayarlartr;
	  }
	  elseif ($dil == 'arapca') {
		$ayarlar = $ayarlarArap;
	  }
	}
	else {
	  $ayarlar = $ayarlar;
	  
	}

	//print_r($ayarlar);
	if ($deger == 'all') {
	  $ayar = $ayarlar;
	}
	else {
	  if (!empty($ayarlar)) {
		foreach ($ayarlar as $key => $item) {
		  if (array_search($deger,$item)) {
			$ayar= $item['ayar'];
			$ayar = html_entity_decode($ayar, ENT_QUOTES,"UTF-8");
		  }
		}
	  }
	  else {
		return array();
	  }
	}
	return $ayar;
  }
function tirnaktemizle($deger) {

  $deger = htmlspecialchars(stripslashes($deger));

  return $deger;

}
function turkceyap($str, $options = array()) {

	$str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());

	$defaults = array(

		'delimiter' => '-',

		'limit' => null,

		'lowercase' => true,

		'replacements' => array(),

		'transliterate' => true

	);

	$options = array_merge($defaults, $options);

	$char_map = array(

  // Latin

		'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',

		'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',

		'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',

		'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',

		'ß' => 'ss',

		'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',

		'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',

		'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',

		'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',

		'ÿ' => 'y',

  // Latin symbols

		'©' => '(c)',

  // Greek

		'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',

		'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',

		'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',

		'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',

		'Ϋ' => 'Y',

		'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',

		'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',

		'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',

		'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',

		'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',

  // Turkish

		'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',

		'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',

  // Russian

		'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',

		'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',

		'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',

		'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',

		'Я' => 'Ya',

		'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',

		'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',

		'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',

		'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',

		'я' => 'ya',

  // Ukrainian

		'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',

		'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',

  // Czech

		'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',

		'Ž' => 'Z',

		'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',

		'ž' => 'z',

  // Polish

		'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',

		'Ż' => 'Z',

		'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',

		'ż' => 'z',

  // Latvian

		'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',

		'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',

		'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',

		'š' => 's', 'ū' => 'u', 'ž' => 'z'

	);

	$str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

	if ($options['transliterate']) {

		$str = str_replace(array_keys($char_map), $char_map, $str);

	}

	$str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

	$str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

	$str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

	$str = trim($str, $options['delimiter']);

	return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;

}