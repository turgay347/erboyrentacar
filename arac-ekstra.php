<?php
ob_start();
session_start();
require_once "api.php";
$api = new api();
$extras = $api->getExtras($_SESSION['form_data']);
include 'baglanti.php';
?>

<!DOCTYPE html> 
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erboy</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="assets/fonts/Montserrat/stylesheet.css">
</head>
<body>


	<?php 
		require('header.php');
		require('headerIc.php');
	?>
	
	
    <section class="page">
        <div class="container">
            <div class="row">
    
                <div class="col-lg-8">
                    <div class="er-card">

                        <div class="er-step">
                            <div class="row">
                                <div class="col">
                                    <div class="content">
                                        <span class="number active">1</span>
                                        <span class="step"><?php echo adim1; ?></span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="content">
                                        <span class="number on">2</span>
                                        <span class="step"><?php echo adim2; ?></span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="content">
                                        <span class="number">3</span>
                                        <span class="step"><?php echo adim3; ?></span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="content">
                                        <span class="number">4</span>
                                        <span class="step"><?php echo adim4; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="body">
                            <a href="#" class="er-back"><img src="assets/img/back.png" alt="Back"> Geri</a>

                            <div class="er-setting-box" style="display:none;">
                                <h5 class="er-setting-label">Seçilebilecek Sigortalar</h5>

                                <div class="er-setting">
                                    <div class="left">
                                        <div class="name"><strong>Full Hasar Güvencesi</strong> / Günlük</div>
                                        <p>1500 TL ye kadar oluşan hasarlarda kiracı beyanı ile onarım sağlanma avantajı sunan güvencesidir. Lastik (1 adet), Cam ve Far (LCF) dahildir.</p>
                                    </div>
                                    <div class="right">
                                        <div class="price">30₺</div>
                                        <a href="#" class="btn btn-outline-success"><?php echo ekle; ?></a>
                                    </div>
                                </div>

                                <div class="er-setting">
                                    <div class="left">
                                        <div class="name"><strong>Muafiyetsiz Sigorta</strong> / Günlük</div>
                                        <p>Maddi hasarlı kazalarda 1000TL muafiyet bedelini ödememeniz için satın almanız gerekir.</p>
                                    </div>
                                    <div class="right">
                                        <div class="price">10₺</div>
                                        <a href="#" class="btn btn-success">Eklenmiş</a>
                                    </div>
                                </div>

                                <div class="er-setting">
                                    <div class="left">
                                        <div class="name"><strong>Lastik-Cam-Far Sigortası</strong> / Günlük</div>
                                        <p>Kaza durumu olmaksızın lastik-cam-far oluşabilecek hasarların karşılanma güvencesidir..</p>
                                    </div>
                                    <div class="right">
                                        <div class="price">7₺</div>
                                        <a href="#" class="btn btn-success">Eklenmiş</a>
                                    </div>
                                </div>

                            </div>

                            <div class="er-setting-box">
                                <h5 class="er-setting-label">Seçilebilecek Ekstralar</h5>
								<?php  foreach($extras as $extra){
										/*echo "<pre>";
										print_r($extra);
										echo "</pre>";*/
										
										$type = $extra['price_calculation_type'] == 'daily' ? 'gunluk' : 'sabit';
										
										$price = $type == 'gunluk' ? $extra['prices'][0]['daily_price'] : $extra['prices'][0]['constant_price'];
										/*echo "<pre>";
										print_r($extra);
										echo "</pre>"; düzelttt*/
										$exn = $_SESSION['dil'] == 'turkce' ? $extra['name_tr'] : $extra['name_en'];
										if($price != 0){
											
											$txt = $extra['vehicle_extra_category']['name'] == 'GÜNLÜK' ? gunlukSecim : kiralamaSecim;
											
											
									?> 
                                <div class="er-setting mini">
                                    <div class="left"><div class="name"><strong><?php echo $exn;?></strong> / <?php echo $txt;?></div></div>
                                    <div class="right">
                                        <div class="price"><?php echo number_format($price/$oran,2);?> <?php echo $sembol; ?></div>
										<?php if($_SESSION['form_data']['extras'][$extra['_id']]){?>
											<a href="#" data-val="<?php echo $exn;?>" data-id="<?php echo $extra['_id'];?>" data-status="true" class="btn btn-secondary extrabtn"> <?php echo cikar; ?></a>
											<input type="hidden" name="extras[<?php echo $extra['_id'];?>]" value="1">
											<input type="hidden" name="extrasval[<?php echo $exn; ?>]" value="1">
										<?php }else{?>
											<a href="#" data-id="<?php echo $extra['_id'];?>" data-val="<?php echo $exn;?>" data-status="false" class="btn btn-success extrabtn"> <?php echo ekle; ?></a>
											<input type="hidden" name="extras[<?php echo $extra['_id'];?>]" value="0">
											<input type="hidden" name="extrasval[<?php echo $exn; ?>]" value="0">
										
										<?php } ?>
                                        
                                    </div>
                                </div>
								<?php } } ?>


                            </div>

                        </div>

                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="er-card er-step-invoice" id="order_summary">
					
                    </div> 
                    <a href="siparis-bilgileri.php" class="btn btn-success btn-block er-btn-success"><?php echo info8; ?></a>
                </div>
                
            </div>
        </div>
    </section>
 
    <?php include('footer.php');?>

    <script src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>
    <script src="vendor/owlcarousel/owl.carousel.min.js"></script>
    <script src="assets/js/main.js"></script>
    
	<script>
	$('#order_summary').load('order_summary.php');
		$('.extrabtn').on('click',function(e){
			e.preventDefault();
			
			var status = $(this).data('status');
			var id = $(this).data('id');
			var val = $(this).data('val');
			console.log(status);
			if(status == false){
				$(this).removeClass('btn-success').addClass('btn-secondary');
				$('input[name="extras['+id+']"]').val('1');
				$('input[name="extrasval['+val+']"]').val('1');
				$(this).data('status',true);
				$(this).text("<?php echo cikar; ?>");
			}else if(status == true){console.log("test");
				$(this).removeClass('btn-secondary').addClass('btn-success');
				$('input[name="extras['+id+']"]').val('0');
				$('input[name="extrasval['+val+']"]').val('0');
				$(this).data('status',false);
				$(this).text("<?php echo ekle; ?>");
			}
			
			$.ajax({
				url: 'functions.php?type=selectextras',
				type: 'post',
				data: $('input[name*="extras"]').serialize(),
				dataType: 'json',
				success: function(response) {
					if(response.success == 'true'){
						$('#order_summary').load('order_summary.php');
					}
					
				}
			});
			
		})
	</script>
</body>
</html>